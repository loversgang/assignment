<?php
require_once("include/config/config.php");
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
?>
<div class="line"></div>
<div class="container">
    <div class="f_text">
        <p> Frequently Asked Questions</p>
    </div>
    <div id="pageWrap" class="pageWrap">
        <div class="pageContent">
            <ul class="accordion">
                <li>
                    <a href="#america">Isn't Unity just a gateway to plagiarism?</a>
                    <div class="tx"><span> Not at all! Unity's purpose is to help those who seek assistance in their studies and those who desire to educate themselves through a paid work environment. We do NOT encourage plagiarism, and if you choose to hand in the work that someone else does for you, then that is at your own discretion. We just provide the service that allows college students to help out others by providing their own input on assignments that should be used for studying purposes. </span></div>

                </li>
                <li>
                    <a href="#america">Are there any fees when using Unity?</a>
                    <div class="tx"><span>We have always had the vision of everyone being their own boss; the possibility of choosing when you want to work or having work done for you at your own convenience. In order to accomplish this, we believed in making the service (along with app) free for all users. With that being said, we take a 15% cut of all transactions made through the website and the app, and the rest is yours. No tricks. No hidden fees. Plain and simple.</span></div>

                </li>
                <li>
                    <a href="#america">Is this legal?</a>
                    <div class="tx"><span>We spent months developing Unity, with most of that time being devoted to the legal portion of it. As a result, we are proud to be able to safely say that using Unity is 100% legal. Although many are quick to assume that this is a plagiarism tactic, they are wrong. The definition of plagiarism is the practice of taking someone else's work or ideas and passing them off as one's own. In other words, it is only plagiarism if the receiver of the assignment takes credit for it and/or hands it in to an institution.</span></div>

                </li>

                <li>
                    <a href="#america">What forms of payment does Unity accept?</a>
                    <div class="tx"><span>Unity contains a Stripe integrated system that allows for the use of Visa, American Express, MasterCard and Discover credit/debit cards.</span></div>

                </li>
                <li>
                    <a href="#america">Am I guaranteed a certain quality of work when using Unity? </a>
                    <div class="tx"><span>Although our rating system differentiates those who have been praised from their input in the past from those who did not quite meet certain expectations, please be aware that we do not guarantee any quality of work through the use of Unity.</span></div>

                </li>
                <li>
                    <a href="#america">What is the minimum price that I can offer someone to do an assignment?</a>
                    <div class="tx"><span>There is a $20 minimum. We have this rule because some payment methods charge us when users choose to use them for their transactions. It's for the same reason when you walk into a store and they have a policy of a $5 minimum for credit card use.</span></div>

                </li>
            </ul>
        </div> 
    </div>
    <br />
    <br />
    <span class="footer-copyright">© 2016 Unity MLA, LLC. All rights reserved.</span>
    <br />
</div>
<!--<div class="bt"></div>-->	
<?php
require 'tmp/footer.php';
?>
<script src="<?php echo DIR_WS_SITE_ASSETS . 'js/faq_js/jquery.accordion.source.js' ?>" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('ul').accordion();
    });
</script> 