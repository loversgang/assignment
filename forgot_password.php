<?php
require_once("include/config/config.php");
$Page = 'home';
if ($admin_user->is_logged_in()) {
    Redirect(DIR_WS_SITE_CONTROL . "control.php");
}

if (isset($_POST['forget_password'])) {
    extract($_POST);
    $query = new user;
    $is_email_exist = $query->is_email_exist($email);
    if (!$is_email_exist) {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Email not found');
        Redirect(DIR_WS_SITE_CONTROL . "forgot_password.php");
    } else {
        $random = time();
        $query = new user;
        $query->update_field($is_email_exist->id, 'reset_password', $random);
        $subject = 'Password Reset';
        $content = 'Click<a href="' . DIR_WS_SITE_CONTROL . "reset_password.php" . '?code=' . $random . '"> here</a> to reset your password';
        send_email_by_cron($email, $content, $subject, SITE_NAME, SITE_EMAIL);
//        SendEmail($subject, $email, SITE_EMAIL, SITE_NAME, $message);
        $_SESSION['admin_session_secure']['toastr'] = 'A password reset link has been sent to your email';
        Redirect(DIR_WS_SITE);
    }
}
?>

<?php require 'tmp/header.php'; ?>
<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                A password reset link will be sent to you
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <form action="" method="POST">
                    <div class="form-group">
                        <label class="control-label col-md-4">Email</label>
                        <div class="col-md-8">
                            <input type="text" name="email" class="form-control" required/><br />
                            <button type="submit" name="forget_password" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<?php require 'tmp/footer.php'; ?>