<?php
require_once("include/config/config.php");
require_once("include/functionClass/instituteClass.php");

$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
$user_type = $_SESSION['admin_session_secure']['user_type'];
require 'tmp/header.php';
toastr_message();
?>
<div class="line"></div>
<div class="container">
    <div class="row">
        <div class="col-md-7">
            <div class="row" id="b4">
                <img src="<?php echo DIR_WS_SITE_ASSETS ?>/images/mobile_with_logo.jpg" class="comming-soon" />
            </div> 
            <div class="row" id="b1">
                <div class="col-md-8">
                    <div class="b2">College student? Need help? Need money?</div>
                </div>
                <div class="col-md-4">
                    <input type="button" value="Sign Up" class="b3 sign-hover" id="login-2" />
                </div>
            </div>
        </div>
        <div class="col-md-5 rt">
            <div class="row" id="a11">
                <h3 class="a1">Welcome to Unity</h3>
                <p class="a2" style="height: 224px;">Welcome to the service that will change education forever. With Unity, you can pay another
                    college student to do an assignment by anonymously posting it in the assignment center. Or, if you want to make
                    a quick buck, you can choose from a variety of assignments to complete that are posted by other college students.<br />100% anonymous. 100% simple.<br />100% safe.
                </p>
                <h6 class="a3"> <a href="<?php echo DIR_WS_SITE . 'about.php' ?>">Read More</a></h6>
                <p class="a31" style="height: 48px;">
                    All works delivered by Unity, such as essays, research papers, problem sets, thesis papers, term papers and all other related pieces, should be used as samples aimed for assistance purposes only.
                </p>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <div class="row" id="a21">
                        <img src="<?php echo DIR_WS_SITE_IMAGE ?>how.jpg" />
                    </div> 
                </div> 
                <div class="col-md-3">
                    <div class="row" id="a22">
                        <p class="a32">And yes, it's free.</p>
                    </div> 
                </div> 
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row" id="b7"> 
        <div class="col-md-3">
            <h1 class="b5">What is Unity?</h1>
            <p class="b6">
                Unity is a service aimed at creating the ability for college students across the country to connect and help each other both educationally and financially.
            </p>
        </div>
        <div class="col-md-3">
            <h1 class="b5">How does it work?</h1>
            <p class="b6">
                Simply make an account and then choose to either post an assignment to be completed or seek to complete another assignment and be paid.
            </p>
        </div>
        <div class="col-md-3">
            <h1 class="b5">Why choose Unity?</h1>
            <p class="b6">
                We offer a reliable system built for the use of college students only. In the United States, student loan debt exceeds $1.2 trillion, and Unity allows these indebted students to make a quick buck. Plus, it is completely anonymous.
            </p>
        </div>
        <div class="col-md-3">
            <h1 class="b5">You can trust us.</h1>
            <p class="b6">Whether it's the advanced plagiarism checker, the secure website with an SSL Certificate or the anonymous-based system, keep in mind that Unity was created by college students like you who had a vision to enhance the education industry.</p>
        </div>
    </div>
    <br />
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-header video-style">Watch our new TV ad</div>
                <div class="panel-body" style="background-color:#A8A8A8">
                    <center>
                        <iframe width="781" height="450" src="https://www.youtube.com/embed/jAsQNK54lxc" frameborder="0" allowfullscreen class="img img-responsive" style="height:400px"></iframe>
                    </center>
                    <br />
                    <span class="footer-copyright">© 2016 Unity MLA, LLC. All rights reserved.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'tmp/footer.php'; ?>