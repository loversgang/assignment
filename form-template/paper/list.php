<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active"><?php echo ucfirst($Page) ?></li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <?php if ($papers) { ?>
                    <div style="overflow-y: hidden">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($papers as $paper) { ?>
                                    <tr>
                                        <td><?php echo $paper->title ?></td>
                                        <td>
                                            <a href="<?php echo make_admin_url('assignment', 'view', 'view', 'id=' . $paper->id) ?>" class="btn btn-primary btn-xs">View</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-info">No Results Found.</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>