<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Assignments Center</li>
                </ol>
            </div>
            <div class="panel-body">
                <h5>
                    The Assignment Center is where all assignments in need of completion are posted. Simply find a post that is appealing to you, complete the assignment that is listed, and be paid that amount!  <small>*Subject to 15% fee.</small>
                </h5>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="assignments_result">
                    <?php if ($centers) { ?>
                        <div style="overflow-y: hidden">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>Be Paid</th>
                                        <th>Due By</th>
                                        <th>Time Due</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($centers as $center) { ?>
                                        <tr>
                                            <td><?php echo $center->title; ?></td>
                                            <td>
                                                <?php
                                                $obj = new subjects;
                                                $subject = $obj->getSubject($center->subject_id);
                                                echo $subject->title;
                                                ?>
                                            </td>
                                            <td>$
                                                <?php
                                                echo number_format($center->price, 2);
                                                ?>
                                            </td>
                                            <td><?php echo $center->due_date; ?></td>
                                            <td><?php echo $center->time_due; ?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('job', 'view', 'view', 'id=' . $center->id) ?>" class="btn btn-primary btn-xs">View</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">No Result Found!</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>