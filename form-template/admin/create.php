<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Admin</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <form class="form" enctype="multipart/form-data" method="post" action="">
                    <div class="form-group">
                        <label class="control-label">Title</label>
                        <input type="text" name="title" class="form-control" required/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Default Value</label>
                        <input type="text" name="default_value" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Type</label>
                        <select class="form-control options_type" name="type">
                            <option value="text">Text</option>
                            <option value="radio">Radio</option>
                            <option value="checkbox">Checkbox</option>
                            <option value="textarea">Textarea</option>
                            <option value="select">Select</option>
                            <option value="email">Email</option>
                            <option value="number">Number</option>
                        </select>
                    </div>
                    <div class="options_data"></div>
                    <div class="add_more_options_data"></div>
                    <div class="form-group">
                        <label class="control-label">Group</label>
                        <select class="form-control" name="group_id">
                            <?php foreach ($groups as $group) { ?>
                                <option value="<?php echo $group->id; ?>"><?php echo $group->title; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Position</label>
                        <input type="number" name="sequence" class="form-control"/>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_matching" value="1"> Is Matching?
                        </label>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_searchable" value="1"> Is Searchable?
                        </label>
                    </div>
                    <input type="submit" name="create_meta" value="Create Meta" class="btn btn-success"/>
                </form>
            </div>
            <div class="col-md-8">
                <ul class="list-group">
                    <li class="list-group-item active_c">Create Meta</li>
                    <li class="list-group-item radio_btns"></li>
                </ul>
            </div>
        </div>
    </div>
</div>