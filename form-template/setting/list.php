<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li><a href="<?php echo make_admin_url('profile'); ?>">Profile</a></li>
                    <li class="active">Change Password</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <p>To change your password, please provide your current password, your new password, and a confirmation of your new password.<br/>
                    <span class="body-reg-bld">Passwords are case-sensitive.</span></p>
                <form action="" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4">Current Password</label>
                        <div class="col-md-8">
                            <input type="password" name="password" value="" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">New Password</label>
                        <div class="col-md-8">
                            <input type="password" name="new_password" value="" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Confirm New Password</label>
                        <div class="col-md-8">
                            <input type="password" name="confirm_new_password" value="" class="form-control" required/>
                        </div>
                    </div>
                    <div class="col-md-12" style="padding-right: 0">
                        <input type="submit" name="change_password" class="btn btn-success pull-right" value="Save"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>