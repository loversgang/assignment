<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                Users
            </div>
            <div class="panel-body" style="padding: 15px 0px">
                <div class="col-md-12">
                    <?php display_message(1); ?>
                    <ul class="list-group">
                        <li class="list-group-item active_c">Users</li>
                        <li class="list-group-item radio_btns">
                            <div style="overflow-y: hidden">
                                <table class="table datatable">
                                    <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Profile</th>
                                            <th>Posts</th>
                                            <th>Accepted Jobs</th>
                                            <th>Block</th>
                                            <th>Delete User</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($listUsers as $listUser) { ?>
                                            <tr>
                                                <td><?php echo $listUser->username ?></td>
                                                <td><a href="<?php echo make_admin_url('profile', 'list', 'list&id=' . $listUser->id) ?>" class="btn btn-primary btn-xs">View</a></td>
                                                <td><a href="<?php echo make_admin_url('assignment', 'list', 'list&id=' . $listUser->id) ?>" class="btn btn-primary btn-xs">View</a></td>
                                                <td><a href="<?php echo make_admin_url('job', 'list', 'list&id=' . $listUser->id) ?>" class="btn btn-primary btn-xs">View</a></td>
                                                <td>
                                                    <?php if ($listUser->is_block == 1) { ?>
                                                        <a href="<?php echo make_admin_url('user', 'block', 'block&id=' . $listUser->id) ?>" class="btn btn-primary btn-xs" onclick="return confirm('Are You Sure')">Block</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo make_admin_url('user', 'block', 'block&id=' . $listUser->id . '&type=unblock') ?>" class="btn btn-primary btn-xs">Unblock</a>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo make_admin_url('user', 'delete', 'delete&id=' . $listUser->id) ?>" class="btn btn-primary btn-xs">Delete User</a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>