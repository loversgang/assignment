<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <?php if ($logged_user->first_time == 0) { ?>
            <div class="alert alert-danger" style="text-align: center"><font style="font-size: 18px;font-weight: bold">Make Your Choice</font></div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div>
                            <img class="img img-responsive" src="<?php echo DIR_WS_SITE_IMAGE ?>post_assignment.jpg" style="height: 230px;width: 100%"/>
                        </div>
                        <p>
                            Post your assignment to get it done from our community of users.
                        </p>
                        <button id="post_assignment" class="btn btn-primary btn-block">Post An Assignment</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <div>
                            <img class="img img-responsive" src="<?php echo DIR_WS_SITE_IMAGE ?>write_assignment.png" style="height: 230px;width: 100%"/>
                        </div>
                        <p>
                            Search for right assignment to do it and get paid from our community of users.
                        </p>
                        <button id="do_assignment" class="btn btn-primary btn-block">Do An Assignment</button>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <?php if ($logged_user->option_type == 'post') { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ol class="breadcrumb">
                            <li class="active">Home</li>
                            <?php if ($user_type != 'admin') { ?>
                                <p class="pull-right"><a class="btn btn-success btn-xs" href="<?php echo make_admin_url('assignment', 'insert', 'insert'); ?>">Post Assignment</a></p>
                            <?php } ?>
                        </ol>
                    </div>
                    <div class="panel-body">
                        <?php display_message(1); ?>
                        <div style="overflow-y:hidden">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <?php if ($user_type == 'admin') { ?>
                                            <th>Username</th>
                                        <?php } ?>
                                        <th>Title</th>
                                        <th>Date Posted</th>
                                        <th>Price ($)</th>
                                        <th>Status</th>
                                        <?php if ($user_type != 'admin') { ?>
                                            <th>Action</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($assignments as $assignment) { ?>
                                        <tr>
                                            <?php if ($user_type == 'admin') { ?>
                                                <td>
                                                    <?php
                                                    $user = get_object('user', $assignment->user_id);
                                                    echo $user->username;
                                                    ?>
                                                </td>
                                            <?php } ?>
                                            <td><?php echo $assignment->title; ?></td>
                                            <td><?php echo date('m/d/Y', $assignment->post_date); ?></td>
                                            <td>$ <?php echo number_format($assignment->price, 2); ?></td>
                                            <td>
                                                <?php
                                                if ($assignment->is_active == 0) {
                                                    echo 'Cancelled';
                                                } else {
                                                    echo ucfirst($assignment->status);
                                                }
                                                ?>
                                            </td>
                                            <?php if ($user_type != 'admin') { ?>
                                                <td>
                                                    <a href="<?php echo make_admin_url('assignment', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View</a>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } else { ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                            <li class="active">Jobs</li>
                            <?php if ($user_type != 'admin') { ?>
                                <p class="pull-right"><a class="btn btn-success btn-xs" href="<?php echo make_admin_url('search'); ?>">Seek Assignment</a></p>
                            <?php } ?>
                        </ol>
                    </div>
                    <div class="panel-body">
                        <?php display_message(1); ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Complete Date</th>
                                    <th>Price ($)</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($jobs as $job) {
                                    $obj = new assignment;
                                    $assignment = $obj->getAssignment($job->assignment_id);
                                    ?>
                                    <tr>
                                        <td><?php echo $assignment->title; ?></td>
                                        <td><?php echo date('m/d/Y', $assignment->post_date); ?></td>
                                        <td>$ <?php echo number_format($assignment->price, 2); ?></td>
                                        <td>
                                            <?php
                                            if ($assignment->is_active == 0) {
                                                echo 'Cancelled';
                                            } else {
                                                echo ucfirst($assignment->status);
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo make_admin_url('job', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
    <div class="col-md-2"></div>
</div>