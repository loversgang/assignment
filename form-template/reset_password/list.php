<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">My Profile</li>
            </ol>
        </div>
        <div class="panel-body">
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Photos <span class="pull-right"><a href="<?php echo make_admin_url('photo') ?>">Manage Photos</a></span></li>
                    <li class="list-group-item radio_btns">
                        <?php if ($photo) { ?>
                            <div class="wrapper account">
                                <div class="">
                                    <img src="<?php echo DIR_WS_SITE_FILE . $photo->file_name; ?>" class="img img-responsive" style="width: 100%;height: 100px"/>
                                </div>
                                <?php if ($photo->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $photo->caption; ?></div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-danger">No Photo Uploaded!</div>
                        <?php } ?>
                    </li>
                </ul>
                <ul class="list-group">
                    <li class="list-group-item active_c">Videos <span class="pull-right"><a href="<?php echo make_admin_url('video') ?>">Manage Videos</a></span></li>
                    <li class="list-group-item radio_btns">
                        <?php if ($video) { ?>
                            <div class="wrapper">
                                <div class="videos_class account">
                                    <?php echo html_entity_decode($video->video_code); ?>
                                </div>
                                <?php if ($video->caption) { ?>
                                    <div class="desc_content" style=""><?php echo $video->caption; ?></div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="alert alert-danger">No Video Uploaded!</div>
                        <?php } ?>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#About">About Me</a></li>
                    <li><a data-toggle="tab" href="#Narratives">My Narratives</a></li>
                    <li><a data-toggle="tab" href="#Personality">About My Personality</a></li>
                    <li><a data-toggle="tab" href="#Match">About My Match</a></li>
                </ul>
                <div class="tab-content">
                    <div id="About" class="tab-pane fade in active">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataAbout as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                    <div id="Narratives" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=narratives') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <?php foreach ($metaDataNarratives as $meta) { ?>
                                    <ul class="list-group">
                                        <li class="list-group-item active_c"><?php echo $meta->title; ?></li>
                                        <li class="list-group-item radio_btns">
                                            <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                        </li>
                                    </ul>
                                <?php } ?>
                            </li>
                        </ul>
                    </div>
                    <div id="Personality" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=personality') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataPersonality as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                    <div id="Match" class="tab-pane fade">
                        <ul class="list-group">
                            <li class="list-group-item" style="border-top: 0px;text-align: right"><a href="<?php echo make_admin_url('account', 'update', 'update', 'type=match') ?>">Edit</a></li>
                            <li class="list-group-item radio_btns">
                                <table class="table table-bordered">
                                    <?php foreach ($metaDataMatch as $meta) { ?>
                                        <tr>
                                            <td>
                                                <b><?php echo $meta->title; ?></b>
                                            </td>
                                            <td>
                                                <?php echo str_replace('||', ',', metaData::getMetaValue($meta->id)); ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>