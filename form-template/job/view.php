<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li><a href="<?php echo make_admin_url('job'); ?>">Jobs</a></li>
                    <li class="active">Assignment Details</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <?php if ($assignment->status == 'active') { ?>
                    <div class="form">
                        <div class="form-group">
                            <label class="control-label col-md-4">Assignment Title</label>
                            <div class="col-md-8">
                                <?php echo $assignment->title; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Subject</label>
                            <div class="col-md-8">
                                <?php
                                $obj = new subjects;
                                $subject = $obj->getSubject($assignment->subject_id);
                                echo $subject->title;
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Attached Material</label>
                            <div class="col-md-8">
                                <?php if ($assignment->file) { ?>
                                    <a class="btn btn-primary btn-xs view" href="<?php echo DIR_WS_SITE_ASSETS . 'assignments/' . $assignment->file ?>" download>Download</a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Payment Method</label>
                            <div class="col-md-8">
                                <?php
                                $obj = new paymentMethod;
                                $method = $obj->getPaymentMethod($assignment->payment_method);
                                echo $method->title;
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Detailed Description of Assignment</label>
                            <div class="col-md-8">
                                <?php echo $assignment->description; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Length (in pages)</label>
                            <div class="col-md-8">
                                <?php echo $assignment->length_in_pages; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Get Paid (USD)</label>
                            <div class="col-md-8">
                                $<?php echo number_format($assignment->price, 2); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Due Date</label>
                            <div class="col-md-8">
                                <?php echo $assignment->due_date; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Time Due</label>
                            <div class="col-md-8">
                                <?php echo $assignment->time_due; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Any link you would like to provide? (Optional)</label>
                            <div class="col-md-8">
                                <?php echo $assignment->optional_link; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <form class="form" action="" method="post">
                            <input type="hidden" name="assignment_id" value="<?php echo $assignment->id; ?>"/>
                            <input type="hidden" name="payment_method" value="<?php echo $assignment->payment_method; ?>"/>
                            <input type="hidden" name="payment_amount" value="<?php echo $assignment->price; ?>"/>
                            <input type="submit" name="accept_assignment" value="Accept assignment" id="accept" class="btn btn-success pull-right" />
                        </form>
                    </div>
                <?php } elseif ($assignment->status == 'Being Done') { ?>
                    <div class="col-md-12">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div style="text-align: center">
                                <img class="img img-responsive" src="<?php echo DIR_WS_SITE_IMAGE ?>upload_assignment.png" style="height: 200px;width: 100%"/>
                                <form class="form" method="post" action="" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="control-label">Upload Assignments</label>
                                        <input type="file" class="form-control" name="file" required/>
                                        <input type="hidden" name="assignment_id" value="<?php echo $assignment->id; ?>"/>
                                    </div>
                                    <input type="submit" name="upload_assignment" class="btn btn-primary btn-block" value="Upload"/>
                                </form>
                                <br/>
                                <form class="form" action="" method="post">
                                    <input type="hidden" name="assignment_id" value="<?php echo $assignment->id; ?>"/>
                                    <input type="submit" name="cancel_assignment" value="Cancel assignment" class="btn btn-danger btn-block"/>
                                </form>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                <?php } elseif ($assignment->status == 'Submitted') { ?>
                    <?php if ($assignment->is_task_approved == 2) { ?>
                        <div class="alert alert-info">Declined by a Unity representative</div>
                    <?php } else { ?>
                        <div class="form-group">
                            <label class="control-label col-md-4">View Rating</label>
                            <div class="col-md-8">
                                <div class="rating pull-left">
                                    <input class="star star-5" id="star-5-2" type="radio" name="star"/>
                                    <label class="star star-5" for="star-5-2" <?php echo $job->rating > 4 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                    <input class="star star-4" id="star-4-2" type="radio" name="star"/>
                                    <label class="star star-4" for="star-4-2" <?php echo $job->rating > 3 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                    <input class="star star-3" id="star-3-2" type="radio" name="star"/>
                                    <label class="star star-3" for="star-3-2" <?php echo $job->rating > 2 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                    <input class="star star-2" id="star-2-2" type="radio" name="star"/>
                                    <label class="star star-2" for="star-2-2" <?php echo $job->rating > 1 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                    <input class="star star-1" id="star-1-2" type="radio" name="star"/>
                                    <label class="star star-1" for="star-1-2" <?php echo $job->rating > 0 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr_custom"/>
                        </div>
                        <div class="form">
                            <div class="form-group">
                                <label class="control-label col-md-4">Assignment Title</label>
                                <div class="col-md-8">
                                    <?php echo $assignment->title; ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr_custom"/>
                            <?php
                            $obj = new job;
                            $job = $obj->getJobByAssignmentId($assignment->id);
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-4">Price (USD)</label>
                                <div class="col-md-8">$
                                    <?php
                                    $price = get_object('assignment', $job->assignment_id);
                                    echo number_format($price->price, 2);
                                    ?>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr_custom"/>
                        </div>
                    <?php } ?>
                <?php } elseif ($assignment->status == 'Paid') { ?>
                    <div class="form">
                        <div class="form-group">
                            <label class="control-label col-md-4">Assignment Title</label>
                            <div class="col-md-8">
                                <?php echo $assignment->title; ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <?php
                        $obj = new job;
                        $job = $obj->getJobByAssignmentId($assignment->id);
                        ?>
                        <div class="form-group">
                            <label class="control-label col-md-4">Price (USD)</label>
                            <div class="col-md-8">
                                <b>
                                    <?php echo number_format(($job->payment_amount * 9 / 10), 2); ?>$
                                </b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Payment Status</label>
                            <div class="col-md-8">
                                <b style="color: green">
                                    Confirmed
                                </b>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                        <div class="form-group">
                            <label class="control-label col-md-4">Payment Method</label>
                            <div class="col-md-8">
                                <?php
                                $obj = new paymentMethod;
                                $method = $obj->getPaymentMethod($job->payment_method);
                                echo $method->title;
                                ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>