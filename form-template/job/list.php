<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Jobs</li>
                    <?php if ($user_type != 'admin') { ?>
                        <p class="pull-right">
                            <a class="btn btn-success btn-xs" href="<?php echo make_admin_url('search'); ?>">Seek Assignment</a>
                            <a class="btn btn-success btn-xs" href="<?php echo make_admin_url('assignment', 'insert', 'insert'); ?>">Post Assignment</a>
                        </p>
                    <?php } ?>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <?php if ($jobs) { ?>
                    <div style="overflow-y: hidden">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Poster</th>
                                    <th>Title</th>
                                    <th>Complete Date</th>
                                    <th>Price ($)</th>
                                    <th>Status</th>
                                    <?php if ($user_type !== 'admin') { ?>
                                        <th>Action</th>
                                    <?php } ?>
                                    <?php if ($user_type == 'admin') { ?>
                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($jobs as $job) {
                                    $obj = new assignment;
                                    $assignment = $obj->getAssignment($job->assignment_id);
                                    ?>
                                    <tr>
                                        <td>
                                            <?php
                                            $user = get_object('user', $assignment->user_id);
                                            echo $user->username;
                                            ?>
                                        </td>
                                        <td><?php echo $assignment->title; ?></td>
                                        <td><?php echo date('m/d/Y', $assignment->post_date); ?></td>
                                        <td>$ <?php echo number_format($assignment->price, 2); ?></td>
                                        <td>
                                            <?php
                                            if ($assignment->is_active == 0) {
                                                echo 'Cancelled';
                                            } else {
                                                echo ucfirst($assignment->status);
                                            }
                                            ?>
                                        </td>
                                        <?php if ($user_type !== 'admin') { ?>
                                            <td>
                                                <?php if ($assignment->status == 'Submitted' && $assignment->is_task_approved == 0) { ?>
                                                    <a href="javascript:;" class="btn btn-primary btn-xs tool" disabled title="Pending">View</a> 
                                                <?php } else { ?>
                                                    <a href="<?php echo make_admin_url('job', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View 
                                                        <?php if ($assignment->is_task_approved != 0) { ?>
                                                            <i class="fa fa-<?php echo $assignment->is_task_approved == 1 ? 'check' : 'times' ?>"></i>
                                                        <?php } ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                        <?php if ($user_type == 'admin') { ?>
                                            <td>
                                                <a href="<?php echo make_admin_url('job', 'admin', 'admin&assignment_id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">
                                                    <?php
                                                    if ($assignment->is_task_approved == 1) {
                                                        echo '<i class="fa fa-check"></i> View';
                                                    } else if ($assignment->is_task_approved == 2) {
                                                        echo '<i class="fa fa-times"></i> view';
                                                    } else {
                                                        echo 'view';
                                                    }
                                                    ?>     
                                                </a>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    <?php } else { ?>
                        <div class="alert alert-info">
                            No Results Found.
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>

<div id="getting-started"></div>
<script>
    $("#getting-started").countdown("2016/01/01", function (event) {
        $(this).text(event.strftime('%D days %H:%M:%S'));
    });
</script>