<div class="row-fluid venmo_username" data-venmo_username="<?php echo $logged_user->venmo ?>" data-page="<?php echo $Page ?>" data-action="<?php echo $action ?>">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Profile</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <div class="alert alert-info">
                    In order to receive payment, please enter one of the following Venmo credentials:
                </div>
                <form action="" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4">Venmo Username or Phone Number</label>
                        <div class="col-md-8">
                            <input type="text" name="venmo" value="<?php echo $user->venmo ? $user->venmo : '' ?>" class="form-control" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4"></label>
                        <div class="col-md-8">
                            <button type="submit" name="venmo_submit" class="btn btn-success">Submit</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>