<style>
    input.star:checked ~ label.star:before {
        color: #FD4;
        transition: all .25s;
    }
    input.star-1:checked ~ label.star:before { color: #F62; }
    label.star:hover { transform: rotate(-15deg) scale(1.3); }
</style>
<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">Venmo Username</div>
                <div class="panel-body">
                    <?php
                    $user_job = get_object('user', $job->user_id);
                    if (isset($user_job->venmo)) {
                        echo $user_job->venmo;
                    }
                    ?>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php if ($assignment->status == 'Submitted') { ?>
                        <?php
                        $submit_user = $user = get_object('user', $job->user_id);
                        ?>
                        Submitted By <a href="<?php echo make_admin_url('profile', 'list', 'list&id=' . $submit_user->id) ?>"><?php echo $submit_user->username ?></a>
                    <?php } ?>
                </div>
                <div class="panel-body">
                    <?php if ($assignment->is_task_approved == 0) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-4">
                                <button data-assignment="<?php echo $assignment->id ?>" class="btn btn-success btn-xs scan_plagiarism">Scan Plagiarism</button>
                            </label>
                            <div class="col-md-8">
                                <div class="scan_percentage"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr class="hr_custom"/>
                    <?php } ?>
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            Submitted File
                        </label>
                        <div class="col-md-8">
                            <a href="<?php echo DIR_WS_SITE_ASSETS . 'files/' . $file->file ?>" target="_blank" class="btn btn-primary btn btn-xs" download>Download</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">
                            Action
                        </label>
                        <div class="col-md-8">
                            <?php if ($assignment->is_task_approved == 0) { ?>
                                <a href="<?php echo make_admin_url('job', 'approve', 'approve', 'id=' . $assignment_id) ?>" class="btn btn-primary btn-xs" onclick=" return confirm('Are You Sure')">Confirm</a>&nbsp;&nbsp;
                                <a href="<?php echo make_admin_url('job', 'approve', 'approve', 'id=' . $assignment_id . '&ty=dissapproved') ?>" class="btn btn-danger btn-xs">Decline</a>&nbsp;&nbsp;
                            <?php } else { ?>
                                <?php
                                if ($assignment->is_task_approved == 1) {
                                    echo 'Confirmed by you';
                                } else if ($assignment->is_task_approved == 2) {
                                    echo 'Decline by you';
                                }
                                ?>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    Posted By 
                    <?php
                    $post_user = get_object('user', $assignment->user_id);
                    ?>
                    <a href="<?php echo make_admin_url('profile', 'list', 'list&id=' . $post_user->id) ?>"><?php echo $post_user->username ?></a>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="control-label col-md-4">Assignment Title</label>
                        <div class="col-md-8">
                            <?php echo $assignment->title; ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Subject</label>
                        <div class="col-md-8">
                            <?php
                            $subjects = get_object('subjects', $assignment->subject_id);
                            echo $subjects->title;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Attached Material</label>
                        <div class="col-md-8">
                            <a href="<?php echo DIR_WS_SITE . 'assets/assignments/' . $assignment->file; ?>" class="btn btn-primary btn-xs" download>Download</a>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Payment Method</label>
                        <div class="col-md-8">
                            <?php
                            $subjects = get_object('payment_method', $assignment->payment_method);
                            echo $subjects->title;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Detailed Description of Assignment</label>
                        <div class="col-md-8">
                            <?php
                            echo $assignment->description;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Length (in pages)</label>
                        <div class="col-md-8">
                            <?php
                            echo $assignment->length_in_pages;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Price (USD)</label>
                        <div class="col-md-8">$
                            <?php
                            echo $assignment->price;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Due Date</label>
                        <div class="col-md-8">
                            <?php
                            echo $assignment->due_date;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Time Due</label>
                        <div class="col-md-8">
                            <?php
                            echo $assignment->time_due;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <div class="form-group">
                        <label class="control-label col-md-4">Any link you would like to provide? (Optional)</label>
                        <div class="col-md-8">
                            <?php
                            echo $assignment->optional_link;
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-2"></div>
</div>