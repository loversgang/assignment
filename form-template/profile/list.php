<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Profile</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <form action="" method="post" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-4">Username</label>
                        <div class="col-md-8">
                            <input type="text" name="username" value="<?php echo $logged_user->username; ?>" class="form-control" disabled/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Email Address</label>
                        <div class="col-md-8">
                            <input type="email" name="email" value="<?php echo $logged_user->email; ?>" class="form-control" disabled/>
                        </div>
                    </div>
                    <?php if ($user_type != 'admin' || $id) { ?>
                        <div class="form-group">
                            <label class="control-label col-md-4">Institute</label>
                            <div class="col-md-8">
                                <input type="text" name="institute_name" value="<?php echo $logged_user->institute_name; ?>" class="form-control" disabled/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Phone Number</label>
                            <div class="col-md-8">
                                <input type="text" name="phone" value="<?php echo $logged_user->phone; ?>" class="form-control" disabled/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Date of Birth</label>
                            <div class="col-md-8">
                                <input type="text" name="phone" value="<?php echo $logged_user->dob; ?>" class="form-control" disabled/>
                            </div>
                        </div>
                    <?php } ?> 
                    <hr />
                    <!--                        <div class="form-group">
                                                <label class="control-label col-md-4">Stripe Secret Key</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="secret" value="<?php echo $logged_user->secret; ?>" class="form-control"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-4">Stripe Publishable Key</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="publishable" value="<?php echo $logged_user->publishable; ?>" class="form-control"/>
                                                </div>
                                            </div>-->

                    <?php if ($user_type == 'admin') { ?>
                        <!--<input type="submit" name="update_profile" class="btn btn-success pull-right" value="Update"/>-->
                    <?php } ?>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>