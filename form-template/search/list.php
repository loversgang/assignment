<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Search Assignments</li>
                </ol>
            </div>
            <div class="panel-body">

                <div class="form search_from">
                    <div class="col-md-4">
                        <div class="form-group">
                            <select class="form-control subject_id">
                                <?php foreach ($subjects as $subject) { ?>
                                    <option value="<?php echo $subject->id ?>"><?php echo $subject->title ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="search_assignments btn btn-success">Search</button> 
                    </div>
                    <div class="col-md-6">
                        <h5 style="margin-top: -3px;">Choose a specific topic of assignments that you would like to complete.</h5>

                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="assignments_result">
                    <?php if ($assignments) { ?>
                        <div style="overflow-y: hidden">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Subject</th>
                                        <th>Due By</th>
                                        <th>Time Due</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($assignments as $assignment) { ?>
                                        <tr>
                                            <td><?php echo $assignment->title; ?></td>
                                            <td>
                                                <?php
                                                $obj = new subjects;
                                                $subject = $obj->getSubject($assignment->subject_id);
                                                echo $subject->title;
                                                ?>
                                            </td>
                                            <td><?php echo $assignment->due_date; ?></td>
                                            <td><?php echo $assignment->time_due; ?></td>
                                            <td>
                                                <a href="<?php echo make_admin_url('job', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-info">No Result Found!</div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>