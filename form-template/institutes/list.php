<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            Institutes
        </div>
        <div class="panel-body" style="padding: 15px 0px">
            <?php display_message(1); ?>
            <div class="col-md-4">
                <ul class="list-group">
                    <li class="list-group-item active_c">Add Institute</li>
                    <li class="list-group-item radio_btns">
                        <form class="form" enctype="multipart/form-data" method="post" action="">
                            <div class="form-group">
                                <label class="control-label">Title</label>
                                <input type="text" name="title" class="form-control" required/>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Type</label>
                                <select class="form-control" name="type">
                                    <option value="school">School</option>
                                    <option value="college">College</option>
                                    <option value="university">University</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Domain</label>
                                <input type="text" name="domain" class="form-control" placeholder="domain.com" required/>
                            </div>
                            <input type="submit" name="save_institute" value="Save" class="btn btn-success"/>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="col-md-8">
                <ul class="list-group">
                    <li class="list-group-item active_c">Institutes</li>
                    <li class="list-group-item radio_btns">
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th>Institute</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($institutes as $institute) { ?>
                                    <tr>
                                        <td>
                                            <b><?php echo $institute->title ?></b><br/>
                                            <span style="color: #999"><?php echo $institute->domain ?></span><br/>
                                            <small>
                                                <a href="<?php echo make_admin_url('institutes', 'update', 'update', 'id=' . $institute->id) ?>">Edit</a> | <a href="<?php echo make_admin_url('institutes', 'delete', 'delete', 'id=' . $institute->id) ?>">Delete</a>
                                            </small>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>