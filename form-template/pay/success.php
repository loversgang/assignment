<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li>Assignments</li>
                    <li class="active">Pay</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php
                $secret_key = 'sk_test_rZu7camB9JXrltBEjirXP8gB';
                $secret_key = 'sk_live_ve4LPyVuiBJk5YvfteyCQfJr';


                $amount = $assignment->price * 100;
                try {
                    require_once(DIR_FS_SITE . 'stripe/Stripe/lib/Stripe.php');
                    Stripe::setApiKey($secret_key);

                    $charge = Stripe_Charge::create(array(
                                "amount" => $amount,
                                "currency" => "usd",
                                "card" => $_POST['stripeToken'],
                                "description" => "Payment"
                    ));

                    $query = new assignment;
                    $query->update_field('payment_status', 1, $id);
                    echo "<h5>Your assignment posted successfully.</h5>";
                    echo $_POST['stripeEmail'];
                } catch (Stripe_CardError $e) {
                    
                }
                //catch the errors in any way you like
                catch (Stripe_InvalidRequestError $e) {
                    // Invalid parameters were supplied to Stripe's API
                } catch (Stripe_AuthenticationError $e) {
                    // Authentication with Stripe's API failed
                    // (maybe you changed API keys recently)
                } catch (Stripe_ApiConnectionError $e) {
                    // Network communication with Stripe failed
                } catch (Stripe_Error $e) {
                    // Display a very generic error to the user, and maybe send
                    // yourself an email
                } catch (Exception $e) {
                    // Something else happened, completely unrelated to Stripe
                }
                ?>
                <a href="<?php echo make_admin_url('assignment') ?>">Click Here</a>
            </div>
        </div>
        <br />
        <div class="col-md-2"></div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('.pay').click();
    });
</script>