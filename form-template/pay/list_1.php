<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li>Assignments</li>
                    <li class="active">Pay</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>     
                <div class="panel panel-default">
                    <div class="panel-heading">Pay for the Assignment</div>
                    <div class="panel-body">
                        <?php if ($assignment->payment_status == 0 && $assignment->is_active == 1) { ?>
                            <div class="form-group">
                                <label class="control-label col-md-4"></label>
                                <div class="col-md-8">
                                    <!--<button type="button" class="btn btn-primary make_payment2" data-assignment_id="<?php echo $id ?>"><i class="fa fa-usd"></i> Make Payment</button>-->
                                    <form action='<?php echo $paypal_url; ?>' method='post' name='frmPayPal1'>
                                        <input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
                                        <input type='hidden' name='cmd' value='_xclick'>

                                        <input type='hidden' name='item_name' value='Assignment'>
                                        <input type='hidden' name='item_number' value='item_number'>
                                        <input type='hidden' name='amount' value='<?php echo $assignment->price ?>'>

                                        <input type='hidden' name='no_shipping' value='1'>
                                        <input type='hidden' name='currency_code' value='USD'>
                                        <input type='hidden' name='handling' value='0'>
                                        <input type='hidden' name='cancel_return' value='<?php echo make_admin_url('assignment', 'list', 'list&payment_status=cancel&id=' . $id) ?>'>
                                        <input type='hidden' name='return' value='<?php echo make_admin_url('assignment', 'list', 'list&id=' . $id) ?>'>

                                        <input type="image" src="<?php echo DIR_WS_SITE_ASSETS . 'images/paypal_pay.gif' ?>" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" class="pay">
                                        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                    </form>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <hr class="hr_custom"/>
                        <?php } else { ?>
                            <div class="alert alert-info">Payment already made for this Assignment</div>
                        <?php } ?>
                    </div>
                    <div class="panel-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>

<script>
    $(document).ready(function () {
        $('.pay').click();
    });
</script>