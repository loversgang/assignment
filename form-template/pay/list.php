<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li>Assignments</li>
                    <li class="active">Pay</li>
                </ol>
            </div>
            <div class="panel-body">

                <br />
                <?php
                $publish_key = 'pk_test_vUT4S9i1yUGa2YOTlozQ7Zhl';
                $publish_key = 'pk_live_Mhv70rbk9WUozcPGl3WfM6iL';


                $data_amount = number_format($assignment->price, 2);
                $data_amount = $assignment->price * 100;
                ?>
                <!--data-image="<?php echo DIR_WS_SITE_ASSETS . 'images/logo.png' ?>"-->
                <form method="POST" action="<?php echo make_admin_url('pay', 'success', 'success&id=' . $id) ?>">
                    <script
                        src="https://checkout.stripe.com/checkout.js "class="stripe-button"
                        data-key="<?php echo $publish_key ?>"
                        data-name="<?php echo SITE_NAME ?>"
                        data-description="Payment"
                        data-amount="<?php echo $data_amount ?>">
                    </script>
                </form>
                <!--data-currency="usd"-->
                <br />
                <br />
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('.pay').click();
        });
    </script>