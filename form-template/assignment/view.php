<style>
    input.star:checked ~ label.star:before {
        color: #FD4;
        transition: all .25s;
    }
    input.star-1:checked ~ label.star:before { color: #F62; }
    label.star:hover { transform: rotate(-15deg) scale(1.3); }
</style>
<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li><a href="<?php echo make_admin_url('assignment'); ?>">Assignments</a></li>
                    <li class="active">Assignment Details</li>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <?php ob_start(); ?>
                <div class="form-group">
                    <label class="control-label col-md-4">Assignment Title</label>
                    <div class="col-md-8">
                        <?php echo $assignment->title; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Subject</label>
                    <div class="col-md-8">
                        <?php
                        $obj = new subjects;
                        $subject = $obj->getSubject($assignment->subject_id);
                        echo $subject->title;
                        ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Attached Material</label>
                    <div class="col-md-8">
                        <?php if ($assignment->file) { ?>
                            <a class="btn btn-primary btn-xs view" href="<?php echo DIR_WS_SITE_ASSETS . 'assignments/' . $assignment->file ?>" download>Download</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Payment Method</label>
                    <div class="col-md-8">
                        <?php
                        $obj = new paymentMethod;
                        $method = $obj->getPaymentMethod($assignment->payment_method);
                        echo $method->title;
                        ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Detailed Description of Assignment</label>
                    <div class="col-md-8">
                        <?php echo $assignment->description; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Length (in pages)</label>
                    <div class="col-md-8">
                        <?php echo $assignment->length_in_pages; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Price (USD)</label>
                    <div class="col-md-8">$
                        <?php echo number_format($assignment->price, 2); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Due Date</label>
                    <div class="col-md-8">
                        <?php echo $assignment->due_date; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Time Due</label>
                    <div class="col-md-8">
                        <?php echo $assignment->time_due; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <div class="form-group">
                    <label class="control-label col-md-4">Any link you would like to provide? (Optional)</label>
                    <div class="col-md-8" style="word-wrap: break-word;">
                        <a href="<?php echo $assignment->optional_link; ?>"><?php echo $assignment->optional_link; ?></a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr class="hr_custom"/>
                <?php $html = ob_get_clean(); ?>
                <?php if ($assignment->status == 'active') { ?>
                    <div class="form">
                        <?php echo $html; ?>
                        <?php if ($assignment->is_active == 1) { ?>
                            <form class="form" action="" method="post">
                                <input type="hidden" name="assignment_id" value="<?php echo $assignment->id; ?>"/>
                                <input type="submit" name="cancel_assignment" value="Cancel assignment" class="btn btn-danger pull-right"/>
                            </form>
                        <?php } else { ?>
                            <form class="form" action="" method="post">
                                <input type="hidden" name="assignment_id" value="<?php echo $assignment->id; ?>"/>
                                <input type="submit" name="undo_cancel_assignment" value="Undo Cancel assignment" class="btn btn-danger pull-right"/>
                            </form>
                        <?php } ?>
                    </div>
                <?php } elseif ($assignment->status == 'Being Done') { ?>
                    <div class="form-group">
                        <label class="control-label col-md-4">Assignment Accepted By</label>
                        <div class="col-md-8">
                            <b>
                                <?php
                                $obj = new job;
                                $job = $obj->getJobByAssignmentId($assignment->id);
                                $obj = new user;
                                $user = $obj->getUser($job->user_id);
                                echo '<b style="color:red">' . ucfirst($user->first_name) . ' ' . ucfirst($user->last_name) . ' (' . $user->username . ')</b>'
                                ?>
                            </b>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr class="hr_custom"/>
                    <?php echo $html; ?>
                <?php } elseif ($assignment->status == 'Submitted') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Submitted By <?php echo $submitter_name ?></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <?php if ($assignment->is_task_approved == 2) { ?>
                                    <div class="alert alert-info">Declined by a Unity representative</div>
                                <?php } else { ?>
                                    <label class="control-label col-md-4">Post Rating</label>
                                    <div class="col-md-8">
                                        <div class="rating pull-left" data-id="<?php echo $_GET['id'] ?>">
                                            <input class="star star-5" id="star-5-2" type="radio" name="star"/>
                                            <label class="star star-5 five" for="star-5-2" <?php echo $job->rating > 4 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                            <input class="star star-4" id="star-4-2" type="radio" name="star"/>
                                            <label class="star star-4 four" for="star-4-2" <?php echo $job->rating > 3 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                            <input class="star star-3" id="star-3-2" type="radio" name="star"/>
                                            <label class="star star-3 three" for="star-3-2" <?php echo $job->rating > 2 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                            <input class="star star-2" id="star-2-2" type="radio" name="star"/>
                                            <label class="star star-2 two" for="star-2-2" <?php echo $job->rating > 1 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                            <input class="star star-1" id="star-1-2" type="radio" name="star"/>
                                            <label class="star star-1 one" for="star-1-2" <?php echo $job->rating > 0 ? 'style="content: \f005;color: #FD4;"' : '' ?>></label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr class="hr_custom"/>
                                    <div class="form-group">
                                        <label class="control-label col-md-4">
                                            Submitted File
                                        </label>
                                        <div class="col-md-8">
                                            <a href="<?php echo DIR_WS_SITE_ASSETS . 'files/' . $file->file ?>" class="btn btn-primary btn btn-xs" download>Download</a>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr class="hr_custom"/>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">Posted Assignment</div>
                        <div class="panel-body">
                            <?php echo $html; ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>