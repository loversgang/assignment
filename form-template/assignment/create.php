<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li><a href="<?php echo make_admin_url('assignment'); ?>">Assignments</a></li>
                    <li class="active">Post Assignment</li>
                </ol>
            </div>
            <div class="panel-body">
                <div class="col-md-4"></div>
                <div class="col-md-8">
                    <h5 style="margin-top: -3px;">This is where you provide the details of an assignment for other users to complete at the price you want.</h5>
                </div>
                <br /><br />
                <?php display_message(1); ?>
                <form action="" method="post" class="form-horizontal" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label col-md-4">Assignment Title <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="title" value="" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Subject <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="subject_id">
                                <?php foreach ($subjects as $subject) { ?>
                                    <option value="<?php echo $subject->id; ?>"><?php echo $subject->title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Attach Material</label>
                        <div class="col-md-8">
                            <input type="file" name="file" required />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Payment Method <span class="required">*</span></label>
                        <div class="col-md-8">
                            <select class="form-control" name="payment_method">
                                <?php foreach ($paymentMethods as $method) { ?>
                                    <option value="<?php echo $method->id; ?>"><?php echo $method->title; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Detailed Description of Assignment <span class="required">*</span></label>
                        <div class="col-md-8">
                            <textarea rows="5" class="form-control" name="description" required></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Length (in pages) <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="number" name="length_in_pages" value="" class="form-control" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Payment Amount (USD) <span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="number" name="price" step="any" value="" class="form-control price" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Due Date <span class="required">*</span></label>
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <select class="form-control month" name="month" id="dob">

                                        <?php for ($m = 1; $m <= 12; $m++) { ?>

                                            <?php
                                            $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                            ?>
                                            <option value="<?php echo $m ?>"><?php echo $month ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control date" name="date" id="dob">
                                        <?php for ($x = 1; $x <= 31; $x++) { ?>
                                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control year" name="year" id="dob">
                                        <?php for ($x = $year; $x <= $year + 10; $x++) { ?>
                                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Time Due<span class="required">*</span></label>
                        <div class="col-md-8">
                            <input type="text" name="time_due" value="" class="form-control" id="datetimepicker3" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Any link you would like to provide? (Optional)</label>
                        <div class="col-md-8">
                            <input type="text" name="optional_link" value="" class="form-control"/>
                        </div>
                    </div>
                    <input type="submit" name="post_assignment" class="btn btn-success pull-right post_assignment_submit" value="Post Assignment"/>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>