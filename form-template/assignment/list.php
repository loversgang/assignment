<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <ol class="breadcrumb">
                    <li><a href="<?php echo make_admin_url('home'); ?>">Home</a></li>
                    <li class="active">Assignments</li>
                    <?php if ($user_type != 'admin') { ?>
                        <p class="pull-right">
                            <a class="btn btn-success btn-xs" href="<?php echo make_admin_url('assignment', 'insert', 'insert'); ?>">Post Assignment</a>
                        </p>
                    <?php } ?>
                </ol>
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <?php if ($assignments) { ?>
                    <div style="overflow-y:hidden">
                        <table class="table">
                            <thead>
                                <tr>
                                    <?php if ($user_type == 'admin') { ?>
                                        <th>Username</th>
                                    <?php } ?>
                                    <th>Title</th>
                                    <th>Date Posted</th>
                                    <th>Price ($)</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <?php if ($user_type != 'admin') { ?>
                                        <th>Action</th>
                                    <?php } ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($assignments as $assignment) { ?>
                                    <tr>
                                        <?php if ($user_type == 'admin') { ?>
                                            <td>
                                                <?php
                                                $user = get_object('user', $assignment->user_id);
                                                echo $user->username;
                                                ?>
                                            </td>
                                        <?php } ?>
                                        <td><?php echo $assignment->title; ?></td>
                                        <td><?php echo date('m/d/Y', $assignment->post_date); ?></td>
                                        <td>$ <?php echo number_format($assignment->price, 2); ?></td>
                                        <td>
                                            <?php
                                            if ($assignment->is_active == 0) {
                                                echo 'Cancelled';
                                            } else {
                                                echo ucfirst($assignment->status);
                                            }
                                            ?>
                                        </td>
                                        <?php if ($user_type != 'admin') { ?>
                                            <td>
                                                <?php if ($assignment->status == 'Submitted') { ?>
                                                    <?php if ($assignment->is_task_approved == 0) { ?>
                                                        <a class="btn btn-primary btn-xs tool" title="Pending" disabled>View</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo make_admin_url('assignment', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View
                                                            <?php if ($assignment->is_task_approved != 0) { ?>
                                                                <i class="fa fa-<?php echo $assignment->is_task_approved == 1 ? 'check' : 'times' ?>"></i>
                                                            </a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <a href="<?php echo make_admin_url('assignment', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View</a>
                                                <?php } ?>
                                            </td>
                                        <?php } ?>
                                        <td><a href="<?php echo make_admin_url('assignment', 'delete', 'delete&id=' . $assignment->id) ?>" class="btn btn-primary btn-xs" onclick="return confirm('Are You Sure')">Delete</a></td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-info">
                        No Results Found.
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>