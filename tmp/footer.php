<?php if ($current_file == 'index.php' || $current_file == 'about.php' || $current_file == 'faq.php') { ?>
    <div class="pull-right godaddy" <?php echo $current_file == 'about.php' ? 'style="margin-top: 20px;"' : '' ?>>
        <span id="siteseal"><script type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=9Z82XUFN2DtzHr0e5mBGD12CmOAGuKgGfED2Cw920a3306QsftA8QDUoGvqi"></script></span>
    </div>
<?php } ?>
<div class="col-md-12 b21" style="<?php echo ($current_file == 'index.php' || $current_file == 'about.php' || $current_file == 'faq.php') ? 'margin-top:0%"' : '' ?>">
    <ul class="nav nav-pills" style="align-items: center;display: flex;justify-content: center;">
        <li><a href="javascript:;" class="terms_conditions">Terms & Conditions</a></li>
        <li><a href="javascript:;" class="privacy_policy">Privacy Policy</a></li>
        <li><a href="javascript:;" class="confidentiality_policy">Confidentiality Policy</a></li>
        <li><a href="javascript:;" id="contact_us" class="contact_us">Contact Us</a></li>
    </ul>
</div>
<div id="login_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><a href="javascript:;" id="signup_show">Create a new account</a></h4>
            </div>
            <div class="modal-body">
                <div class="login">
                    <form class="omb_loginForm" action="" autocomplete="off" method="POST">
                        <div class="input-group">

                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control" name="username" placeholder="username">
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            <input  type="password" class="form-control" name="password" placeholder="password">
                        </div>
                        <span class="help-block"></span>
                        <input type="submit" name="login_user" value="Login" class="btn btn-success btn-block"/>
                    </form>
                    <br />
                    <a href="forgot_password.php">Forgot your password?</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary close_login" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div id="sign_popup" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><a href="javascript:;" id="login_show">Already have an account? Log in</a></h4>
            </div>
            <div class="modal-body">
                <div class="signup">
                    <form class="form" action="" method="post">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" class="form-control signup_username" name="username" placeholder="username">
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-graduation-cap" style="font-size: 8px;"></i></span>
                            <input  type="text" class="form-control institute_name" name="institute_name" placeholder="name of college">
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-at" style="font-size: 12px;"></i></span>
                            <input  type="text" class="form-control" name="email" placeholder="email address" />
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key" style="font-size: 10px;"></i></span>
                            <input  type="password" class="form-control password_s" name="password" placeholder="password"  />
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-key" style="font-size:10px"></i></span>
                            <input  type="password" class="form-control confirm_password_s" name="confirm_password" placeholder="confirm password" />
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-mobile" style="font-size: 22px;"></i></span>
                            <input  type="text" class="form-control" name="phone" placeholder="phone number" />
                        </div>
                        <span class="help-block"></span>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-birthday-cake" style="font-size: 10.5px;"></i></span>
                            <div class="row">
                                <div class="col-md-3">
                                    <span style="position:absolute;margin-left: 10px;margin-top:8px">DOB</span>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control month" name="month" id="dob" style="margin-left: -60px;">
                                        <?php for ($m = 1; $m <= 12; $m++) { ?>

                                            <?php
                                            $month = date('F', mktime(0, 0, 0, $m, 1, date('Y')));
                                            ?>
                                            <option value="<?php echo $m ?>"><?php echo $month ?></option>
                                        <?php } ?>

                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control date" name="date" id="dob" style="margin-left: -27px;">
                                        <?php for ($x = 1; $x <= 31; $x++) { ?>
                                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control year" name="year" id="dob">
                                        <?php for ($x = 2001; $x >= 1990; $x--) { ?>
                                            <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <span class="help-block"></span>
                        <a href="javascript:;" class="post_assignment" style="display : none">Read Terms and conditions</a>
                        <input type="button" name="signup_user" value="Sign Up" class="btn btn-success btn-block"/>
                        <input type="submit" name="signup_user" value="Sign Up" class="btn btn-success btn-block signup_submit" style="display: none"/>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary close_signup" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade terms_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Terms & Conditions</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <!--<code>Move scroll to read the document</code>-->
            <!--<button type="button" class="btn btn-primary tos_class" data-tos="" data-page="<?php echo $_GET['Page'] ?>">I agree</button>-->
                <button type="button" class="btn btn-primary agree_submit">I Agree</button>
            </div>
        </div>
    </div>
</div>

<?php
require 'terms.php';
require 'privacy.php';
require 'confidentiality.php';
require 'contact.php';
if (!$admin_user->is_logged_in()) {
    require DIR_FS_SITE . 'user_registeration.php';
}
require 'script.inc.php';
?>