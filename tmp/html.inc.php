<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <!--<meta name="viewport" content="width=1280, initial-scale=1"/>-->
        <!--<meta name="viewport" content="width=1366, initial-scale=1"/>-->
        <meta name="viewport" content="width=1366, user-scalable=yes" />
        <title><?php echo SITE_NAME ?></title>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>bootstrap.min.css"/>
        <!--<link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>responsive.css" type="text/css"/>-->
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>style.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>style1.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>responsive_header.css" type="text/css"/>
        <!--<link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>res.css" type="text/css"/>-->
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_CSS ?>stack.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>toastr/toastr.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>fa-fa/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/jquery.dataTables.min.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo DIR_WS_SITE_PLUGINS ?>datepicker/datepicker.min.css" type="text/css"/>
        <link href='https://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css' />
        <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css' />
        <link href='https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css' rel='stylesheet' type='text/css' />
        <script src="<?php echo DIR_WS_SITE_JS ?>jquery.min.js" ></script>
        <script src="<?php echo DIR_WS_SITE_PLUGINS ?>toastr/toastr.js" ></script>
    </head>
