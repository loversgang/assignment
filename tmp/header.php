<?php
$current_file = basename($_SERVER['PHP_SELF']);
$user_type = $_SESSION['admin_session_secure']['user_type'];
require 'tmp/html.inc.php';
?>
<body id="main_c" data-first-time-login="<?php echo isset($_SESSION['admin_session_secure']['welcome']) ? $_SESSION['admin_session_secure']['welcome'] : '' ?>">
    <div id="masthead" class="menu navbar navbar-static-top header-logo-center-menu-below oxy-mega-menu navbar-not-sticky text-caps header-bg-top" role="banner">
        <div class="logo-navbar container-logo header-bg-bottom">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".main-navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo DIR_WS_SITE; ?>" class="navbar-brand" id="site_url">
                        <img src="<?php echo DIR_WS_SITE_IMAGE ?>logo.png" />
                    </a>
                    <div class="logo-sidebar">
                        <?php
                        if ($admin_user->is_logged_in()) {
                            $obj = new user;
                            $user = $obj->getUser();
                            ?>
                            <b>
                                <?php if ($Page !== 'home') { ?>
                                    Hello <?php echo $user->username; ?><br/>
                                    <a href="<?php echo make_admin_url('logout'); ?>">Log out</a>
                                <?php } else { ?>
                                    <?php if ($user_type == 'admin') { ?>
                                        <a href="<?php echo make_admin_url('job'); ?>">Return to Dashboard</a>
                                    <?php } else { ?>
                                        <a href="<?php echo make_admin_url('center'); ?>">Return to Dashboard</a>
                                    <?php } ?>
                                <?php } ?>
                            </b>
                        <?php } else { ?>
                            <a href="<?php echo DIR_WS_SITE ?>"id="login" class="login-sign-style">Log in</a> &nbsp; &nbsp;/ &nbsp; <a href="javascript:;" id="login-2" class="login-sign-style">Sign Up</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="nav-container clearfix">
                <nav class="collapse navbar-collapse main-navbar" role="navigation">
                    <div class="menu-container">
                        <ul id="menu-mega-menu" class="nav navbar-nav">
                            <?php if ($Page == 'home') { ?>
                                <li class="active"><a href="<?php echo DIR_WS_SITE; ?>" <?php echo $current_file == 'index.php' ? 'style="color: #242424;"' : '' ?>>Home <span class="border-left hidden-xs hidden-sm"  style="color: #242424;">|</span> </a></li>
                            <?php } ?>
                            <?php if ($admin_user->is_logged_in()) { ?>
                                <?php if ($user_type != 'admin') { ?>
                                    <?php if ($Page != 'home') { ?>
                                        <li style="margin-left: 60px;"><a href="<?php echo make_admin_url('center', 'list', 'list') ?>" <?php echo $Page == 'center' ? 'style="color: #242424"' : '' ?>>Assignment Center <span class="border-left hidden-xs hidden-sm" style="color:#242424">|</span> </a></li>
                                        <li style="margin-left: 60px;"><a href="<?php echo make_admin_url('assignment', 'insert', 'insert') ?>" <?php echo ($Page == 'assignment' && $action == 'insert') ? 'style="color: #242424"' : '' ?>>Post <span class="border-left hidden-xs hidden-sm" style="color:#242424">|</span> </a></li>
                                        <li style="margin-left: 60px;"><a href="<?php echo make_admin_url('search', 'list', 'list') ?>" <?php echo $Page == 'search' ? 'style="color: #242424"' : '' ?>>Seek <span class="border-left hidden-xs hidden-sm" style="color:#242424">|</span> </a></li>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($admin_user->is_logged_in()) { ?>
                                <?php if ($Page != 'home') { ?>
                                    <li class="dropdown" style="margin-left: 60px;">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" <?php echo ($Page == 'setting' || $Page == 'profile' || (($Page == 'assignment' && $action == 'list' || $action == 'view')) || $Page == 'job' || $Page == 'job' || $Page == 'paper') ? 'style="color: #242424"' : '' ?>>Account <span class="caret"></span></a>
                                        <ul class="dropdown-menu" style="background-color: #FFFFF;">
                                            <li><a href="<?php echo make_admin_url('setting'); ?>">Change Password</a></li>
                                            <li><a href="<?php echo make_admin_url('profile'); ?>">Profile</a></li>
                                            <li><a href="<?php echo make_admin_url('assignment'); ?>"><?php echo $user_type != 'admin' ? 'My Posts' : 'Assignments' ?></a></li>
                                            <li><a href="<?php echo make_admin_url('job'); ?>"><?php echo $user_type != 'admin' ? 'Accepted Tasks' : 'Jobs' ?></a></li>
                                            <?php if ($user_type == 'admin') { ?>
                                                <li><a href="<?php echo make_admin_url('user'); ?>">Users</a></li>
                                            <?php } else { ?>
                                                <li><a href="<?php echo make_admin_url('paper'); ?>">Completed Papers</a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($Page == 'home') { ?>
                                <li style="margin-left:60px"><a href="<?php echo DIR_WS_SITE; ?>about.php"  <?php echo $current_file == 'about.php' ? 'style="color:#242424"' : '' ?>>About Unity <span class="border-left hidden-xs hidden-sm" style="color:#242424">|</span> </a></li>
                                <li style="margin-left:60px"><a href="<?php echo DIR_WS_SITE; ?>faq.php" <?php echo $current_file == 'faq.php' ? 'style="color:#242424"' : '' ?>>FAQ</a></li>

                                <?php if ($admin_user->is_logged_in()) { ?>
                                    <li class="hidden-lg hidden-md"><a href="<?php // echo make_admin_url('logout');                                   ?>"></a></li>
                                    <?php } ?>

                                <?php if (!$admin_user->is_logged_in()) { ?>
                                                                                                                                                                                                                                                                                                                                    <!--<li class="hidden-lg hidden-md"><a href="<?php echo DIR_WS_SITE ?>" id="login">Log in</a></li>-->
                                    <!--                                    <li class="hidden-lg hidden-md">
                                                                            <a href="javascript:;" id="login-2">Sign Up</a>
                                                                        </li>-->
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
    </div>      