<script src="<?php echo DIR_WS_SITE_JS ?>bootstrap.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_JS ?>jquery.validationEngine-en.js" ></script>
<script src="<?php echo DIR_WS_SITE_JS ?>jquery.validationEngine.js" ></script>
<script src="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/jquery.dataTables.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_PLUGINS ?>datatables/dataTables.bootstrap.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_PLUGINS ?>datepicker/datepicker.min.js" ></script>
<script src="<?php echo DIR_WS_SITE_JS ?>main.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.12.0/moment.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js" ></script>
</body>
</html>