<?php
$root = dirname(__DIR__);
require_once($root . "/include/config/config.php");
include_once(DIR_FS_SITE . 'include/functionClass/assignmentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paymentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paypalClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/jobClass.php');
$user_id = $_SESSION['admin_session_secure']['user_id'];
$user_type = $_SESSION['admin_session_secure']['user_type'];
$obj = new user;
$logged_user = $obj->getUser($user_id);
if (isset($_POST['action'])) {
    extract($_POST);
    if ($action == 'user_signin') {
        $arr = array();
        if ($user = validate_user('user', $data_array)) {
            $user->user_type = 'user';
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            if ($user->is_auto_login == 1) {
                setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
            }
            $arr['status'] = 'success';
        } else {
            $arr['status'] = 'error';
        }
        echo json_encode($arr);
    }
    if ($action == 'user_signup') {
        $obj = new user;
        $usernameExists = $obj->checkField('username', $data_array['username']);
        if (!$usernameExists) {
            $obj = new user;
            $emailExists = $obj->checkField('email', $data_array['email']);
            if (!$emailExists) {
                $obj = new user;
                $user_id = $obj->saveUser($data_array);
                $object = new user;
                $user = $object->getUser($user_id);
                SendEmail($Subject, $user->email, SITE_EMAIL, $FromName, $Message);
                $user->user_type = 'user';
                $admin_user->set_admin_user_from_object($user);
                update_last_access($user->id, 1);
                if ($user->is_auto_login == 1) {
                    setcookie("kissconnection", $user->username, time() + (365 * 24 * 60 * 60), "/");
                }
            } else {
                echo 'emailExists';
            }
        } else {
            echo 'usernameExists';
        }
    }
    if ($action == 'post_assignment') {
        ?>
        <div class="bluebox" style="height: 300px;overflow-y: scroll;">
            <h2 class="licence"></h2>
            <p>
                YOUR USE OF THIS WEBSITE AND SYSTEM CONSTITUTES YOUR
                AGREEMENT TO THE FOLLOWING TERMS AND CONDITIONS. IF YOU
                DISAGREE WITH ANY OF THE FOLLOWING TERMS AND CONDITIONS,
                PLEASE REFRAIN FROM USING OUR WEBSITE AND SERVICES.
                THIS WEBSITE AND SYSTEM IS ONLY AVAILABLE FOR USE IF YOU ARE A
                CURRENT STUDENT OF A HIGHER EDUCATIONAL INSTITUTE.
                OTHERWISE, USE OF ANY PRODUCTS OR SERVICES OFFERED BY THIS
                WEBSITE IS STRICTLY PROHIBITED.
                BY SELECTING A SERVICE ON THIS WEBSITE OR MOBILE APPLICATION
                YOU ACKNOWLEDGE HAVING READ, UNDERSTOOD, AND AGREED TO
                ALL THESE TERMS AND CONDITIONS TO THEIR FULLEST EXTENT. BY
                SELECTING A SERVICE AND / OR PAYMENT, YOU ALSO AGREE TO BE
                LEGALLY BOUND BY THESE TERMS AND CONDITIONS, IN WHICH THE
                AGREEMENT BETWEEN YOU AND UNITY MLA, LLC, BECOMES EFFECTIVE
                IMMEDIATELY.
            </p>
            <h2 class="licence">INTERPRETATION</h2>
            <p>
                In this document:
                “Website” means UNITYFORCOLLEGE.com.
                "System" means the Unity Mobile Application
                “Client”, “You” or “Yours” mean and refer to you and / or any other person
                selecting the service through the website on your behalf.
                “Company”, “We” or “Our” mean and refer to Unity MLA, LLC , a company
                registered under the laws of the United States of America.
                “Product” – an original assignment, essay, paper, and / or other written product,
                which is prepared and delivered to the client in accordance with the selection of
                service to not be distributed in any possible way for academic credit or for the aid
                in obtaining a degree at a higher educational Institute.
                “Order” refers to the option of posting an assignment and means a written
                description on a standard electronic form, filled and submitted online by the client
                to our website. "Order" specifies the scope of work and other requirements of the
                customer regarding the product.
            </p>
            <h2 class="licence">OUR SERVICES</h2>
            <p>
                By posting or seeking an assignment and / or payment, you are purchasing the product or receiving a payment for the order once completed and selecting a service strictly for your personal, non-commercial, use not intended to be submitted for academic credit or advancement in obtaining a degree at a higher educational institute.All our products are drafted by current college students of accredited higher educational institutes within the United States of America, of whom transfer all rights and ownership of the products to the company. All products are nonrefundable and come with no warranties, expressed or implied. By seeking a service and / or payment, you are legally complying to complete an assignment within the given time frame of the assignment to be feasible to pass a plagiarism check software maintaining complete originality in addition to transferring all rights and ownership of the product to the company.-
                Additional detailed information about our services can be found on our FAQ
                page. Please note, it is your obligation to read these Terms and Conditions and
                FAQ page, prior to selecting your service and / or payment to our website.
            </p>
            <h2 class="licence">COPYRIGHT & PERSONAL USE</h2>
            <p>The products this company delivers are completely original. The full copyright
                pertaining to products and other materials delivered is retained by the company
                and / or its affiliates and partners.
                The purpose of delivered products, and other materials available from this
                website, is strictly for personal, non-commercial use and is not intended for any
                form of achievement for academic credit or the assistance in obtaining a degree
                from a higher educational institute. By selecting a service with our company, you
                agree not to not display, distribute, modify, publish, reproduce, and/or transmit
                any of the products and services offered of our company, Neither will you,
                without prior written consent, exploit the products, services, assignments, and/or
                create derivative works from the contents provided by this website or the
                system.
                By selecting a service, you also agree to defend, indemnify, and hold harmless
                the company for any and all unauthorized use made of any material, product,
                content, and/or service available from this website and the company’s mobile
                application. Any unauthorized use of delivered products and / or content of this
                website can subject our customers to civil or criminal penalties.</p>
            <h2 class="licence">NO REFUND</h2>
            <p>All products this company offers are non-refundable, and come with no
                warranties, expressed or implied.
                No refund can be granted to the client under any circumstances once the order is
                completed, unless specifically stated herein.
                In certain cases, the company may provide a refund at its own discretion.
                We absolutely do not guarantee that our system or website will aid you in
                achieving a desirable grade or academic credit in any circumstances as stated in
                these agreed upon terms and conditions.</p>
            <h2 class="licence">PENALTY SYSTEM</h2>
            <p>You	agree	to	complete	the	assignment	upon	accepting	to	complete	the	assignment.	In	
                the	situation	in	which	you	don't	complete	the	assignment,	you	will	receive	a	message	
                outlining	the	situation	on	the	first	offense.	Cases	in	which	the	same	situation	arises	
                more	than	once	the	client	will	be	given	a	final	offense	notice	stating	the	client	is	
                prohibited	to	use	the	service	and	will	be	discarded	as	a	client	unless	there	is	written	
                communication	and	written	authorization	by	Unity.	A	notice	of	appeal	to	this	penalty	
                system	may	be	submitted	to	Unity	in	which	membership	will	be	under	Unity's	
                discretion.	Unity	reserves	the	right	to	choose	not	to	give	penalties	on	a	situational	basis	
                and	reserves	the	right	to	change	this	policy.</p>
            <h2 class="licence">NO PLAGIARISM</h2>
            <p>By agreeing to these terms and conditions, you acknowledge that the company
                reserves the right to cancel any service request, assignment, agreement,
                arrangement or contract with any person who condones or attempts to pass
                plagiarized products as original.
                You also agree that any product delivered by the company may not be disclosed
                to any third parties, nor distributed in any way for payment or for any other
                purpose. You also acknowledge that if the company suspects that the delivered
                product has been distributed or used by the customer in any form of plagiarism,
                the company reserves the right to refuse to carry out any further work and
                services with this client.
                It is not acceptable for customers to put their own name on the product we
                deliver. The custom written samples are provided by Unity MLA, LLC, for
                research purposes only and can not be used as a substitute of your own writing.
                It can only be used as a model or work cited, from which you can learn how to
                draft your own research properly or take inspiration for your own thinking. Entire
                parts of the research provided by our company may be used in customer's
                original piece of writing only if properly cited or paraphrased. Please refer to your
                University definition of plagiarism for acceptable use of source material.
                Our company does not condone, encourage, or knowingly take part in plagiarism
                or any other acts of academic dishonesty or fraud at ANY TIME. We strongly
                adhere to and abide by all copyright laws, and will not knowingly allow any client
                to carry out plagiarism or violate copyright laws.
                Neither the company nor any of its affiliates and / or partners shall be liable for
                any inappropriate, illegal, unethical, or otherwise wrongful use of the products
                and / or other written material received from our website. This includes and is not
                limited to plagiarism, expulsion, academic probation, loss of scholarships / titles /
                awards / prizes / grants / positions, lawsuits, poor grading, failure, suspension, or
                any other disciplinary or legal actions outside the intended use of this system and
                /or website expressed in these terms.
                The clients who post, request, seek, and / or complete assignments from our
                website are solely responsible for any and all disciplinary actions arising from the
                improper, unethical, inappropriate and / or illegal use of material outside of the
                intended use for this system and/or website.</p>
            <h2 class="licence">LINKS DISCLAIMER</h2>
            <p>Even though this website can be linked to others, we do not in any way
                approve, endorse, certify or sponsor linked sites unless specifically stated
                herein. The company is not the owner of, is not responsible for, and does
                not control any content of any website linked to our website. By agreeing
                to these terms and conditions, you acknowledge that linking to other
                websites is at your users’ own risk.</p>
            <h2 class="licence">PRIVACY & SECURITY</h2>
            <p>Please refer to our Privacy Statement posted on this website for detailed
                explanation of our company's policies and practices regarding collection, storage
                and use of online guests’ information. For more information about the security of any personal information you submit when you register,select a service with this
                website, and proceed with the payment, please refer to the Privacy Policy webpage</p>
            <h2 class="licence">WARRANTIES</h2>
            <p>By selecting a service and proceeding with payment, you acknowledge and that
                you are in complete understanding and agreement with the statements above, as
                well as each of the following:
                • All products are provided entirely as assistance examples for research,
                reference, and / or for the purposes of learning how to write a paper in a
                proper manner and in a particular citation style (MLA, APA, Chicago,
                Turabian, Harvard, and so forth).
                • The proper citation is required for any information and / or ideas used from the
                product. The act of using our written materials to attain higher marks
                within education institutions is never allowed and the company does not
                offer you guarantee for results.
                • You comply and agree to the terms wherein this website acquires payment
                which is for the work and time involved to gather, organize, correct, edit,
                post, and deliver reference materials, in addition to the maintenance,
                administration and advertising involved in regards to the company’s
                system and/or website .
                • Aside from a rational number of copies for non-commercial and/or personal
                use, you may not display, distribute, modify, publish, reproduce, and/or
                transmit any of our products or services. Neither will you, without prior
                written consent, exploit the products, services, assignments, and/or create
                derivative works from the contents provided by this website or the
                system.
                • All services, assignments, written work, contents, and products are acquired
                from current college students who enter in a "Work-For-Hire" agreement
                subjected to federal and State laws. Once the product is acquired there is
                a transfer of all rights and ownership to the company and / or its affiliates
                and partners.
                • Instantly after conducted research by yourself and/ or reference use of the
                material is complete, you comply to the agreement of destroying all
                delivered products, assignments, written work, and contents. No copies
                shall be made for distribution, and no parts of any product shall be used
                without proper citation.
                • You agree to receive emailed information for your password for your first time
                registering as client wherein the password is required to be changed upon
                first use of this website or the system as a registered client.
                • You agree to receive emailed promotional information about deals and online
                events organized by the company. You have the right to unsubscribe from
                receiving this kind of information directly, through the e-mail marketing
                distributor.
                • You agree to completing the assignment if chosen to complete the assigment
                and agree to pay the fees associated with failing to complete the
                assignment.</p>
            <h2 class="licence">WARRANTY DISCLAIMER</h2>
            <p>THE COMPANY MAKES NO REPRESENTATIONS OR WARRANTIES WITH
                REGARD TO THE WEBSITE, SYSTEM, OR ANY MATERIALS HEREIN,
                WHETHER EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
                INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF
                MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT
                OR ANY IMPLIED WARRANTY ARISING OUT OF COURSE
                OF PERFORMANCE, COURSE OF DEALING OR USAGE OF TRADE.
                IN ADDITION, THE COMPANY MAKES NO REPRESENTATION THAT THE
                OPERATION OF THE WEBSITE, SYSTEM, AND ITS SERVICE WILL BE
                UNINTERRUPTED OR ERROR-FREE. THE COMPANY SHALL NOT BE HELD
                LIABLE FOR THE CONSEQUENCES OF ANY INTERRUPTIONS OR ERRORS
                ON THE WEBSITE OR SYSTEM. IT IS YOUR RESPONSIBILITY TO ANALYZE
                THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY OPINION,
                ADVICE, INFORMATION, OR OTHER CONTENT PROVIDED IN
                CONNECTION WITH THE SERVICE OR OTHERWISE MADE AVAILABLE
                THROUGH THE WEBSITE OR SYSTEM. PLEASE SEEK THE ADVICE OF
                PROFESSIONALS, AS APPROPRIATE, REGARDING THE EVALUATION OF
                ANY SUCH OPINION, ADVICE, INFORMATION OR OTHER CONTENT.</p>
            <h2 class="licence">LIMITATION OF LIABILITY</h2>
            <p>By acknowledging the above terms and conditions, you agree to release and
                hold the company and its employees, shareholders, officers, agents,
                representatives, directors, affiliates, promotion, subsidiaries, advertising and fulfillment agencies, any third-party providers or sources of information or data and legal advisers (the “Company’s Affiliates”) harmless from any and all losses,rights, damages, claims, and actions of any kind, arising from or related to the products, including but not limited to: (a) telephone, electronic, hardware or software, Internet, network, email, or computer malfunctions, failures or difficulties of any kind; (b) failed, garbled, incomplete or delayed computer transmissions; (c) any condition caused by events beyond the control of the company, that might cause the product to be corrupted, delayed or disrupted; (d) any injuries, damages or losses of any sort arising in connection with, or as a result of, utilizing our services in the scope of business or; (e) any printing or typographical errors in any materials associated with our services. In addition, you agree to defend, indemnify, and hold the company and company’s Affiliates harmless from any claim, suit or demand, including attorney’s fees, made by a third party due to or arising out of your utilizing of our services, your violation or breach of these Terms and Conditions, your violation of any rights of a third party, or any other act or omission by you. IN NO EVENT SHALL THE COMPANY BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THIS WEBSITE OR SYSTEM, OR ANY INFORMATION PROVIDED ON THIS WEBSITE OR SYSTEM. DUE TO SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FORCONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MIGHT NOT BE APPLIED TO YOU.</p>
            <h2 class="licence">AMENDMENTS</h2>
            <p>You agree and acknowledge the fact that the company may unilaterally change these Terms and Conditions. It is highly recommended that our customers keep track of any changes made to the above Terms and Conditions by reviewing this webpage from time to time, since we guarantee immediate reflection of any such changes on the present webpage of this website.</p>
            <br />
            <hr />
            <br />
            <h2 class="licence">Privacy Policy</h2>
            <p>
                Effective date: February 11th, 2016<br /><br />
                Unity is a fast and simple way to get academic guidance through a paid work
                environment with other college students across the nation. You can “seek an assignment”
                or “post an assignment” with our system amongst college students, receive help by
                posting an assignment, touch base by filling out details, and get paid by seeking an
                assignment to immerse yourself in an easier load of worries for your extremely busy
                lives., and enjoy original non plagiarized work pieces of academic assistance from the
                world’s best students across the nation.<br />
                Inevitably you’ll share some information with us after choosing to select one of our
                current services and any other services we provide. For this reason, we want to be candid
                about the information we receive and collect, including whom we share it with, how we
                use it, and the options we grant to you to control, access, and review your information.
                Due to this we’ve taken the time to write this privacy policy. And it’s why we’ve made
                an effort to write it in a way that’s heartily free of the legalese that usually fills up these
                documents. Of course, you can always email us if you still have questions about anything
                in our privacy policy.<br /><br />
                Information We Collect<br />
                The information we collect is divided into 3 simple categories:<br />
                First of which is the Information you choose to give to us. Second of which is the
                Information we receive automatically when you use our services. And the Third is the
                Information we get from third parties.<br />
                The categories displayed above are also shown below with much more detail.<br /><br />
                Information You Choose to Give Us<br />
                Information that you choose to share with us will be collected whenever you interact with
                our services. For example, all of our services require you to create a simple Unity
                account, so we need to collect a few important details about you: a unique username
                you’d like to go by but must not include your first or last name, a password, an email
                address, a phone number, the college or university you are currently attending and your
                date of birth. Other services, such as commerce products, may also require you to provide
                us with a debit or credit card number and its associated account information, paypal
                account information, or venmo account details. And, of course, when you contact Unity
                Support or communicate with us in any other way, we’ll collect whatever information
                you volunteer.<br />
                Information We Get Automatically When You Use Our Services<br />
                Information about which of the services you’ve used and how you’ve used them will be
                collected whenever you use our services. We might know, for instance, that you posted a
                particular assignment, filled out details in full for that particular assignment, and paid a
                set amount of money for that assignment. Here’s a fuller explanation of the types of
                information we collect when you use our services:<br /><br />
                * Usage Information. Information about your activity through our services is collectedwhen you use our services. For example, we may collect information about:<br />
            <ul>
                <li>how you interact with the services, such as which service you decide to select
                    from our available services which include “post an assignment” and “ seek an
                    assignment”. In addition to which postings you may have posted or viewed when
                    using our services.</li>
                <li>how you communicate with other Unity clients, such as the details included in
                    the posting, deadline posted, amount of money offered, materials attached, the
                    number of postings you post or complete, which Unity clients you exchange
                    services with the most, and your interactions with postings (such as when you
                    download or upload an assignment or materials ).</li>
            </ul>

            * Content Information<br/>
            The content you provide and information about that content is collected, such as if the
            Unity client has viewed the content, downloaded the content, and the metadata that is
            provided with the content.<br /><br />
            * Device Information<br />
            Device-specific information is collected by us. For example this information may be the
            hardware model, operating system version, unique device identifiers, browser type,
            language, wireless network, and mobile network information (including the mobile phone
            number).<br />
            <ul>
                <li>Also device information will be collected by us. In the event you experience any
                    crash or other problem when using our services, this information will be used
                    diagnose and troubleshoot such problems.</li>
            </ul>

            * Location Information<br />
            Information about your location may be collected when using our services. With your
            consent, we may also collect information about your precise location using methods that
            include GPS, wireless networks, cell towers, Wi-Fi access points, and other sensors, such
            as gyroscopes, accelerometers, and compasses.<br /><br />
            * Information Collected by Cookies and Similar Technologies<br />
            Just like almost all online services, websites and mobile applications, we may use cookies
            and other technologies to collect information about your device, activity and browser.
            Such other technologies include but are not limited to web beacons, web storage, and
            unique device identifiers. When you interact with services we offer through one of our
            affiliates, such as commerce features, these technologies to collect information may also
            be used by us. By default, most web browsers are set to accept cookies. If you prefer, you
            can usually remove or reject browser cookies through the settings on your browser or
            device. If you do decide to remove or reject cookies, acknowledge that the availability
            and functionality of our services can be affected by these actions or actions related to
            such.<br /><br />
            * Log Information<br />
            Log file information is collected when you use our services. That information includes,
            among other things:<br />
            <ul>
                <li>details about how you’ve used our services</li>
                <li>device information, such as your web browser type and language</li>
                <li>access times</li>
                <li>pages viewed</li>
                <li>IP address</li>
                <li>pages you visited before navigating to our services</li>
                <li>cookies that may uniquely identify your device or browser</li>
            </ul>
            * Information We Collect from Third Parties<br />
            Information from other third-party sources may also be obtained and combined with the
            information we collect through our services. For example, one of our affiliates may be
            used to help us identify that you are indeed a current college student at the college or
            university you have listed as the college or university you currently attend. their device
            phonebook—we may combine the information we collect from our affiliates or partners
            with other information we have collected about you.<br /><br />
            * How We Use Information<br />
            Aside of using the information to supply you with an incredible set of services and
            products that we that we tenaciously improve, we also use the information to:<br />
            <ul>
                <li>enhance, develop, maintain, protect and provide our products and services</li>
                <li>evaluate and monitor trends and usage<li>
                <li>interact with you</li>
                <li>improve the protection and security of our services</li>
                <li>validate your identity and prevent fraud or other unauthorized or illegal activity</li>
                <li>use information we’ve collected from cookies and other technology to enhance
                    the services and your experience with them</li>
                <li>personalize the services by, among other things, suggesting assignments, or
                    providing advertisements, content, or features that match user profiles, interests,
                    or previous activities</li>
            </ul>
            Some information may also be stored locally on your device by our system. For example,
            in order for you to be able to visit the website or use the mobile app and view the content
            faster, information such as local cache may be stored.<br /><br />
            Note that no matter where you live or where you happen to use our services, you consent
            and agree to us processing and transferring information in and to the United States.<br /><br />
            * How We Share Information<br />
            We may share information about you in the following ways:
            * With other Unity Clients<br /><br />
            We may share the following information about you with other Unity clients:<br />
            <ul>
                <li>public information, such as your username and the college or university you
                    currently attend;</li>
                <li>information about how you have interacted with the services, such as if you chose
                    the service to “post an assignment” or “seek an assignment”, the details of the•
                    •
                    assignment you have posted, the amount of money you are offering for a service,
                    and other information that will help Unity clients understand your connections
                    with others using the services. For example, because it may not be clear whether
                    an other Unity client is capable of assisting you, we may share which university
                    or college the other Unity client currently attends.</li>
                <li>any additional information you have consented to be shared. For example, when
                    you attach materials, we may share information about the material you attached
                    with other users who view the posting; and</li>
                <li>content you post will be shared with other Unity clients and potentially the public
                    at large; how widely your content is shared depends on what information you
                    decide to include or exclude and the type of service you are using.</li>
            </ul>
            * With our affiliates<br />
            We may share information with entities within the Unity community of companies.<br /><br />
            * With third parties<br />
            We may share your information with the following third parties:
            <ul>
                <li>With service providers, sellers, and partners. We may share information about you
                    with service providers who perform services on our behalf, sellers that provide
                    goods through our services, and business partners that provide services and
                    functionality.
                    • With third parties for legal reasons. We may share information about you if we
                    reasonably believe that disclosing the information is needed to:
                    * comply with any valid legal process, government request, or applicable law, rule, or
                    regulation;
                    * investigate, remedy, or enforce potential Terms of Service violations;
                    * protect the rights, property, and safety of us, our users, or others; or
                    * detect and resolve any fraud or security concerns.
                    • With third parties as part of a merger or acquisition. If Unity gets involved in a
                    merger, asset sale, financing, or acquisition of some portion of our business to
                    another company, we may share your information with that company before and
                    after the transaction closes.
                    * In the aggregate<br />
                    We may also share with third parties, such as advertisers, aggregated or de-identified
                    information that cannot reasonably be used to identify you.<br /><br />
                    Links Disclaimer<br />
                    In order to introduce our clients to specific information, we may provide links to local,
                    state and federal government agencies, as well as some websites of other organizations. A
                    link does not constitute an endorsement of the content, products, accuracy, services,
                    opinions, viewpoint, policies, or accessibility of that website. Once you link to another
                    website from this one, including any maintained by the State, you are subject to the terms
                    and conditions of that website, including, but not limited to, its privacy policy.<br /><br />
                    Children<br />
                    We don’t direct our Services to anyone under 18. We also don’t direct our Services to
                    anyone who is not a student of a higher educational institution. For this reason we do not
                    knowingly collect personal information from anyone under 18 or anyone who is not a
                    student of a higher educational institution.<br /><br />
                    Revisions to the Privacy Policy<br />
                    This privacy policy can be subject to change at any time due to continuous changes in our
                    system. However, when we do update, revise or change the privacy policy, we’ll let you
                    know one way or another. Some instances our way of letting you know will be by
                    revising the date at the top of the privacy policy that’s available on our website and
                    mobile application. Other instances, we may provide you with additional notice. For
                    example, we can add a statement to our website's homepage or provide you with an in-
                    app notification or personal email.
                    </p>
                    <br />
                    <hr />
                    <br />
                    <h2 class="licence">Confidentiality policy</h2>
                    <p>
                        At Unityforcollege.com we take confidentiality with extreme seriousness. We do not collect any personal information apart from your date of birth, your name, email address, the higher educational institute you are currently attending, and the phone number you use when placing an order with us. You can be 100% certain we will never disclose your name, date of birth, or email address to anybody, including the clients who seek or post an assignment. This is due to the scope of information you may provide in the description and the attached material of an assignment,<br /><br />
                        1. Definition of Confidential Information;
                        The client's’ agreement that any amount of data that is in connection directly or indirectly
                        with the system or products being outputted, or any property that was viewed both
                        intellectually and physically within the scope of the relationship; be it gained directly or
                        indirectly from directors, agents, managers, or owners of this system or within the course
                        of the relationship, this data includes information that is related to the design, build, or
                        any parameters that are in the relationship between clients but is not limited to any
                        marketing details, resources, tactics, schemes, business information, financial
                        information, administrative information, property, or any third party information that is
                        collected or brought to the attention of the client these said terms and conditions and
                        policy or any course of communication that occurs will be deemed and referred to as
                        “Confidential Information.” This “Confidential Information,” that the client agrees to is
                        not confined by boundaries or is not limited to; I) any data or information that was
                        previous public knowledge or has recently became publicly available and was gained
                        legally both federally and on a state by state basis. II) Any information that was not
                        previously disclosed between the client and the system, that is in the current possession
                        of the client before this relationship establishment. III) All projects or actions both
                        personal and commercial that the client is in relation to or involved in even at the most
                        remedial of levels that indirectly brings about the use or disclosure of any Confidential
                        Information. IV) All information or data that has been provided by a third party to the
                        client with proper jurisdiction of said transfer of information without limitation.<br /><br />
                        2. Nondisclosure and nonuse of Confidential Information;<br />
                        Client agrees to protect Unity’s Confidential Information; using at least the identical
                        extent of care that it uses to shield its own confidential and proprietary information of
                        identical importance, but no less than a reasonable extent of care. Client agrees to use
                        Unity’s Confidential Information for the sole purpose of evaluation in connection with
                        clients operations and discussions with Unity in relation to this agreement. Client will not
                        at any time publish, disclose, or disperse Confidential Information to anyone other than
                        the clients’ employees and contractors who have a need to know in order to attain such
                        purpose or guide client in connection with the service, and who are also bound by a
                        written agreement that prevent unauthorized disclosure or use of Unity’s Confidential
                        Information. Furthermore, client may disclose such information to its legal and financial
                        advisers for the purpose of obtaining guidance in relation with the service, provided that
                        such disbursement of Confidential Information is subject to a written confidentiality
                        agreement or other legal binding sorts of confidential agreement. Client agrees not to use 
                        Confidential Information for any other purpose or for its own or any third party’s benefit
                        without the prior written consent of an authorized representative of Unity in each
                        instance. Client may disclose Confidential Information to the extent required by law,
                        including but not limited to when required in connection with a judicial or governmental
                        proceeding, provided client makes reasonable efforts to give Unity notice of such
                        requirement prior to any such disclosure and take reasonable steps to obtain protective
                        treatment of the Confidential Information.<br /><br />
                        3. No License to Confidential Information;<br />
                        Except as expressly set forth herein, no license or other rights to Confidential Information
                        are given or insinuated hereby and Unity conserves all of its rights herein.<br /><br />
                        4. No Warranty;<br />
                        Any and all information provided is “As Is”. Furthermore, whether expressed or implied,
                        all information comes with no warranty as to its accuracy and completeness.<br /><br />
                        5. Return of Documents;<br />
                        Upon recipient of Unity’s written request and at Unity’s option, Client will either return
                        to Unity all Confidential Information or will provide Unity with written certification that
                        all tangible Confidential Information has been destroyed. The tangible Confidential
                        Information includes but is not limited to all electronic files, documentation, notes, plans
                        , drawings, and copies thereof.<br /><br />
                        6. Term and Termination;<br />
                        This agreement may be terminated by either party (a) upon twelve days of written notice
                        or (b) due to the other party’s infraction of this agreement, instantly upon giving written
                        notice of such information. Furthermore, Any termination of this agreement will not
                        relieve client of its confidentiality and use of obligations with respect to Confidential
                        Information disclosed prior to the date of termination in addition to any other provisions
                        of this agreement unless expressly stated herein the confidentiality policy and terms and
                        conditions or upon authorized written consent from Unity.<br /><br />
                        7. Equitable Relief;<br />
                        Client hereby acknowledges that unauthorized disclosure or use of Confidential
                        Information could cause irreparable harm and crucial injury to Unity that may be arduous
                        to ascertain. Correspondingly, client agrees that Unity will have the right to seek
                        injunctive relief to enforce obligations under this agreement, terms and conditions, and
                        any other rights and remedies it may have.<br /><br />
                        8. No Implied Waiver;<br />
                        Unless expressly waived in writing, neither party’s hindrance nor failure in exercising
                        any of its rights will constitute a waiver of such rights.<br /><br />
                        9. No Assignment;<br />
                        Client may not assign this agreement by any means, including without limitation, by
                        operation of law or merger. Any attempt of assignment of this agreement by the client in 
                        violation of this section will be void.<br /><br />
                        10. Entire Agreement;<br />
                        This agreement constitutes the entire agreement with respect to the Confidential
                        Information disclosed pursuant to this Agreement and supersedes all prior or coexisting
                        oral or written agreements concerning such Confidential Information. This agreement
                        may not be modified except by written agreement signed by authorized representatives of
                        Unity.<br /><br />
                        We have introduced three principles of confidentiality we ask all clients to follow:<br /><br />
                        1. Do not share your personal information within your details of an assignment. This
                        includes but is not limited to your first and last name, date of birth, email address, phone
                        number, or your home address. Our system and website is built for clients to be
                        anonymous to other clients. If you need to share a document or other types of additional
                        materials, upload them into the designated area on the website in the description area for
                        the assignment to be posted.<br /><br />
                        2. Do not give the system your login details to your school or university website. If you
                        need to share information, retrieve it yourself and upload it using one of the methods
                        described in the previous paragraph but do not include any personal information in these
                        documents. Giving anyone access to your personal log is something we strongly prohibit.<br /><br />
                        3. Do not include personal information subjecting you to fraud and/or intent for academic
                        credit in any of the documents you upload and attach. This includes all and any
                        documents that you may upload and attach as materials to be used to aid a client
                        completing your assignment. At no point should the client ever be posting personal
                        information that can be used for but is not limited to, unintended purposes such as
                        banking information, identification information, and any information that should not be
                        distributed for others to view.<br /><br />
                        Unityforcollege.com is a DIGICERT SECURE website.<br /><br />
                        Complete confidentiality is of crucial importance when it comes to online academic
                        assistance. We never ask for any more information than we need to get you started with
                        selecting a choice to begin using our services.<br />
                        You agree to comply with all points of this Confidentiality Policy when you first select a
                        service from our system at Unityforcollege.com or our mobile application.<br />
                        Direct contact between the two parties in a product transaction is never condoned and is a
                        strict violation of this privacy policy. Any issues regarding the client-to-client
                        relationship should be brought to the system alone in which our system will handle
                        accordingly.<br />
                        This service is strictly for the use of college students or students attending a higher
                        learning institution. Providing false information to the system will result as a direct
                        violation of this policy and adhered to terms and conditions. At no point should this 
                        system be used for anything outside of the parameters explicitly stated in the terms and
                        conditions and this policy.<br />
                    </p>
                    <div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  class="check_terms" /> I have READ and AGREE to the Terms and Conditions, Confidentiality Policy, and Privacy Policy.
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox"  class="check_terms_age" /> I am 18 years old or older and currently attend a higher education institute within the United States of America.
                            </label>
                        </div>
                    </div>
                    </div>      
                    <?php
                }
                if ($action == 'do_assignment') {
                    ?>
                    <div class="bluebox">
                        <h2 style="margin-top: 5px">Summary of licensing terms</h2>
                        <p>The main terms of the licence incorporated into the terms and conditions are as follows.</p>
                        <ol>
                            <li>Unless you have paid for the right to use the relevant document without a credit and hyperlink, you must: (a) retain the credit in the free legal document; and (b) if you publish the document on a website, <strong>include a link to www.unity.com from your website</strong>.&nbsp; The link can be pointed at any page on www.unity.com.</li>
                            <li>Subject to this point, you may edit and amend the documents to render them suitable for your purposes.</li>
                            <li>You must not re-publish the free legal documents in unamended form. All footnotes and brackets should be removed from the documents before publication.</li>
                            <li>You must not sell or re-distribute the free legal documents or derivatives thereof.</li>
                            <li>We give no warranties or representations concerning the free legal documents, and accept no liability in relation to the use of the free legal documents.</li>
                        </ol>
                    </div>
                    <?php
                }
                if ($action == 'choose_option') {
                    $arr['id'] = $user_id;
                    $arr['first_time'] = 1;
                    $arr['option_type'] = $option;
                    $obj = new user;
                    $obj->saveUser($arr);
                }
                if ($action == 'get_assignments') {
                    $obj = new assignment;
                    $assignments = $obj->listactiveAssignmentsBySubject($subject_id);
                    if ($assignments) {
                        ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Subject</th>
                                    <th>Due By</th>
                                    <th>Time Due</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($assignments as $assignment) { ?>
                                    <tr>
                                        <td><?php echo $assignment->title; ?></td>
                                        <td>
                                            <?php
                                            $obj = new subjects;
                                            $subject = $obj->getSubject($assignment->subject_id);
                                            echo $subject->title;
                                            ?>
                                        </td>
                                        <td><?php echo $assignment->due_date; ?></td>
                                        <td><?php echo $assignment->time_due; ?></td>
                                        <td>
                                            <a href="<?php echo make_admin_url('job', 'view', 'view', 'id=' . $assignment->id) ?>" class="btn btn-primary btn-xs">View Complete Job</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php
                    } else {
                        echo '<div class="alert alert-info">No Result Found!</div>';
                    }
                }
                if ($action == 'make_payment') {
                    $obj = new assignment;
                    $assignment = $obj->getAssignment($assign_id);
                    $obj = new job;
                    $job = $obj->getJobByAssignmentId($assign_id);
                    $obj = new user;
                    $user = $obj->getUser($job->user_id);
                    $admin_email = 'rina-facilitator@cwebconsultants.com';
                    $user_email = $user->paypal_id;
                    $user_email = $user->email;
                    $price = (float) $assignment->price;
                    $admin_amount = (float) ($price * 1 / 10);
                    $user_amount = (float) ($price * 9 / 10);
                    $obj = new Paypal;
                    $data = $obj->splitPay($admin_email, $user_email, $admin_amount, $user_amount, $assign_id);
                    echo $data;
                }
                if ($action == 'make_payment2') {
                    $obj = new assignment;
                    $assignment = $obj->getAssignment($assign_id);
                    $admin_email = 'rina-facilitator@cwebconsultants.com';
                    $price = (float) $assignment->price;
                    $admin_amount = (float) ($price * 1 / 10);
                    $user_amount = (float) ($price * 9 / 10);

                    $obj = new Paypal;
                    $data = $obj->splitPay2($admin_email, $admin_amount, $user_amount, $assign_id);
                    echo $data;
                }
                if ($action == 'scan_plagiarism') {
                    $assign_id = $_POST['assignment_id'];
                    $obj = new assignment;
                    $assignment = $obj->getAssignment($assign_id);
                    $obj = new job;
                    $job = $obj->getJobByAssignmentId($assign_id);
                    $obj = new files;
                    $detail = $obj->getFileByAssignmentIdAndUserID($job->assignment_id, $job->user_id);
                    if (!empty($detail) && is_object($detail)):
                        $filename = $detail->file;
                        $filename = DIR_FS_SITE . 'assets/files/' . $filename;
                        $result = handlePostByUnplag($filename);
                        if (empty($result)):
                            $result = "Unable to scan attached document";
                        endif;
                    endif;
                    echo $result;
                    exit;
                }
                if ($action == 'one') {
                    $query = new job;
                    $query->update_rating($assignment_id, 1);
                }
                if ($action == 'two') {
                    $query = new job;
                    $query->update_rating($assignment_id, 2);
                }
                if ($action == 'three') {
                    $query = new job;
                    $query->update_rating($assignment_id, 3);
                }
                if ($action == 'four') {
                    $query = new job;
                    $query->update_rating($assignment_id, 4);
                }
                if ($action == 'five') {
                    $query = new job;
                    $query->update_rating($assignment_id, 5);
                }
            }
            ?>