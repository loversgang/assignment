<?php
require_once("include/config/config.php");
require 'tmp/header.php';
$id = $_GET['id'];
$code = $_GET['code'];
$obj = new user;
$user = $obj->getUser($id);

if ($admin_user->is_logged_in()) {
    $admin_user->set_error();
    $admin_user->set_pass_msg('You are already login');
    Redirect(make_admin_url('job'));
}
?>
<div class="row-fluid">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ol class="breadcrumb">
                <li><a href="<?php echo make_admin_url('home') ?>">Home</a></li>
                <li class="active">Email Verification</li>
            </ol>
        </div>
        <div class="panel-body">
            <?php display_message(1); ?>
            <?php
            if ($user) {
                if (@$user->is_email_verfied == 0) {
                    if ($user->verif_code == $code) {
                        $arr['id'] = $user->id;
                        $arr['is_status'] = 1;
                        $arr['is_email_verified'] = 1;
                        $obj = new user;
                        $obj->saveUser($arr);
                        ?>
                        <div class="alert alert-info">
                            <b>Email Confirmed Successfully!</b><br/>
                            <?php
                            $_SESSION['admin_session_secure']['toastr'] = 'Email verified successfully';
                            Redirect(DIR_WS_SITE);
                            ?>
                        </div>
                    <?php } else { ?>
                        <div class="alert alert-danger">
                            Your verification code is incorrect!
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="alert alert-info">
                        <b>Email Already Confirmed!</b><br/>
                        Please check mail for account details!
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="alert alert-danger">
                    Something Went Wrong!
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <?php require 'tmp/footer.php'; ?>
</div>