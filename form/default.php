<!-- section coding -->
<?php
/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'update':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/edit.php');
        break;
    case 'insert':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/create.php');
        break;
    case 'view':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/view.php');
        break;
    case 'thrash':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/thrash.php');
        break;
    case 'admin':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/admin.php');
        break;
    case 'success':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/success.php');
        break;
    case 'venmo':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/venmo.php');
        break;
    default:break;
endswitch;
