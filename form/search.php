<?php

/* sections */
switch ($section):
    case 'list':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/list.php');
        break;
    case 'quick':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/quick.php');
        break;
    case 'username':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/username.php');
        break;
    case 'full':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/full.php');
        break;
    case 'results':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/results.php');
        break;
    case 'saved':
        include_once(DIR_FS_SITE . ADMIN_FOLDER . '/form-template/' . $modName . '/saved.php');
        break;
    default:break;
endswitch;
?>