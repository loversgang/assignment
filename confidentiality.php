<div class="modal fade" id="confidentiality_policy" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Confidentiality policy</h4>
            </div>
            <div class="modal-body">
                <div class="bluebox" style="height: 300px;overflow-y: scroll;">
                    <p>
                        At Unityforcollege.com we take confidentiality with extreme seriousness. We do not collect any personal information apart from your date of birth, your name, email address, the higher educational institute you are currently attending, and the phone number you use when placing an order with us. You can be 100% certain we will never disclose your name, date of birth, or email address to anybody, including the clients who seek or post an assignment. This is due to the scope of information you may provide in the description and the attached material of an assignment,<br /><br />
                        1. Definition of Confidential Information;
                        The client's’ agreement that any amount of data that is in connection directly or indirectly
                        with the system or products being outputted, or any property that was viewed both
                        intellectually and physically within the scope of the relationship; be it gained directly or
                        indirectly from directors, agents, managers, or owners of this system or within the course
                        of the relationship, this data includes information that is related to the design, build, or
                        any parameters that are in the relationship between clients but is not limited to any
                        marketing details, resources, tactics, schemes, business information, financial
                        information, administrative information, property, or any third party information that is
                        collected or brought to the attention of the client these said terms and conditions and
                        policy or any course of communication that occurs will be deemed and referred to as
                        “Confidential Information.” This “Confidential Information,” that the client agrees to is
                        not confined by boundaries or is not limited to; I) any data or information that was
                        previous public knowledge or has recently became publicly available and was gained
                        legally both federally and on a state by state basis. II) Any information that was not
                        previously disclosed between the client and the system, that is in the current possession
                        of the client before this relationship establishment. III) All projects or actions both
                        personal and commercial that the client is in relation to or involved in even at the most
                        remedial of levels that indirectly brings about the use or disclosure of any Confidential
                        Information. IV) All information or data that has been provided by a third party to the
                        client with proper jurisdiction of said transfer of information without limitation.<br /><br />
                        2. Nondisclosure and nonuse of Confidential Information;<br />
                        Client agrees to protect Unity’s Confidential Information; using at least the identical
                        extent of care that it uses to shield its own confidential and proprietary information of
                        identical importance, but no less than a reasonable extent of care. Client agrees to use
                        Unity’s Confidential Information for the sole purpose of evaluation in connection with
                        clients operations and discussions with Unity in relation to this agreement. Client will not
                        at any time publish, disclose, or disperse Confidential Information to anyone other than
                        the clients’ employees and contractors who have a need to know in order to attain such
                        purpose or guide client in connection with the service, and who are also bound by a
                        written agreement that prevent unauthorized disclosure or use of Unity’s Confidential
                        Information. Furthermore, client may disclose such information to its legal and financial
                        advisers for the purpose of obtaining guidance in relation with the service, provided that
                        such disbursement of Confidential Information is subject to a written confidentiality
                        agreement or other legal binding sorts of confidential agreement. Client agrees not to use 
                        Confidential Information for any other purpose or for its own or any third party’s benefit
                        without the prior written consent of an authorized representative of Unity in each
                        instance. Client may disclose Confidential Information to the extent required by law,
                        including but not limited to when required in connection with a judicial or governmental
                        proceeding, provided client makes reasonable efforts to give Unity notice of such
                        requirement prior to any such disclosure and take reasonable steps to obtain protective
                        treatment of the Confidential Information.<br /><br />
                        3. No License to Confidential Information;<br />
                        Except as expressly set forth herein, no license or other rights to Confidential Information
                        are given or insinuated hereby and Unity conserves all of its rights herein.<br /><br />
                        4. No Warranty;<br />
                        Any and all information provided is “As Is”. Furthermore, whether expressed or implied,
                        all information comes with no warranty as to its accuracy and completeness.<br /><br />
                        5. Return of Documents;<br />
                        Upon recipient of Unity’s written request and at Unity’s option, Client will either return
                        to Unity all Confidential Information or will provide Unity with written certification that
                        all tangible Confidential Information has been destroyed. The tangible Confidential
                        Information includes but is not limited to all electronic files, documentation, notes, plans
                        , drawings, and copies thereof.<br /><br />
                        6. Term and Termination;<br />
                        This agreement may be terminated by either party (a) upon twelve days of written notice
                        or (b) due to the other party’s infraction of this agreement, instantly upon giving written
                        notice of such information. Furthermore, Any termination of this agreement will not
                        relieve client of its confidentiality and use of obligations with respect to Confidential
                        Information disclosed prior to the date of termination in addition to any other provisions
                        of this agreement unless expressly stated herein the confidentiality policy and terms and
                        conditions or upon authorized written consent from Unity.<br /><br />
                        7. Equitable Relief;<br />
                        Client hereby acknowledges that unauthorized disclosure or use of Confidential
                        Information could cause irreparable harm and crucial injury to Unity that may be arduous
                        to ascertain. Correspondingly, client agrees that Unity will have the right to seek
                        injunctive relief to enforce obligations under this agreement, terms and conditions, and
                        any other rights and remedies it may have.<br /><br />
                        8. No Implied Waiver;<br />
                        Unless expressly waived in writing, neither party’s hindrance nor failure in exercising
                        any of its rights will constitute a waiver of such rights.<br /><br />
                        9. No Assignment;<br />
                        Client may not assign this agreement by any means, including without limitation, by
                        operation of law or merger. Any attempt of assignment of this agreement by the client in 
                        violation of this section will be void.<br /><br />
                        10. Entire Agreement;<br />
                        This agreement constitutes the entire agreement with respect to the Confidential
                        Information disclosed pursuant to this Agreement and supersedes all prior or coexisting
                        oral or written agreements concerning such Confidential Information. This agreement
                        may not be modified except by written agreement signed by authorized representatives of
                        Unity.<br /><br />
                        We have introduced three principles of confidentiality we ask all clients to follow:<br /><br />
                        1. Do not share your personal information within your details of an assignment. This
                        includes but is not limited to your first and last name, date of birth, email address, phone
                        number, or your home address. Our system and website is built for clients to be
                        anonymous to other clients. If you need to share a document or other types of additional
                        materials, upload them into the designated area on the website in the description area for
                        the assignment to be posted.<br /><br />
                        2. Do not give the system your login details to your school or university website. If you
                        need to share information, retrieve it yourself and upload it using one of the methods
                        described in the previous paragraph but do not include any personal information in these
                        documents. Giving anyone access to your personal log is something we strongly prohibit.<br /><br />
                        3. Do not include personal information subjecting you to fraud and/or intent for academic
                        credit in any of the documents you upload and attach. This includes all and any
                        documents that you may upload and attach as materials to be used to aid a client
                        completing your assignment. At no point should the client ever be posting personal
                        information that can be used for but is not limited to, unintended purposes such as
                        banking information, identification information, and any information that should not be
                        distributed for others to view.<br /><br />
                        Unityforcollege.com is a DIGICERT SECURE website.<br /><br />
                        Complete confidentiality is of crucial importance when it comes to online academic
                        assistance. We never ask for any more information than we need to get you started with
                        selecting a choice to begin using our services.<br />
                        You agree to comply with all points of this Confidentiality Policy when you first select a
                        service from our system at Unityforcollege.com or our mobile application.<br />
                        Direct contact between the two parties in a product transaction is never condoned and is a
                        strict violation of this privacy policy. Any issues regarding the client-to-client
                        relationship should be brought to the system alone in which our system will handle
                        accordingly.<br />
                        This service is strictly for the use of college students or students attending a higher
                        learning institution. Providing false information to the system will result as a direct
                        violation of this policy and adhered to terms and conditions. At no point should this 
                        system be used for anything outside of the parameters explicitly stated in the terms and
                        conditions and this policy.<br />
                    </p>
                    <div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>