-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 24, 2016 at 11:47 PM
-- Server version: 5.5.41-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kissconnection`
--

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `email`, `gender`, `dob`, `age`, `foot`, `inch`, `zip`, `lat`, `lng`, `is_auto_login`, `is_cupid`, `is_loggedin`, `is_active`, `is_visible`, `ip_address`, `last_access`, `date_add`, `message_login`) VALUES
(18, 'marshel', '2c9520f0e5d91a843ac72aab4c1a810b', 'bharat@cwebconsultants.com', 'male', '1-1-1990', 26, 6, 6, '160012', 59.2012, 39.9232, 0, 0, 1, 1, 1, '122.173.34.168', '1455284737', '1455024507', '1455868567 1455868606 1455868712 1455871187 1455881282 1455884008 1455884150 1455885209 1455885536 1455885600 1456118648 1456118709'),
(19, 'fred', '202cb962ac59075b964b07152d234b70', 'bryan7878@gmail.com', 'male', '1-5-1995', 21, 5, 6, '30041', 34.2001, -84.0907, 0, 0, 1, 1, 1, '174.49.77.99', '1455554887', '1455113589', '1455881488 1455881525 1455881583 1455882354 1455890361 1455895270'),
(20, 'jill', '202cb962ac59075b964b07152d234b70', 'bryan@bryankilpatrick.com', 'female', '1-5-1995', 21, 5, 6, '30041', 34.2001, -84.0907, 0, 0, 1, 1, 1, '174.49.77.99', '1455114148', '1455114148', ''),
(21, 'mary', '202cb962ac59075b964b07152d234b70', 'bob@bryankilpatrick.com', 'female', '1-5-1995', 21, 5, 6, '93060', 34.4089, -119.069, 0, 0, 1, 1, 1, '174.49.77.99', '1455286262', '1455114255', '1455879058 1455881750 1455881831 1455882242 1455882518 1455884601 1455885211 1455890278'),
(22, 'claydeveloper', 'e10adc3949ba59abbe56e057f20f883e', 'clay.developer+temp0011@gmail.com', 'male', '1-5-1995', 21, 5, 6, '99501', 61.2189, -149.85, 0, 0, 1, 1, 1, '122.173.51.45', '1455705847', '1455705847', ''),
(23, 'gomerdeveloper', 'e10adc3949ba59abbe56e057f20f883e', 'ambika@cwebconsultants.com', 'male', '1-5-1995', 21, 5, 6, '99501', 61.2189, -149.85, 0, 0, 1, 1, 1, '122.173.51.45', '1455709230', '1455709230', '1455868670 1455881196 1455881240 1455884093 1455885568 1455937792 1455938028 1456118579 1456118686');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
