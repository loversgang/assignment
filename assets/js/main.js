var welcome = $('#main_c').attr('data-first-time-login');

$(document).ready(function () {
    window.base_url = $('#site_url').attr('href');
    window.img_url = base_url + 'assets/images/';
    window.ajax_url = base_url + 'tmp/ajax.php';
    // Plugins Initialization
    $('.datatable').DataTable();
    $(".validation").validationEngine();
    $('.datepicker').datepicker({
        format: "mm-dd-yyyy",
    });

    // Registration
    $(document).on('click', '.signup_user', function (e) {
        toastr.remove();
        var el = $(this);
        var html = el.html();
        var main_div = el.closest('.signup_form');
        var data_array = {};
        data_array.username = main_div.find('#username').val();
        data_array.password = main_div.find('#password').val();
        data_array.password2 = main_div.find('#password2').val();
        data_array.email = main_div.find('#email').val();
        data_array.gender = main_div.find('#gender').val();
        data_array.is_auto_login = main_div.find('input[name="auto_login"]:checked').val();
        data_array.is_cupid = main_div.find('#cupid').val();
        if (data_array.username) {
            if (data_array.password) {
                if (data_array.password === data_array.password2) {
                    if (data_array.email && validateEmail(data_array.email)) {
                        if (confirm("By clicking on 'I Agree' below, you acknowledge that you have read and accept KissConnection.com's Terms and Conditions and Privacy Policy.")) {
                            main_div.find('.disagree').hide();
                            el.html('<i class="fa fa-spinner fa-spin"></i> Signing Up');
                            $.post(ajax_url, {action: 'user_signup', data_array: data_array}, function (data) {
                                el.html(html);
                                main_div.find('.disagree').show();
                                if (data === 'usernameExists') {
                                    toastr.error('Username Already Exists!', 'Error');
                                } else if (data === 'emailExists') {
                                    toastr.error('Email Already Exists!', 'Error');
                                } else {
                                    window.location = base_url + 'control.php?Page=account&action=response&section=response';
                                }
                            });
                        }
                    } else {
                        toastr.error('Please Enter Valid Email Address!', 'Error');
                    }
                } else {
                    toastr.error('Password Not Matched!', 'Error');
                }
            } else {
                toastr.error('Please Enter Password!', 'Error');
            }
        } else {
            toastr.error('Please Enter Username!', 'Error');
        }
    });

    // Sign In
    $(document).on('click', '.signin_user', function () {
        toastr.remove();
        var el = $(this);
        var html = el.html();
        var main_div = el.closest('.signin_form');
        var data_array = {};
        data_array.username = main_div.find('.username').val();
        data_array.password = main_div.find('.password').val();
        if (data_array.username) {
            if (data_array.password) {
                el.html('<i class="fa fa-spinner fa-spin"></i> Logging In');
                $.post(ajax_url, {action: 'user_signin', data_array: data_array}, function (data) {
                    el.html(html);
                    data = $.parseJSON(data);
                    if (data.status === 'success') {
                        window.location = base_url + 'control.php';
                    } else {
                        toastr.error('Something went wrong!', 'Error');
                    }
                });
            } else {
                toastr.error('Please Enter Password!', 'Error');
            }
        } else {
            toastr.error('Please Enter Username!', 'Error');
        }
    });
    $(document).on('change', '.options_type', function () {
        var option = $(this).val();
        if (option === 'radio' || option === 'checkbox' || option === 'select') {
            $('.options_data').html('<div class="form-group"><label class="control-label">Options</label> <div class="input-group"><input type="text" name="options[]" class="form-control" required/><div class="input-group-addon add_more_options" style="cursor:pointer""><i class="fa fa-plus"></i></div></div>');
        } else {
            $('.options_data').html('');
        }
    });
    $(document).on('click', '.add_more_options', function () {
        $(this).html('<i class="fa fa-trash"></i>').removeClass('add_more_options').addClass('remove_more_options');
        $('.add_more_options_data').append('<div class="form-group"><div class="input-group"><input type="text" name="options[]" class="form-control" required/><div class="input-group-addon add_more_options" style="cursor:pointer"><i class="fa fa-plus"></i></div></div>');
    });
    $(document).on('click', '.remove_more_options', function () {
        $(this).closest('.form-group').hide(400, function () {
            $(this).remove();
        });
    });
    // Assignments
    $(document).on('click', '#post_assignment', function () {
        var modal = $('.terms_modal');
        var modal_body = modal.find('.modal-body');
        modal_body.html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        modal.modal();
        $.post(ajax_url, {action: 'post_assignment'}, function (data) {
            modal_body.html(data);
            modal.find('.tos_class').attr('data-tos', 'post_assignment');
        });
    });
//    if (welcome == 'welcome') {
    $(document).on('click', '.post_assignment', function (e) {
        e.preventDefault();
        var modal = $('.terms_modal');
        var modal_body = modal.find('.modal-body');
        modal_body.html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        modal.modal();
        $.post(ajax_url, {action: 'post_assignment'}, function (data) {
            modal_body.html(data);
        });
        $(document).on('click', '.tos_class', function () {
            window.location = base_url + 'control.php?Page=assignment&action=insert&section=insert';
        });
    });
    $(document).on('click', '.search_assignment', function (e) {
        e.preventDefault();
        var modal = $('.terms_modal');
        var modal_body = modal.find('.modal-body');
        modal_body.html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        modal.modal();
        $.post(ajax_url, {action: 'post_assignment'}, function (data) {
            modal_body.html(data);
        });
        $(document).on('click', '.tos_class', function () {
            window.location = base_url + 'control.php?Page=search&action=list&section=list';
        });
    });
    $(document).on('click', '.accept_assignment', function (e) {
        var modal = $('.terms_modal');
        var modal_body = modal.find('.modal-body');
        modal_body.html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        modal.modal();
        $.post(ajax_url, {action: 'do_assignment'}, function (data) {
            modal_body.html(data);
        });

    });
//    }
    $(document).on('click', '#do_assignment', function () {
        var modal = $('.terms_modal');
        var modal_body = modal.find('.modal-body');
        modal_body.html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        modal.modal();
        $.post(ajax_url, {action: 'do_assignment'}, function (data) {
            modal_body.html(data);
            modal.find('.tos_class').attr('data-tos', 'do_assignment');
        });

    });

    $(document).on('click', '.tos_class', function () {
        var el = $(this);
        var page = el.attr('data-page');
        if (page == 'job') {
            $('#accept').click();
        }
    });
    $(document).on('click', '.tos_class[data-tos="post_assignment"]', function () {
        var option = 'post';
        $.post(ajax_url, {action: 'choose_option', option: option}, function (data) {
            window.location = base_url + 'control.php?Page=assignment';
        });
    });

    $(document).on('click', '.tos_class[data-tos="do_assignment"]', function () {
        var option = 'do';
        $.post(ajax_url, {action: 'choose_option', option: option}, function () {
            window.location = base_url + 'control.php?Page=search';
        });
    });
    $(document).on('click', '.search_assignments', function () {
        var el = $(this);
        var main_div = el.closest('.search_from');
        var subject_id = main_div.find('.subject_id').val();
        $('.assignments_result').html('<center><img style="margin: 30px 0;" src="' + img_url + 'loader.gif"/></center>');
        $.post(ajax_url, {action: 'get_assignments', subject_id: subject_id}, function (data) {
            $('.assignments_result').html(data);
        });
    });
//    $(document).on('click', '.scan_plagiarism', function () {
//        var assignment_id = $(this).attr('data-assignment');
//        $('.scan_percentage').html('<img src="' + img_url + 'loader.gif"/> Scanning..');
//        $.post(ajax_url, {action: 'scan_plagiarism', assignment_id: assignment_id}, function (data) {
//            //setTimeout(function () {
//            $('.scan_percentage').html(data + '<br/><br/><button type="button" class="btn btn-primary make_payment" data-assignment_id="' + assignment_id + '"><i class="fa fa-usd"></i> Make Payment</button>');
//            //}, 3000);
//        });
//    });
    $(document).on('click', '.scan_plagiarism', function () {
        var assignment_id = $(this).attr('data-assignment');
        $('.scan_percentage').html('<img src="' + img_url + 'loader.gif"/> Scanning..');
        $.post(ajax_url, {action: 'scan_plagiarism', assignment_id: assignment_id}, function (data) {
            $('.scan_percentage').html(data);
        });
    });
    $(document).on('click', '.make_payment', function () {
        var el = $(this);
        var assign_id = el.attr('data-assignment_id');
        el.html('<i class="fa fa-spinner fa-spin"></i> Processing..');
        $.post(ajax_url, {action: 'make_payment', assign_id: assign_id}, function (data) {
            window.location = data;
        });
    });
    $(document).on('click', '.make_payment2', function () {
        var el = $(this);
        var assign_id = el.attr('data-assignment_id');
        el.html('<i class="fa fa-spinner fa-spin"></i> Processing..');
        $(el).attr('disabled', true);
        $.post(ajax_url, {action: 'make_payment2', assign_id: assign_id}, function (data) {
            window.location = data;
        });
    });
});
function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

$(document).on('click', 'input[name="login_user"]', function (e) {
    var username = $('input[name="username"]').val();
    var password = $('input[name="password"]').val();
    toastr.remove();
    if (!username || !password) {
        e.preventDefault();
        toastr.error('All fields are required');
    }
});

$(document).on('click', 'input[name="signup_user"]', function (e) {
    toastr.remove();
    var username = $('.signup_username').val();
    var email = $('input[name="email"]').val();
    var password = $('.password_s').val();
    var confirm_password = $('.confirm_password_s').val();
    var new_month = $('.month').val();
    var new_date = $('.date').val();
    var new_year = $('.year').val();
    var date = new_month;
    var month = new_date;
    var year = new_year;
    if (username && email && password && confirm_password && new_month && new_date && new_year) {
        var dob = new Date(year - 1, month - 1, date);
        var today = new Date();
        var age = Math.floor((today - dob) / (365.25 * 24 * 60 * 60 * 1000));
        if (username.length < 5) {
            e.preventDefault();
            toastr.error('Username atleast 5 characters');
        } else if (validateEmail(email) == false) {
            e.preventDefault();
            toastr.error('Please enter correct email address');
        } else if (password.length < 5) {
            e.preventDefault();
            toastr.error('Password atleast 6 characters');
        } else if (password != confirm_password) {
            e.preventDefault();
            toastr.error('Password not match');
        } else if (age < 18) {
            e.preventDefault();
            toastr.error('You must be at least 18 years old');
        } else {
            $('.post_assignment').click();
        }
    } else {
        e.preventDefault();
        toastr.error('All fields are required');
    }
});
$(document).on('click', '.agree_submit', function () {
    toastr.remove();
    var check_terms_checked = $('.check_terms').is(':checked');
    var check_terms_checked_age = $('.check_terms_age').is(':checked');
    if (!check_terms_checked || !check_terms_checked_age) {
        toastr.error('You must check the required boxes in order to continue.');
    }
    if (check_terms_checked && check_terms_checked_age) {
        $('.signup_submit').click();
    }
});

$(document).on('click', '#contact_us', function (e) {
    e.preventDefault();
    $('#contact_us_modal').modal();
});

$(document).on('click', 'input[name="change_password"]', function (e) {
    var password = $('input[name="password"]').val();
    var new_password = $('input[name="new_password"]').val();
    var confirm_new_password = $('input[name="confirm_new_password"]').val();
    if (new_password && confirm_new_password && password) {
        if (new_password != confirm_new_password) {
            e.preventDefault();
            toastr.error('Password not match');
        } else if (new_password.length < 5) {
            e.preventDefault();
            toastr.error('password should be 5 digits');
        }
    }
});

$(document).on('click', '#login', function (e) {
    e.preventDefault();
    $('#login_popup').modal();
});

$(document).on('click', '#login-2', function () {
    $('#sign_popup').modal();
});

$(document).on('click', '#signup_show', function () {
    $('.close_login').click();
    $('#sign_popup').modal();
});
$(document).on('click', '#login_show', function () {
    $('.close_signup').click();
    $('#login_popup').modal();
});

$(".rating.one").trigger('hover');
var assignment_id = $('.rating').attr('data-id');
$(document).on('click', '.one', function () {
    $.post(ajax_url, {'action': 'one', assignment_id: assignment_id}, function (data) {
        $('.two').removeAttr("style");
        $('.three').removeAttr("style");
        $('.four').removeAttr("style");
        $('.five').removeAttr("style");
    });
});
$(document).on('click', '.two', function () {
    $.post(ajax_url, {'action': 'two', assignment_id: assignment_id}, function (data) {
        $('.three').removeAttr("style");
        $('.four').removeAttr("style");
        $('.five').removeAttr("style");
    });

});
$(document).on('click', '.three', function () {
    $.post(ajax_url, {'action': 'three', assignment_id: assignment_id}, function (data) {
        $('.four').removeAttr("style");
        $('.five').removeAttr("style");
    });

});
$(document).on('click', '.four', function () {
    $.post(ajax_url, {'action': 'four', assignment_id: assignment_id}, function (data) {
        $('.five').removeAttr("style");
    });
});
$(document).on('click', '.five', function () {
    $.post(ajax_url, {'action': 'five', assignment_id: assignment_id}, function (data) {
    });

});

$(document).ready(function () {
    $('.tool').tooltip();
});

$(document).on('click', '.post_assignment_submit', function (e) {
    toastr.remove();
    var price = $('.price').val();
    if (price) {
        if (price < 20) {
            e.preventDefault();
            toastr.error('There is a $20 minimum.');
        }
    }
});

$(document).on('click', '.terms_conditions', function () {
    $('#myModal').modal();
});

$(document).on('click', '.privacy_policy', function () {
    $('#privacy_policy').modal();
});
$(document).on('click', '.confidentiality_policy', function () {
    $('#confidentiality_policy').modal();
});

$(document).on('click', '.contact_us', function () {
    $('#contact_modal').modal();
});

$(document).ready(function () {
    var page = $('.venmo_username').attr('data-page');
    var action = $('.venmo_username').attr('data-action');
    var venmo_username = $('.venmo_username').attr('data-venmo_username');
    if (page == 'job' && action == 'venmo') {
        if (!venmo_username) {
            $('.nav.navbar-nav').find('li a').attr('href', 'javascript:;').attr('class', 'venmo_username_required');
            $('#contact_us').attr('class', false);
            $('#contact_us').attr('class', 'venmo_username_required');
        }
        $(document).on('click', '.venmo_username_required', function () {
            alert('In order to continue, you must enter your Venmo credentials in order to be paid for your work.');
        });
    }
});

$(function () {
    $('#datetimepicker3').datetimepicker({
        format: 'LT'
    });
});