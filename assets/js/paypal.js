
$(document).on('click', '#button-confirm', function () {
    $('.show_message').fadeIn();
    var first_name = $('input[name="first_name"]').val();
    var last_name = $('input[name="last_name"]').val();
    var email_address = $('input[name="email_address"]').val();
    var phone_no = $('input[name="phone_no"]').val();
    var address = $('input[name="address"]').val();
    var city = $('input[name="city"]').val();
    var state = $('input[name="state"]').val();
    var zip = $('input[name="zip"]').val();
    var country_code = $('input[name="country_code"]').val();
    var amount = $('input[name="amount"]').val();
    var cc_type = $('select[name="cc_type"]').val();
    var cc_number = $('input[name="cc_number"]').val();
    var cc_start_date_month = $('select[name="cc_start_date_month"]').val();
    var cc_start_date_year = $('select[name="cc_start_date_year"]').val();
    var cc_expire_date_month = $('select[name="cc_expire_date_month"]').val();
    var cc_expire_date_year = $('select[name="cc_expire_date_year"]').val();
    var cc_cvv2 = $('input[name="cc_cvv2"]').val();
    var cc_issue = $('input[name="cc_issue"]').val();
    var order_id = $('.content').attr('data-order-id');
    $('.show_message').html('<i class="fa fa-spinner fa-spin"></i>Please wait!');
    $('#button-confirm').attr('disabled', true);
    $.post('send.php', {action: 'pay', first_name: first_name, last_name: last_name, email_address: email_address, phone_no: phone_no, address: address, city: city, state: state, zip: zip, country_code: country_code, amount: amount, cc_type: cc_type, cc_number: cc_number, cc_start_date_month: cc_start_date_month, cc_start_date_year: cc_start_date_year, cc_expire_date_month: cc_expire_date_month, cc_expire_date_year: cc_expire_date_year, cc_cvv2: cc_cvv2, cc_issue: cc_issue, order_id: order_id}, function (data) {
        $('#button-confirm').attr('disabled', false);
        $('.show_message').fadeOut();
        data = $.parseJSON(data);
        alert(data.status);
    });
});
