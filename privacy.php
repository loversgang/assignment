<div class="modal fade" id="privacy_policy" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Privacy Policy</h4>
            </div>
            <div class="modal-body">
                <div class="bluebox" style="height: 300px;overflow-y: scroll;">
                    <p>
                        Effective date: February 11th, 2016<br /><br />
                        Unity is a fast and simple way to get academic guidance through a paid work
                        environment with other college students across the nation. You can “seek an assignment”
                        or “post an assignment” with our system amongst college students, receive help by
                        posting an assignment, touch base by filling out details, and get paid by seeking an
                        assignment to immerse yourself in an easier load of worries for your extremely busy
                        lives., and enjoy original non plagiarized work pieces of academic assistance from the
                        world’s best students across the nation.<br />
                        Inevitably you’ll share some information with us after choosing to select one of our
                        current services and any other services we provide. For this reason, we want to be candid
                        about the information we receive and collect, including whom we share it with, how we
                        use it, and the options we grant to you to control, access, and review your information.
                        Due to this we’ve taken the time to write this privacy policy. And it’s why we’ve made
                        an effort to write it in a way that’s heartily free of the legalese that usually fills up these
                        documents. Of course, you can always email us if you still have questions about anything
                        in our privacy policy.<br /><br />
                        Information We Collect<br />
                        The information we collect is divided into 3 simple categories:<br />
                        First of which is the Information you choose to give to us. Second of which is the
                        Information we receive automatically when you use our services. And the Third is the
                        Information we get from third parties.<br />
                        The categories displayed above are also shown below with much more detail.<br /><br />
                        Information You Choose to Give Us<br />
                        Information that you choose to share with us will be collected whenever you interact with
                        our services. For example, all of our services require you to create a simple Unity
                        account, so we need to collect a few important details about you: a unique username
                        you’d like to go by but must not include your first or last name, a password, an email
                        address, a phone number, the college or university you are currently attending and your
                        date of birth. Other services, such as commerce products, may also require you to provide
                        us with a debit or credit card number and its associated account information, paypal
                        account information, or venmo account details. And, of course, when you contact Unity
                        Support or communicate with us in any other way, we’ll collect whatever information
                        you volunteer.<br />
                        Information We Get Automatically When You Use Our Services<br />
                        Information about which of the services you’ve used and how you’ve used them will be
                        collected whenever you use our services. We might know, for instance, that you posted a
                        particular assignment, filled out details in full for that particular assignment, and paid a
                        set amount of money for that assignment. Here’s a fuller explanation of the types of
                        information we collect when you use our services:<br /><br />
                        * Usage Information. Information about your activity through our services is collectedwhen you use our services. For example, we may collect information about:<br />
                    <ul>
                        <li>how you interact with the services, such as which service you decide to select
                            from our available services which include “post an assignment” and “ seek an
                            assignment”. In addition to which postings you may have posted or viewed when
                            using our services.</li>
                        <li>how you communicate with other Unity clients, such as the details included in
                            the posting, deadline posted, amount of money offered, materials attached, the
                            number of postings you post or complete, which Unity clients you exchange
                            services with the most, and your interactions with postings (such as when you
                            download or upload an assignment or materials ).</li>
                    </ul>

                    * Content Information<br/>
                    The content you provide and information about that content is collected, such as if the
                    Unity client has viewed the content, downloaded the content, and the metadata that is
                    provided with the content.<br /><br />
                    * Device Information<br />
                    Device-specific information is collected by us. For example this information may be the
                    hardware model, operating system version, unique device identifiers, browser type,
                    language, wireless network, and mobile network information (including the mobile phone
                    number).<br />
                    <ul>
                        <li>Also device information will be collected by us. In the event you experience any
                            crash or other problem when using our services, this information will be used
                            diagnose and troubleshoot such problems.</li>
                    </ul>

                    * Location Information<br />
                    Information about your location may be collected when using our services. With your
                    consent, we may also collect information about your precise location using methods that
                    include GPS, wireless networks, cell towers, Wi-Fi access points, and other sensors, such
                    as gyroscopes, accelerometers, and compasses.<br /><br />
                    * Information Collected by Cookies and Similar Technologies<br />
                    Just like almost all online services, websites and mobile applications, we may use cookies
                    and other technologies to collect information about your device, activity and browser.
                    Such other technologies include but are not limited to web beacons, web storage, and
                    unique device identifiers. When you interact with services we offer through one of our
                    affiliates, such as commerce features, these technologies to collect information may also
                    be used by us. By default, most web browsers are set to accept cookies. If you prefer, you
                    can usually remove or reject browser cookies through the settings on your browser or
                    device. If you do decide to remove or reject cookies, acknowledge that the availability
                    and functionality of our services can be affected by these actions or actions related to
                    such.<br /><br />
                    * Log Information<br />
                    Log file information is collected when you use our services. That information includes,
                    among other things:<br />
                    <ul>
                        <li>details about how you’ve used our services</li>
                        <li>device information, such as your web browser type and language</li>
                        <li>access times</li>
                        <li>pages viewed</li>
                        <li>IP address</li>
                        <li>pages you visited before navigating to our services</li>
                        <li>cookies that may uniquely identify your device or browser</li>
                    </ul>
                    * Information We Collect from Third Parties<br />
                    Information from other third-party sources may also be obtained and combined with the
                    information we collect through our services. For example, one of our affiliates may be
                    used to help us identify that you are indeed a current college student at the college or
                    university you have listed as the college or university you currently attend. their device
                    phonebook—we may combine the information we collect from our affiliates or partners
                    with other information we have collected about you.<br /><br />
                    * How We Use Information<br />
                    Aside of using the information to supply you with an incredible set of services and
                    products that we that we tenaciously improve, we also use the information to:<br />
                    <ul>
                        <li>enhance, develop, maintain, protect and provide our products and services</li>
                        <li>evaluate and monitor trends and usage<li>
                        <li>interact with you</li>
                        <li>improve the protection and security of our services</li>
                        <li>validate your identity and prevent fraud or other unauthorized or illegal activity</li>
                        <li>use information we’ve collected from cookies and other technology to enhance
                            the services and your experience with them</li>
                        <li>personalize the services by, among other things, suggesting assignments, or
                            providing advertisements, content, or features that match user profiles, interests,
                            or previous activities</li>
                    </ul>
                    Some information may also be stored locally on your device by our system. For example,
                    in order for you to be able to visit the website or use the mobile app and view the content
                    faster, information such as local cache may be stored.<br /><br />
                    Note that no matter where you live or where you happen to use our services, you consent
                    and agree to us processing and transferring information in and to the United States.<br /><br />
                    * How We Share Information<br />
                    We may share information about you in the following ways:
                    * With other Unity Clients<br /><br />
                    We may share the following information about you with other Unity clients:<br />
                    <ul>
                        <li>public information, such as your username and the college or university you
                            currently attend;</li>
                        <li>information about how you have interacted with the services, such as if you chose
                            the service to “post an assignment” or “seek an assignment”, the details of the•
                            •
                            assignment you have posted, the amount of money you are offering for a service,
                            and other information that will help Unity clients understand your connections
                            with others using the services. For example, because it may not be clear whether
                            an other Unity client is capable of assisting you, we may share which university
                            or college the other Unity client currently attends.</li>
                        <li>any additional information you have consented to be shared. For example, when
                            you attach materials, we may share information about the material you attached
                            with other users who view the posting; and</li>
                        <li>content you post will be shared with other Unity clients and potentially the public
                            at large; how widely your content is shared depends on what information you
                            decide to include or exclude and the type of service you are using.</li>
                    </ul>
                    * With our affiliates<br />
                    We may share information with entities within the Unity community of companies.<br /><br />
                    * With third parties<br />
                    We may share your information with the following third parties:
                    <ul>
                        <li>With service providers, sellers, and partners. We may share information about you
                            with service providers who perform services on our behalf, sellers that provide
                            goods through our services, and business partners that provide services and
                            functionality.
                            • With third parties for legal reasons. We may share information about you if we
                            reasonably believe that disclosing the information is needed to:
                            * comply with any valid legal process, government request, or applicable law, rule, or
                            regulation;
                            * investigate, remedy, or enforce potential Terms of Service violations;
                            * protect the rights, property, and safety of us, our users, or others; or
                            * detect and resolve any fraud or security concerns.
                            • With third parties as part of a merger or acquisition. If Unity gets involved in a
                            merger, asset sale, financing, or acquisition of some portion of our business to
                            another company, we may share your information with that company before and
                            after the transaction closes.
                            * In the aggregate<br />
                            We may also share with third parties, such as advertisers, aggregated or de-identified
                            information that cannot reasonably be used to identify you.<br /><br />
                            Links Disclaimer<br />
                            In order to introduce our clients to specific information, we may provide links to local,
                            state and federal government agencies, as well as some websites of other organizations. A
                            link does not constitute an endorsement of the content, products, accuracy, services,
                            opinions, viewpoint, policies, or accessibility of that website. Once you link to another
                            website from this one, including any maintained by the State, you are subject to the terms
                            and conditions of that website, including, but not limited to, its privacy policy.<br /><br />
                            Children<br />
                            We don’t direct our Services to anyone under 18. We also don’t direct our Services to
                            anyone who is not a student of a higher educational institution. For this reason we do not
                            knowingly collect personal information from anyone under 18 or anyone who is not a
                            student of a higher educational institution.<br /><br />
                            Revisions to the Privacy Policy<br />
                            This privacy policy can be subject to change at any time due to continuous changes in our
                            system. However, when we do update, revise or change the privacy policy, we’ll let you
                            know one way or another. Some instances our way of letting you know will be by
                            revising the date at the top of the privacy policy that’s available on our website and
                            mobile application. Other instances, we may provide you with additional notice. For
                            example, we can add a statement to our website's homepage or provide you with an in-
                            app notification or personal email.
                            </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>