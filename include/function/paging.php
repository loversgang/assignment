<?php

function PageControl($page, $totalPages, $totalRecords, $url, $querystring = '', $type = 1, $Class = 'pad', $tdclass = '', $Title = 'Records', $LClass = 'cat') {
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>
    <div class="pagination">
        <a href="<?= $url ?>?page=<?= $Pp ?>&<?= $querystring ?>" title="Previous Page">&laquo;</a>
        <?php for ($i = $begin; $i <= $totalPages && $i <= $end; $i++): ?>
            <?php if ($i == $page): ?>
                <a href="<?= $url ?>?page=<?= $i ?>&<?= $querystring ?>" class="active" title="Page No: <?= $i ?>"><?= $i ?></a>
            <?php else: ?>
                <a href="<?= $url ?>?page=<?= $i ?>&<?= $querystring ?>"  title="Page No: <?= $i ?>"><?= $i ?></a>
            <?php endif; ?>

        <?php endfor; ?>
        <a href="<?= $url ?>?page=<?= $Np ?>&<?= $querystring ?>" title="Next Page">&raquo;</a>
    </div>
    <?php
}

function PageControl_front($page, $totalPages, $totalRecords, $url, $querystring = '', $type = 1, $Class = 'pad', $tdclass = '', $Title = 'Records', $LClass = 'cat') {
    if ($type == 1):
        ?>
        <table width="100%" cellspacing="1" cellpadding="2" align="center" class="<?= $Class ?>">
            <tr>
                <td class="<?= $tdclass ?>" width="30%" align="left">Total&nbsp;<?= $Title ?>:&nbsp;&nbsp;<?= $totalRecords ?></td>
                <td align="right" >Pages:&nbsp;&nbsp;
                    <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                        <?php if ($i == $page): ?>
                            <?= display_url($i, $url, 'p=' . $i . '&' . $querystring, 'blockselected'); ?>
                        <?php else: ?>
                            <?= display_url($i, $url, 'p=' . $i . '&' . $querystring, $LClass); ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                </td>
            </tr>
        </table>
        <?php
    elseif ($type == 2):
        # $Pp-previous page
        # $Np- next page
        ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
        ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
        if ($totalPages > 3):
            if (($page + 3) <= $totalPages):
                $end = $page + 3;
                $begin = $page;
            else:
                $begin = $totalPages - 3;
                $end = $totalPages;
            endif;
        else:
            $begin = 1;
            $end = $totalPages;
        endif;
        ?>


        <div class="news_page_links">
            <ul>  
                <li>  <a href="<?= make_url($url, 'p=' . $Pp . '&' . $querystring) ?>" title="Previous Page"><?php echo " PREV" ?></a>|</li>
                <?php
                for ($i = $begin; $i <= $totalPages && $i <= $end; $i++):
                    if ($i == $page):
                        ?>
                        <li> <?php echo display_url($i, $url, 'p=' . $i . '&' . $querystring, 'blockselected'); ?>|</li>
                    <?php else: ?>
                        <li><?php echo display_url($i, $url, 'p=' . $i . '&' . $querystring, $LClass); ?>|</li>
                    <?php
                    endif;
                endfor;
                ?>
                <li><a href="<?= make_url($url, 'p=' . $Np . '&' . $querystring) ?>"  title="Next Page"  >&nbsp;<?php echo "NEXT" ?></a></li>
            </ul>      
        </div>

        <?php
    endif;
}

function PageControl_front_ajax($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 


    <a href="javascript:void(0)" id="<?php echo $Pp ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" title="Previous Page" class="paging_change unselected funder_prev"></a>
    <div class="founder_link">
        <ul> 
            <?php
            for ($i = $begin; $i <= $totalPages && $i <= $end; $i++):

                if ($i == $page):
                    ?>

                    <li>  <a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change" ><span class="founder_link_select"><?php echo $i ?></span></a></li>

                <?php else: ?>
                    <li>  <a href="javascript:void(0)" id="<?php echo $i ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change unselected"  ><?php echo $i ?></a></li>

                <?php
                endif;
            endfor;
            ?>
        </ul> 
    </div>    
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change unselected funder_next"  title="Next Page"></a>

    <?php
}

function PageControl_front_ajax_load_more($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_ajax_load_more_twitter($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change_twitter unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_ajax_load_more_flickr($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change_flickr unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_ajax_load_more_youtube($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change_youtube unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_ajax_load_more_wordpress($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change_wordpress unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_ajax_load_more_community($page, $totalPages, $totalRecords, $url, $querystring = '', $max_records = '10', $menu_id = '2') {
    /* Paging by Ajax */
    # $Pp-previous page
    # $Np- next page
    ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
    ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
    if ($totalPages > 3):
        if (($page + 3) <= $totalPages):
            $end = $page + 3;
            $begin = $page;
        else:
            $begin = $totalPages - 3;
            $end = $totalPages;
        endif;
    else:
        $begin = 1;
        $end = $totalPages;
    endif;
    ?>

    <!--Here in "id" we set page and in "rel" we set total pages , "max_records" here we set max records on a page ,"query_string" we pass query string "menu id" represents menu id--> 
    <div style="clear:both"></div>
    <a href="javascript:void(0)" id="<?php echo $Np ?>" rel="<?php echo $totalPages ?>" max_records="<?php echo $max_records ?>" menu_id="<?php echo $menu_id ?>" query_string="<?php echo $querystring ?>" class="paging_change_community unselected load_more"  title="Load More"></a>
    <div style="clear:both"></div>    
    <?php
}

function PageControl_front_tabs($page, $totalPages, $totalRecords, $url, $querystring = '', $type = 1, $Class = 'pad', $tdclass = '', $Title = 'Records', $LClass = 'cat', $pag = '') {
    if ($type == 1):
        ?>
        <table width="100%" cellspacing="1" cellpadding="2" align="center" class="<?= $Class ?>">
            <tr>
                <td class="<?= $tdclass ?>" width="30%" align="left">Total&nbsp;<?= $Title ?>:&nbsp;&nbsp;<?= $totalRecords ?></td>
                <td align="right" >Pages:&nbsp;&nbsp;
                    <?php for ($i = 1; $i <= $totalPages; $i++): ?>
                        <?php if ($i == $page): ?>
                            <?= display_url($i, $url, 'pag=' . $i . '&' . $querystring, 'blockselected'); ?>
                        <?php else: ?>
                            <?= display_url($i, $url, 'pag=' . $i . '&' . $querystring, $LClass); ?>
                        <?php endif; ?>
                    <?php endfor; ?>
                </td>
            </tr>
        </table>
        <?php
    elseif ($type == 2):
        # $Pp-previous page
        # $Np- next page
        ($page >= $totalPages) ? $Np = $totalPages : $Np = $page + 1;
        ($page <= 1) ? $Pp = 1 : $Pp = $page - 1;
        if ($totalPages > 3):
            if (($page + 3) <= $totalPages):
                $end = $page + 3;
                $begin = $page;
            else:
                $begin = $totalPages - 3;
                $end = $totalPages;
            endif;
        else:
            $begin = 1;
            $end = $totalPages;
        endif;
        ?>
        <table width="100%" cellspacing="0" cellpadding="0" align="center"  >
            <tr >
                <td  align="right" class="page_height" >
                    <div class="page_outer">
                        <a href="<?= make_url($url) ?>pag=<?= $Pp ?>&<?= $querystring ?>" class="pnp" title="Previous Page"><?php echo"<<" ?></a>&nbsp;
                        <?php
                        for ($i = $begin; $i <= $totalPages && $i <= $end; $i++):
                            if ($i == $page):
                                echo display_url($i, $url, 'pag=' . $i . '&' . $querystring, 'blockselected');
                                echo'&nbsp;&nbsp;';
                            else:
                                echo display_url($i, $url, 'pag=' . $i . '&' . $querystring, $LClass);
                                echo'&nbsp;&nbsp;';
                            endif;
                        endfor;
                        ?>
                        <a href="<?= make_url($url) ?>pag=<?= $Np ?>&<?= $querystring ?>" class="pnp" title="Next Page"  ><?php echo">>" ?></a>
                    </div>
                </td>
            </tr>
        </thead>
        </table>
        <?php
    endif;
}
?>