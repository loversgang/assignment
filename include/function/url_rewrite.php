<?php

function make_url($page, $query = NULL) {
    global $conf_rewrite_url;
    parse_str($query, $string);
    if (isset($conf_rewrite_url[strtolower($page)]))
        return _makeurl($page, $string);
    else
        return DIR_WS_SITE . '?page=' . $page . '&' . $query;
}

function verify_string($string) {

    if ($string != '')
        if (substr($string, -1) == '/')
            return substr($string, 0, strlen($string) - 1);

    return $string;
}

function load_url() {
    global $conf_rewrite_url;
    $prefix = str_replace(HTTP_SERVER, '', DIR_WS_SITE);
    $URL = $_SERVER['REQUEST_URI'];
    if (strpos($URL, '?') === false):
        $string = substr($URL, -(strlen($URL) - strlen($prefix)));
        $string = verify_string($string);
        $string_parts = explode('/', $string);
        $url_array = array_flip($conf_rewrite_url);

        if (isset($url_array[$string_parts['0']])) {
            _load($url_array[$string_parts['0']], $string_parts);
        }
    endif;
}

function _makeurl($page, $string) {

    switch ($page) {
        case 'home':
            return DIR_WS_SITE;
            break;

        case 'community':

            return DIR_WS_SITE . 'buzz';

            break;
        case 'content':
            if (isset($string['id'])):
                $object = get_object('content', $string['id']);
                return DIR_WS_SITE . 'content/' . $object->urlname;
            endif;
            break;
        case 'blog':
            if (isset($string['id'])):
                $object = get_object('blog', $string['id']);
                return DIR_WS_SITE . 'blog/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'blog';
            endif;
            break;
        case 'campaign_category':
            if (isset($string['id'])):
                $object = get_object('project_category', $string['id']);
                return DIR_WS_SITE . 'field-of-study/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'field-of-study';
            endif;
            break;


        case 'campaign_perk':
            if (isset($string['id'])):

                $object = get_object('dropdown_values', $string['id']);
                return DIR_WS_SITE . 'perk-type/' . $object->value;
            else:
                return DIR_WS_SITE . 'perk-type';
            endif;
            break;

        case 'campaign_location':
            if (isset($string['location'])):

                return DIR_WS_SITE . 'location/' . strtolower($string['location']);
            else:
                return DIR_WS_SITE . 'location';
            endif;
            break;
        case 'campaign_closeby':
            if (isset($string['location'])):

                return DIR_WS_SITE . 'close-by/' . strtolower(trim($string['location']));
            else:
                return DIR_WS_SITE . 'close-by';
            endif;
            break;
        case 'campaign':
            if (isset($string['id'])):
                $object = get_object('project', $string['id']);
                return DIR_WS_SITE . 'campaign/' . $object->urlname . "-" . $string['id'];
            else:
                return DIR_WS_SITE . 'campaign';
            endif;
            break;
        case 'user_profile':
            if (isset($string['id'])):
                $object = get_object('user', $string['id']);
                return DIR_WS_SITE . 'user/' . strtolower($object->first_name . "-" . $string['id']);
            else:
                return DIR_WS_SITE . 'user';
            endif;
            break;

        case 'user_profile_campaign':
            if (isset($string['id'])):
                $object = get_object('user', $string['id']);
                return DIR_WS_SITE . 'user-campaigns/' . strtolower($object->first_name . "-" . $string['id']);
            else:
                return DIR_WS_SITE . 'user-campaigns';
            endif;
            break;

        case 'campaigns':
            if (isset($string['id'])):
                $object = get_object('project_admin_category', $string['id']);
                return DIR_WS_SITE . 'campaigns/' . $object->urlname;
            else:
                return DIR_WS_SITE . 'campaigns';
            endif;
            break;
        case 'myaccount':

            return DIR_WS_SITE . 'my-campaign';

            break;

        case 'previous_campaign':

            return DIR_WS_SITE . 'my-previous-campaigns';

            break;

        case 'my_funding_history':

            return DIR_WS_SITE . 'rewards-i-bought';

            break;

        case 'perks_for_me':

            return DIR_WS_SITE . 'rewards-i-sold';

            break;

        case 'changepassword':
            if (isset($string['msg'])):
                return DIR_WS_SITE . 'change-password/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'change-password';
            endif;
            break;

        case 'forgotpassword':

            if (isset($string['msg'])):
                return DIR_WS_SITE . 'forgot-password/' . $string['msg'];
            else:
                return DIR_WS_SITE . 'forgot-password';
            endif;
            break;
        case 'logout':
            return DIR_WS_SITE . 'logout';
            break;
        case 'login':
            return DIR_WS_SITE . 'login';
            break;


        default: break;
    }
}

function _load($page, $string_parts) {
    global $conf_rewrite_url;
    switch ($page) {
        case 'home':
            return DIR_WS_SITE;
            break;

        case 'community':

            $_REQUEST['page'] = 'community';

            break;

        case 'content':
            if (count($string_parts) == 2):
                $object = get_object_by_col('content', 'urlname', urldecode($string_parts['1']));
                $_REQUEST['page'] = 'content';
                $_GET['id'] = $object->id;
            endif;
            break;
        case 'blog':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'blog';
                $object = get_object_by_col('blog', 'urlname', $string_parts['1']);
                $_GET['id'] = $object->id;
            else:
                $_REQUEST['page'] = 'blog';
            endif;
            break;
        case 'campaign_category':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaign_category';
                $object = get_object_by_col('project_category', 'urlname', $string_parts['1']);
                $_GET['id'] = $object->id;
            else:
                $_REQUEST['page'] = 'campaign_category';
            endif;
            break;
        case 'campaign_perk':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaign_perk';
                $object = get_object_by_col_without_delete('dropdown_values', 'value', $string_parts['1']);
                $_GET['id'] = $object->id;
            else:
                $_REQUEST['page'] = 'campaign_perk';
            endif;
            break;
        case 'campaign_location':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaign_location';
                $_GET['location'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'campaign_location';
            endif;
            break;

        case 'campaign_closeby':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaign_closeby';
                $_GET['location'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'campaign_closeby';
            endif;
            break;

        case 'campaign':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaign';
                $object = get_object_by_col('project', 'urlname', $string_parts['1']);
                $_GET['id'] = array_pop(explode('-', $string_parts['1']));
            else:
                $_REQUEST['page'] = 'campaign';
            endif;
            break;
        case 'user_profile':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'user_profile';
                $_GET['id'] = array_pop(explode('-', $string_parts['1']));
            else:
                $_REQUEST['page'] = 'user_profile';
            endif;
            break;

        case 'user_profile_campaign':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'user_profile_campaign';
                $_GET['id'] = array_pop(explode('-', $string_parts['1']));
            else:
                $_REQUEST['page'] = 'user_profile_campaign';
            endif;
            break;

        case 'campaigns':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'campaigns';
                $object = get_object_by_col_without_delete('project_admin_category', 'urlname', $string_parts['1']);
                $_GET['id'] = $object->id;
            else:
                $_REQUEST['page'] = 'campaigns';
            endif;
            break;

        case 'myaccount':

            $_REQUEST['page'] = 'myaccount';

            break;
        case 'previous_campaign':

            $_REQUEST['page'] = 'previous_campaign';

            break;

        case 'my_funding_history':

            $_REQUEST['page'] = 'my_funding_history';

            break;

        case 'perks_for_me':

            $_REQUEST['page'] = 'perks_for_me';

            break;

        case 'changepassword':
            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'changepassword';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'changepassword';
            endif;
            break;

        case 'forgotpassword':

            if (count($string_parts) == 2):
                $_REQUEST['page'] = 'forgotpassword';
                $_GET['msg'] = $string_parts['1'];
            else:
                $_REQUEST['page'] = 'forgotpassword';
            endif;
            break;
        case 'logout':
            $_REQUEST['page'] = 'logout';
            break;
        case 'login':
            $_REQUEST['page'] = 'login';
            break;
        default:break;
    }
}

?>