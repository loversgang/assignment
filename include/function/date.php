<?php

function ToUKDate($date) {
    return date("d-m-Y", strtotime($date));
}

function ToUSDate($date) {

    $date = date("m/d/Y", strtotime($date));
    $Parts = array();
    $Parts = explode('/', $date);
    $Result = $Parts['2'] . '-' . $Parts['0'] . '-' . $Parts['1'];
    return $Result;
}

?>