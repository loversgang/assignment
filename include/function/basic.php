<?php

function include_functions($functions) {
    foreach ($functions as $value):
        if (file_exists(DIR_FS_SITE . 'include/function/' . $value . '.php')):
            include_once(DIR_FS_SITE . 'include/function/' . $value . '.php');
        endif;
    endforeach;
}

function __autoload($class_name) {
    include DIR_FS_SITE . 'include/functionClass/' . $class_name . '.php';
}

function display_message($unset = 0) {
    $admin_user = new admin_session();

    if ($admin_user->isset_pass_msg()):
        if (isset($_SESSION['admin_session_secure']['msg_type']) && $_SESSION['admin_session_secure']['msg_type'] == '1'):
            $error_type = 'danger';
        else:
            $error_type = 'success';
        endif;
        foreach ($admin_user->get_pass_msg() as $value):
            echo '<div class="fadeOut-notif alert alert-' . $error_type . '"><strong>';
            echo $value;
            echo '</strong></div>';
        endforeach;
    endif;
    ($unset) ? $admin_user->unset_pass_msg() : '';
}

function get_var_if_set($array, $var, $preset = '') {

    return isset($array[$var]) ? $array[$var] : $preset;
}

function get_var_set($array, $var, $var1) {
    if (isset($array[$var])):
        return $array[$var];
    else:
        return $var1;
    endif;
}

function get_template($template_name, $array, $selected = '') {
    include_once(DIR_FS_SITE . 'template/' . TEMPLATE . '/' . $template_name . '.php');
}

function get_meta($page) {
    $page = trim($page);
    if ($page != ''):
        $query = new query('keywords');
        $query->Where = "where page_name='$page'";
        if ($content = $query->DisplayOne()):
            return $content;
        else:
            return null;
        endif;
    endif;
    return null;
}

function head($content = '') {
    /* include all the header related things here... like... common meta tags/javascript files etc. */
    global $page;
    global $content;
    if (is_object($content)):
        ?>
        <title><?php echo isset($content->name) && $content->name ? $content->name : ''; ?></title>	
        <meta name="viewport" content="width=device-width,user-scale=1"/>
        <meta name="keywords" content="<?php echo isset($content->meta_keyword) ? $content->meta_keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->meta_description) ? $content->meta_description : ''; ?>" />
        <?php
    else:
        $content = get_meta($page);
        ?>
        <title><?php echo isset($content->page_title) && $content->page_title ? $content->page_title : ''; ?></title>	
        <meta name="keywords" content="<?php echo isset($content->keyword) ? $content->keyword : ''; ?>" />
        <meta name="description" content="<?php echo isset($content->description) ? $content->description : ''; ?>" />
    <?php endif; ?>
    <link rel="shortcut icon" href="<?php echo DIR_WS_SITE_GRAPHIC ?>favicon.ico" />
    <?php include_once(DIR_FS_SITE . 'include/template/stats/google_analytics.php'); ?>
    <?php
}

function css($array = array('reset', 'master')) {

    echo '<link href="' . DIR_WS_SITE . 'css/print.css" rel="stylesheet" type="text/css" media="print" >' . "\n";

    echo '<link href="' . DIR_WS_SITE . 'css/base.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";



    foreach ($array as $k => $v):

        if ($v == 'style' && isset($_SESSION['use_stylesheet'])):

            echo '<link href="' . DIR_WS_SITE . 'css/' . $_SESSION['use_stylesheet'] . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";

        else:

            echo '<link href="' . DIR_WS_SITE . 'css/' . $v . '.css" rel="stylesheet" type="text/css" media="screen, projection" >' . "\n";

        endif;

    endforeach;

    echo '<link href="' . DIR_WS_SITE . 'javascript/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";

    echo '<link href="' . DIR_WS_SITE . 'css/validation/css/validationEngine.jquery.css" rel="stylesheet" type="text/css" media="screen" >' . "\n";
}

function js($array = array('jquery-1.2.6.min', 'search-reset')) {

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/jquery-1.7.2.min.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'fancybox/lib/jquery.mousewheel-3.0.6.pack.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox/jquery.mousewheel-3.0.4.pack.js" type="text/javascript"></script> ' . "\n";





    foreach ($array as $k => $v):

        echo '<script src="' . DIR_WS_SITE . 'javascript/' . $v . '.js" type="text/javascript"></script> ' . "\n";

    endforeach;

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/languages/jquery.validationEngine-en.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation/js/jquery.validationEngine.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/email/resettable.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/validation.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/fancybox.js" type="text/javascript"></script> ' . "\n";

    echo '<script src="' . DIR_WS_SITE . 'javascript/sharethis.js" type="text/javascript"></script> ' . "\n";
}

function body() {

    /* # include all the body related things like... tracking code here. */
}

function footer() {

    /* # if you need to add something to the website footer... please add here. */
}

function array_map_recursive($callback, $array) {

    $b = Array();

    foreach ($array as $key => $value) {

        $b[$key] = is_array($value) ? array_map_recursive($callback, $value) : call_user_func($callback, $value);
    }

    return $b;
}

function if_set_in_post_then_display($var) {

    if (isset($_POST[$var])):

        echo $_POST[$var];

    endif;

    echo '';
}

function set_data_in_session($POST) {

    unset($_SESSION['user_session']['register_data']);
    $register_data = array();
    $register_data['first_name'] = isset($POST['first_name']) ? $POST['first_name'] : "";
    $register_data['last_name'] = isset($POST['first_name']) ? $POST['last_name'] : "";
    $register_data['username'] = isset($POST['first_name']) ? $POST['username'] : "";
    $_SESSION['user_session']['register_data'] = $register_data;
}

function if_set_in_session_then_display($var) {

    if (isset($_SESSION['user_session']['register_data'][$var])):

        echo $_SESSION['user_session']['register_data'][$var];

    endif;

    echo '';
}

function validate_captcha() {

    global $privatekey;



    if ($_POST["recaptcha_response_field"]) {

        $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

        if ($resp->is_valid) {

            return true;
        } else {

            /* # set the error code so that we can display it */

            return false;
        }
    }
}

function is_set($array = array(), $item, $default = 1) {

    if (isset($array[$item]) && $array[$item] != 0) {

        return $array[$item];
    } else {

        return $default;
    }
}

function limit_text($text, $limit = 100) {

    if (strlen($text) > $limit):

        return substr($text, 0, strpos($text, ' ', $limit));

    else:

        return $text;

    endif;
}

function get_object($tablename, $id, $type = 'object') {

    $query = new query($tablename);
    $query->Where = "where id='$id'";

    return $query->DisplayOne($type);
}

function get_object_by_col($tablename, $col, $col_value, $type = 'object') {

    $query = new query($tablename);
    $query->Where = "where $col='$col_value' AND is_deleted='0'";

    return $query->DisplayOne($type);
}

function get_object_by_col_without_delete($tablename, $col, $col_value, $type = 'object') {

    $query = new query($tablename);

    $query->Where = "where $col='$col_value'";

    return $query->DisplayOne($type);
}

function get_object_var($tablename, $id, $var) {

    $q = new query($tablename);

    $q->Field = "$var";

    $q->Where = "where id='" . $id . "'";

    if ($obj = $q->DisplayOne()):

        return $obj->$var;

    else:

        return false;

    endif;
}

function echo_y_or_n($status) {

    echo ($status) ? 'Yes' : 'No';
}

function target_dropdown($name, $selected = '', $tabindex = 1) {

    $values = array('new window' => '_blank', 'same window' => '_parent');

    echo '<select name="' . $name . '" size="1" tabindex="' . $tabindex . '">';

    foreach ($values as $k => $v):

        if ($v == $selected):

            echo '<option value="' . $v . '" selected>' . ucfirst($k) . '</option>';

        else:

            echo '<option value="' . $v . '">' . ucfirst($k) . '</option>';

        endif;

    endforeach;

    echo '</select>';
}

function make_csv_from_array($array) {

    $sr = 1;

    $heading = '';

    $file = '';

    foreach ($array as $k => $v):

        foreach ($v as $key => $value):

            if ($sr == 1):$heading.=$key . ', ';
            endif;

            $file.=str_replace("\r\n", "<<>>", str_replace(",", ".", html_entity_decode($value))) . ', ';

        endforeach;

        $file = substr($file, 0, strlen($file) - 2);

        $file.="\n";

        $sr++;

    endforeach;

    return $file = $heading . "\n" . $file;
}

function get_y_n_drop_down($name, $selected) {

    echo '<select class="regular" name="' . $name . '">';

    if ($selected):

        echo '<option value="1" selected>Yes</option>';

        echo '<option value="0">No</option>';

    else:

        echo '<option value="0" selected>No</option>';

        echo '<option value="1">Yes</option>';

    endif;

    echo '</select>';
}

function get_setting_control($key, $type, $value) {

    switch ($type) {

        case 'text':

            echo '<input type="text" name="key[' . $key . ']" value="' . $value . '" class="form-control">';

            break;

        case 'textarea':

            echo '<textarea class="form-control" name="key[' . $key . ']" rows="3" tabindex="6">' . $value . '</textarea>';

            break;

        case 'select':

            echo get_y_n_drop_down('key[' . $key . ']', $value);

            break;

        default: echo get_y_n_drop_down('key[' . $key . ']', $value);
    }
}

function css_active($page, $value, $class) {

    if ($page == $value)
        echo 'class=' . $class;
}

function parse_into_array($string) {

    return explode(',', $string);
}

function get_section_heading($Page) {

    switch ($Page) {

        case 'product':

        case 'category':

        case 'cat_product':

        case 'product_search':

        case 'product_map':

        case 'product_collection':

        case 'product_check':

            return 'Product Manager';

            break;

        case 'review':

            return 'Review Manager';

            break;

        case 'gallery':

            return 'Gallery Manager';

            break;

        case 'gallery_image':

            return 'Image Manager';

            break;

        case 'meet_the_team':

            return 'Team Manager';

            break;

        case 'project':

            return 'Project Manager';

            break;

        case 'project_image':

            return 'Project Image Manager';

            break;

        case 'user':

        case 'search_user':

        case 'udetail':

            return 'User Manager';

            break;

        case 'jobposting':

            return 'Job Postings Manager';

            break;

        case 'news':

            return 'News Manager';

            break;

        case 'link_heading':

            return 'Links Heading Manager';

            break;

        case 'links':

            return 'Links Manager';

            break;

        case 'department':

            return 'Department Manager';

            break;

        case 'business_category':

            return 'Business Directory Manager';

            break;

        case 'directory':

            return 'Business Directory Manager';

            break;

        case 'quick_pos':

            return 'Home Manager';

            break;

            break;

        case 'quick_pos_image':

            return 'Manage Home Images';

            break;

        case 'discount':

            return 'Discounts Manager';

            break;

        case 'setting':

            return 'Website Settings';

            break;

        case 'content':return 'Page Manager';

            break;

        case 'content_collection':

        case 'content_navigation':return 'Navigation Manager';

            break;

        case 'content_photo':

            return 'Content Manager';

            break;

        case 'order':

        case 'order_d':

        case 'a_order':

        case 'archive':

        case 'search_order':

            return 'Order Manager';

            break;

        case 'all_attribute':

            return 'Attribute Manager';

            break;

        case 'email':

            return 'Email Manager';

            break;

        case 'new_letter':

        case 'letters':

        case 'send_to':

            return 'Newsletter Manager';

            break;

        case 'event':

            return 'Event Manager';

            break;

        case 'videos':

            return 'Videos Manager';

            break;

        case 'faq':

            return 'FAQ Manager';

            break;

        case 'articles':

            return 'Article Manager';

            break;

        case 'blog':

            return 'Blog Manager';

            break;

        case 'success_stories':

            return 'Success Story Manager';

            break;



        case 'keywords_management':

            return 'Keywords Management';

            break;



        case 'services':

            return 'Footer Service Links Manager';

            break;

        case 'cities':

            return 'Footer City Links Manager';

            break;

        case 'glossary':

            return 'Glossary Manager';

            break;

        case 'admin':

            return 'Admin Manager';

            break;



        case 'banner':

            return 'Slider Manager';

            break;

        case 'zones':

        case 'ship':

        case 'zone_country':

        case 'country':

            return 'Delivery Manager';

            break;

        case 'home':

            return 'Dashboard';

            break;

        case 'upload_logo':

            return 'Company Links Manager';

            break;

        default:

            return $Page;
    }
}

function MakeDataArray($post, $not) {

    $data = array();

    foreach ($post as $key => $value):

        if (!in_array($key, $not)):

            $data[$key] = $value;

        endif;

    endforeach;

    return $data;
}

function is_var_set_in_post($var, $check_value = false) {

    if (isset($_POST[$var])):

        if ($check_value):

            if ($_POST[$var] === $check_value):

                return true;

            else:

                return false;

            endif;

        endif;

        return true;

    else:

        return false;

    endif;
}

function display_form_error() {

    $login_session = new user_session();

    if ($login_session->isset_pass_msg()):

        $array = $login_session->get_pass_msg();
        ?>

        <div class="errorMsg">

            <?php
            foreach ($array as $value):

                echo $value . '<br/>';

            endforeach;
            ?>

        </div>

    <?php elseif (isset($_GET['msg']) && $_GET['msg'] != ''):
        ?>

        <div class="errorMsg">

            <?php echo urldecode($_GET['msg']) . '<br/>'; ?>

        </div>

        <?php
    endif;

    $login_session->isset_pass_msg() ? $login_session->unset_pass_msg() : '';
}

function Redirect($URL) {
    @header("location:$URL", TRUE);
    exit;
}

function Redirect1($filename) {

    if (!headers_sent())
        header('Location: ' . $filename);

    else {

        echo '<script type="text/javascript">';

        echo 'window.location.href="' . $filename . '";';

        echo '</script>';

        echo '<noscript>';

        echo '<meta http-equiv="refresh" content="0;url=' . $filename . '" />';

        echo '</noscript>';
    }
}

function re_direct($URL) {

    header("location:$URL");

    exit;
}

/* function make_url($page, $query=null)

  {

  return DIR_WS_SITE.'?page='.$page.'&'.$query;

  } */

function display_url($title, $page, $query = '', $class = '') {

    return '<a href="' . make_url($page, $query) . '" class="' . $class . '">' . $title . '</a>';
}

function make_admin_url($page, $action = 'list', $section = 'list', $query = '') {

    return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . ($query ? '&' . $query : '');
}

function make_load_url($page, $action = 'list', $section = 'list', $query = '') {

    return 'control.php?Page=' . $page . '&action=' . $action . '&section=' . $section . ($query ? '&' . $query : '');
}

function load_action_url($page, $action = 'list') {

    return 'control.php?Page=' . $page . '&action=' . $action;
}

function make_admin_url2($page, $action = 'list', $section = 'list', $query = '') {

    if ($page == 'home'):

        return DIR_WS_SITE . 'index.php';

    else:

        return DIR_WS_SITE_CONTROL . 'control.php?Page=' . $page . '&action2=' . $action . '&section2=' . $section . '&' . $query;

    endif;
}

function prepare_query($queryString) {



    $string = '';

    parse_str($queryString, $string);

    switch ($string['page']):

        case 'content':

            $query = new query('content');

            $name = $string['id'];

            $query->Where = "where name='$name'";

            $object = $query->DisplayOne();

            $_GET['id'] = $object->id;

            $_REQUEST['id'] = $object->id;

            break;

        case 'category':

            $category_name = strtolower(str_replace('-', " ", $string['id']));

            $id = get_category_id_by_name($category_name);

            $_GET['id'] = $id;
            $_REQUEST['id'] = $id;

            isset($string['p']) ? $_GET['p'] = $string['p'] : '';

            $_GET['page'] = 'product';
            $_REQUEST['page'] = 'product';

            break;

        case 'pdetail':

            $category_name = strtolower(str_replace('-', " ", $string['id']));

            if ($id = get_product_id_by_url_name($category_name)):

                $_GET['id'] = $id;
                $_REQUEST['id'] = $id;

                $_GET['page'] = 'pdetail';
                $_REQUEST['page'] = 'pdetail';

            elseif ($id = get_product_id_by_product_name($category_name)):

                $_GET['id'] = $id;
                $_REQUEST['id'] = $id;

                $_GET['page'] = 'pdetail';
                $_REQUEST['page'] = 'pdetail';

            endif;

            break;

        case 'wish_list':

            $_GET['page'] = 'wish_list';
            $_REQUEST['page'] = 'wish_list';

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            isset($string['id']) ? $_REQUEST['id'] = $string['id'] : '';

            isset($string['delete']) ? $_GET['delete'] = 1 : '';

            isset($string['add_wishlist']) ? $_GET['add_wishlist'] = 1 : '';

            break;

        case 'search':

            $_GET['page'] = 'search';
            $_REQUEST['page'] = 'search';

            isset($string['category']) ? $_GET['category'] = $string['category'] : '';

            isset($string['keyword']) ? $_GET['keyword'] = $string['keyword'] : '';

            isset($string['p']) ? $_GET['p'] = $string['p'] : '';

            break;

        case 'payment':

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            break;

        case 'home':

            isset($string['download']) ? $_GET['download'] = $string['download'] : '';

            break;

        default:

            isset($string['id']) ? $_GET['id'] = $string['id'] : '';

            isset($string['msg']) ? $_GET['msg'] = str_replace('-', ' ', $string['msg']) : '';

            !isset($_GET['page']) ? $_GET['page'] = 'home' : '';

            break;

    endswitch;
}

function make_admin_link($url, $text, $title = '', $class = '', $alt = '') {

    return '<a href="' . $url . '" class="' . $class . '" title="' . $title . '" alt="' . $alt . '" >' . $text . '</a>';
}

function quit($message = 'processing stopped here') {

    echo $message;

    exit;
}

function download_orders($payment_status, $order_status) {

    $orders = new query('orders');

    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";

    if ($order_status == 'paid'):

        $orders->Where = "where payment_status=" . $payment_status . " and order_status!='delivered'";

    elseif ($order_status == 'attempted'):

        $orders->Where = "where payment_status=" . $payment_status . " and order_status='received'";

    else:

        $orders->Where = "where payment_status=" . $payment_status . " and order_status='delivered'";

    endif;

    $orders->DisplayAll();



    $orders_arr = array();

    if ($orders->GetNumRows()):

        while ($order = $orders->GetArrayFromRecord()):

            $order['Username'] = get_username_by_orders($order['user_id']);

            array_push($orders_arr, $order);

        endwhile;

    endif;

    $file = make_csv_from_array($orders_arr);

    $filename = "orders" . '.csv';

    $fh = @fopen('download/' . $filename, "w");

    fwrite($fh, $file);

    fclose($fh);

    download_file('download/' . $filename);
}

function download_orders_by_criteria($from_date, $to_date, $payment_status, $order_status) {

    $orders = new query('orders');

    $orders->Field = "id,user_id,sub_total,vat,shipping,voucher_code,voucher_amount,shipping_firstname,shipping_lastname,shipping_address1,shipping_address2,shipping_city,shipping_state,shipping_zip,shipping_country,shipping_phone,shipping_fax,billing_firstname,billing_lastname,billing_email,billing_address1,billing_address2,billing_city,billing_state,billing_zip,billing_country,billing_phone,billing_fax,grand_total,order_type,order_status,order_date,ip_address,order_comment,shipping_comment,cart_id";

    $orders->Where = "where order_status='$order_status' AND payment_status='$payment_status' AND (order_date BETWEEN CAST('$from_date' as DATETIME) AND CAST('$to_date' as DATETIME)) order by order_date";

    $orders->DisplayAll();



    $orders_arr = array();

    if ($orders->GetNumRows()):

        while ($order = $orders->GetArrayFromRecord()):

            $order['Username'] = get_username_by_orders($order['user_id']);

            array_push($orders_arr, $order);

        endwhile;

    endif;

    $file = make_csv_from_array($orders_arr);

    $filename = "orders" . '.csv';

    $fh = @fopen('download/' . $filename, "w");

    fwrite($fh, $file);

    fclose($fh);

    download_file('download/' . $filename);
}

function get_username_by_orders($id) {
    if ($id == 0):

        return 'Guest';

    elseif ($id):

        $q = new query('user');

        $q->Field = "firstname,lastname";

        $q->Where = "where id='" . $id . "'";

        $o = $q->DisplayOne();

        return $o->firstname;

    endif;
}

function get_zones_box($selected = 0) {

    $q = new query('zone');

    $q->DisplayAll();

    echo '<select name="zone" size="1">';

    while ($obj = $q->GetObjectFromRecord()):

        if ($selected = $obj->id):

            echo '<option value="' . $obj->id . '" selected>' . $obj->name . '</option>';

        else:

            echo '<option value="' . $obj->id . '">' . $obj->name . '</option>';

        endif;

    endwhile;

    echo '</select>';
}

function zone_drop_down($zone_id, $id, $s, $z) {

    $query = new query('zone_country');

    $query->Where = "where zone_id=$zone_id";

    $query->DisplayAll();

    $country_list = array();

    $country_name = '';

    while ($object = $query->GetObjectFromRecord()):

        $country_name = get_country_name_by_id($object->country_id);

        $idd = $object->country_id;

        /* $country_list('id'=>$object->id, 'name'=>$country_name)); */

        array_push($country_list, array('name' => $country_name, 'id' => $object->id));

    /* $country_list[$object->id]=$country_name; */

    endwhile;

    $total_list = array();

    foreach ($country_list as $k => $v):

        $total_list[] = $v['name'];

    endforeach;

    array_multisort($total_list, SORT_ASC, $country_list);



    echo '<select name="' . $id . '" size="10" style="width:200px;" multiple>';

    foreach ($country_list as $k => $v):

        if (($z == $zone_id) && $s):

            echo '<option value="' . $v['id'] . '" selected="selected">' . ucfirst($v['name']) . '</option>';

        else:

            echo '<option value="' . $v['id'] . '">' . ucfirst($v['name']) . '</option>';

        endif;

    endforeach;

    echo'</select>';
}

function get_page_head_description($page = 'home') {

    $defoult_text_for_home = "Whether you live in the province, have visited Newfoundland, or are a homesick Newfie looking for some much-needed reminders of The Rock, Island Treasures is your one source for everything Newfoundland and Labrador.";

    $defoult_text = "Mauris hendrerit ligula ut sapien varius in adipiscing nisl cursus. Nullam varius lacinia hendrerit. Ut eleifend porttitor porta. In et justo justo, eget dignissim lorem. Etiam eleifend enim eu purus fermentum a vulputate diam dapibus.";

    switch ($page) {

        case 'product':

            global $category;

            if (isset($_GET['id'])):

                echo html_entity_decode(limit_text($category->description, 303));

            else:

                echo html_entity_decode($defoult_text);

            endif;

            break;

        case 'content':

            global $object;

            echo html_entity_decode(limit_text($object->top_content, 303));

            break;

        case 'home':

            global $object;

            echo html_entity_decode($defoult_text_for_home);

            break;

        case 'pdetail':

            global $object;

            echo html_entity_decode(limit_text($object->top_description, 303));

            break;

        default:

            echo html_entity_decode($defoult_text);
    }
}

/* Function to add meta tags to content pages */

function add_metatags($title = "", $keyword = "", $description = "") {
    /* description rule */
    $description_length = 150; /* characters */
    if (strlen($description) > $description_length)
        $description = substr($description, 0, $description_length);

    /* title rule */
    $title_length = 100;
    if (strlen($title) > $title_length)
        $title = SITE_NAME . ' | ' . substr($title, 0, $title_length);
    else
        $title = SITE_NAME . ' | ' . $title;
    $content = new stdClass();
    $content->name = ucwords($title);
    $content->meta_keyword = $keyword;
    $content->meta_description = ucfirst($description);
    return $content;
}

/* sanitize funtion */

function sanitize($value) {

    $prohibited_chars = array(" ", "/", "$", "&", "'", '%', '"', "@");

    foreach ($prohibited_chars as $k => $v):

        $value = str_replace($v, '-', $value);

    endforeach;

    return strtolower($value);
}

function get_object_from_table($tablename, $id, $type = 'object') {
    $query = new query($tablename);
    $query->Where = "where $id";
//$query->print=1;
    return $query->DisplayOne($type);
}

function pr($e, $show_count = FALSE) {
    if ($show_count === TRUE && (is_array($e) || is_object($e))) {
        echo 'Count: ' . count($e) . '<hr />';
    }
    echo '<pre>';
    print_r($e);
    echo '</pre>';
}

function generateHTML($value = '', $meta_id = 0, $type = 'text', $default_value = '', $existing_value = '') {

    switch ($type) {
        case'text':
            break;
        case'textarea':
            ?>
            <div class="form-group">
                <textarea rows="3" name="<?php echo $meta_id; ?>" class="form-control"></textarea>
            </div>
            <?php
            break;
        case'radio':
            if ($existing_value != '') {
                $default_value = $existing_value;
            }
            ?>
            <div class="radio">
                <label>
                    <input type="radio" name="<?php echo $meta_id; ?>" value="<?php echo $value; ?>" <?php echo $value == $default_value ? 'checked' : ''; ?>> <?php echo $value; ?>
                </label>
            </div>
            <?php
            break;
        case'select':
            ?>
            <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
            <?php
            break;
        case'checkbox':
            ?>
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="<?php echo $meta_id; ?>[]" value="<?php echo $value; ?>" <?php echo in_array($value, $existing_value) ? 'checked' : ''; ?>> <?php echo $value; ?>
                </label>
            </div>
            <?php
            break;
        default:
            break;
    }
}

function calculateAge($dob) {
    $birthDate = explode("-", $dob);
    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
    return $age;
}

function getLocation($zip) {
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($zip) . "&sensor=false";
    $result_string = file_get_contents($url);
    $data = json_decode($result_string, true);
    return $data['results'][0]['formatted_address'];
}

function getLnt($zip) {
    $url = "http://maps.googleapis.com/maps/api/geocode/json?address=" . urlencode($zip) . "&sensor=false";
    $result_string = file_get_contents($url);
    $result = json_decode($result_string, true);
    $result1[] = $result['results'][0];
    $result2[] = $result1[0]['geometry'];
    $result3[] = $result2[0]['location'];
    return $result3[0];
}

function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Mi') {
    $theta = $longitude1 - $longitude2;
    $distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
    $distance = acos($distance);
    $distance = rad2deg($distance);
    $distance = $distance * 60 * 1.1515;
    switch ($unit) {
        case 'Mi': break;
        case 'Km' : $distance = $distance * 1.609344;
    }
    return (round($distance, 2));
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function pdf2string($sourcefile) {
    $fp = fopen($sourcefile, 'rb');
    $content = fread($fp, filesize($sourcefile));
    fclose($fp);

    $searchstart = 'stream';
    $searchend = 'endstream';
    $pdfText = '';
    $pos = 0;
    $pos2 = 0;
    $startpos = 0;

    while ($pos !== false && $pos2 !== false) {
        $pos = strpos($content, $searchstart, $startpos);
        $pos2 = strpos($content, $searchend, $startpos + 1);

        if ($pos !== false && $pos2 !== false) {

            if ($content[$pos] == 0x0d && $content[$pos + 1] == 0x0a) {
                $pos += 2;
            } else if ($content[$pos] == 0x0a) {
                $pos++;
            }

            if ($content[$pos2 - 2] == 0x0d && $content[$pos2 - 1] == 0x0a) {
                $pos2 -= 2;
            } else if ($content[$pos2 - 1] == 0x0a) {
                $pos2--;
            }
            $textsection = substr(
                    $content, $pos + strlen($searchstart) + 2, $pos2 - $pos - strlen($searchstart) - 1
            );
            $data = @gzuncompress($textsection);
            $pdfText .= pdfExtractText($data);
            $startpos = $pos2 + strlen($searchend) - 1;
        }
    }

    return preg_replace('/(\s)+/', ' ', $pdfText);
}

function pdfExtractText($psData) {

    if (!is_string($psData)) {
        return '';
    }

    $text = '';

    // Handle brackets in the text stream that could be mistaken for 
    // the end of a text field. I'm sure you can do this as part of the 
    // regular expression, but my skills aren't good enough yet. 
    $psData = str_replace('\)', '##ENDBRACKET##', $psData);
    $psData = str_replace('\]', '##ENDSBRACKET##', $psData);

    preg_match_all(
            '/(T[wdcm*])[\s]*(\[([^\]]*)\]|\(([^\)]*)\))[\s]*Tj/si', $psData, $matches
    );
    for ($i = 0; $i < sizeof($matches[0]); $i++) {
        if ($matches[3][$i] != '') {
            // Run another match over the contents. 
            preg_match_all('/\(([^)]*)\)/si', $matches[3][$i], $subMatches);
            foreach ($subMatches[1] as $subMatch) {
                $text .= $subMatch;
            }
        } else if ($matches[4][$i] != '') {
            $text .= ($matches[1][$i] == 'Tc' ? ' ' : '') . $matches[4][$i];
        }
    }

    // Translate special characters and put back brackets. 
    $trans = array(
        '...' => '…',
        '\205' => '…',
        '\221' => chr(145),
        '\222' => chr(146),
        '\223' => chr(147),
        '\224' => chr(148),
        '\226' => '-',
        '\267' => '•',
        '\(' => '(',
        '\[' => '[',
        '##ENDBRACKET##' => ')',
        '##ENDSBRACKET##' => ']',
        chr(133) => '-',
        chr(141) => chr(147),
        chr(142) => chr(148),
        chr(143) => chr(145),
        chr(144) => chr(146),
    );
    $text = strtr($text, $trans);
    return $text;
}

function readDocx($filePath) {
    // Create new ZIP archive
    $zip = new ZipArchive;
    $dataFile = 'word/document.xml';
    // Open received archive file
    if (true === $zip->open($filePath)) {
        // If done, search for the data file in the archive
        if (($index = $zip->locateName($dataFile)) !== false) {
            // If found, read it to the string
            $data = $zip->getFromIndex($index);
            // Close archive file
            $zip->close();

            // Load XML from a string
            // Skip errors and warnings

            $xml = DOMDocument::loadXML($data, LIBXML_NOENT | LIBXML_XINCLUDE | LIBXML_NOERROR | LIBXML_NOWARNING);

            $contents = explode('\n', strip_tags($xml->saveXML()));
            $text = '';
            foreach ($contents as $i => $content) {
                $text .= $contents[$i];
            }
            return $text;
        }
        $zip->close();
    }
    return "";
}

function postToHost($url, $postdata) {
    $result = '';
    $data = "";
    $boundary = "---------------------" . substr(md5(rand(0, 32000)), 0, 10);
    // parse the given URL
    $url = parse_url($url);

    // extract host and path:
    $host = $url['host'];
    $path = $url['path'];

    // open a socket connection on port 443 - timeout: 300 sec
    $fp = fsockopen("ssl://" . $host, 443, $errno, $errstr, 300);
    if ($fp) {
        fputs($fp, "POST $path HTTP/1.0\n");
        fputs($fp, "Host: $host\n");
        fputs($fp, "Content-type: multipart/form-data; boundary=" . $boundary . "\n");

        // Ab dieser Stelle sammeln wir erstmal alle Daten in einem String
        // Sammeln der POST Daten
        foreach ($postdata as $key => $val) {
            $data .= "--$boundary\n";
            $data .= "Content-Disposition: form-data; name=\"" . $key . "\"\n\n" . $val . "\n";
        }
        $data .= "--$boundary\n";

        // Senden aller Informationen
        fputs($fp, "Content-length: " . strlen($data) . "\n\n");
        fputs($fp, $data);

        // Auslesen der Antwort
        while (!feof($fp)) {
            $result .= fread($fp, 1);
        }
        // close the socket connection:
        fclose($fp);
    } else {
        return array(
            'status' => 'err',
            'error' => "$errstr ($errno)"
        );
    }


    // split the result header from the content
    $result = explode("\r\n\r\n", $result, 2);

    $header = isset($result[0]) ? $result[0] : '';
    $content = isset($result[1]) ? $result[1] : '';

    return json_decode($content);
}

function handlePostByUnplag($filename) {
    $apiKey = "ygiSzY8xg3ilWPRk";
    $apiSecret = "DF85UGtWAqGpYvHjrgtjo9S6jIOv4AmT";
    $last = substr($filename, -4);
    if ($last == 'docx' || $last == '.doc'):
        $fileContent = readDocx($filename);
    elseif ($last == '.pdf'):
        $fileContent = pdf2string($filename);
    else:
        return "";
    endif;


    $json = json_encode(array(
        'ext' => 'txt',
        'f_content' => base64_encode($fileContent),
        'ClientID' => $apiKey,
    ));

    $post_data = array(
        'sign' => md5($json . $apiSecret),
        'json' => $json
    );

    $result = postToHost('https://unplag.com/api/UploadFile', $post_data);
    $error = 0;
    $error_msg = '';
    if (!empty($result) && isset($result->result)):

        if (!empty($result->errors)):
            $error = 1;
            $error_msg = implode("</br>", $result->errors);
        endif;

        if ($error == '0'):
            $file_id = $result->file_id;
            $json = json_encode(array(
                'type' => 'my_library',
                'file_id' => $file_id,
                'ClientID' => $apiKey,
            ));
            $post_data = array(
                'sign' => md5($json . $apiSecret),
                'json' => $json
            );
            $resultNew = postToHost('https://unplag.com/api/Check', $post_data);

            if (isset($resultNew) && ($resultNew->result == 1)):

                if (!empty($resultNew->errors)):
                    $error = 1;
                    $error_msg = implode("</br>", $resultNew->errors);
                endif;

                if ($error == '0'):
                    $resultNew = (array) $resultNew;
                    $checkid = '0';
                    foreach ($resultNew as $kk => $vv):
                        if ($kk == '0'):
                            $checkid = $vv->check_id;
                        endif;
                    endforeach;

                    $json = json_encode(array(
                        'check_id' => $checkid,
                        'ClientID' => $apiKey,
                    ));
                    $post_data = array(
                        'sign' => md5($json . $apiSecret),
                        'json' => $json
                    );

                    $result = postToHost('https://unplag.com/api/GetResults', $post_data);
                    if (!empty($result) && !empty($result->checks_results)):
                        //echo '<pre>'; print_R($result->checks_results); exit;
                        if (isset($result->checks_results[0][0]->progress)):
                            $error_msg = "<strong>Progress<strong>: " . $result->checks_results[0][0]->progress . "%<br/>";
                        endif;
                        if (isset($result->checks_results[0][0]->similarity)):
                            $error_msg.="<strong>Similarity<strong>: " . $result->checks_results[0][0]->similarity . "%<br/>";
                        else:
                            $error_msg.="<strong>Similarity<strong>: 0%<br/>";
                        endif;

                    else:
                        $result = "Unable to scan attached document";
                    endif;
                endif;
            else:
                return "Unable to scan document..!";
            endif;
        endif;
    else:
        return "Unable to scan document..!";
    endif;

    if (empty($error_msg)):
        $error_msg = 'There is no result found in scaning..!';
    endif;

    return $error_msg;
}

function toastr_message() {
    if (isset($_SESSION['admin_session_secure']['toastr'])) {
        if (isset($_SESSION['admin_session_secure']['toastr_error'])) {
            ?>
            <script>
                toastr.error('<?php echo $_SESSION['admin_session_secure']['toastr'] ?>');
            </script>
            <?php
            unset($_SESSION['admin_session_secure']['toastr_error']);
        } else {
            ?>
            <script>
                toastr.success('<?php echo $_SESSION['admin_session_secure']['toastr'] ?>');
            </script>
            <?php
        }
        unset($_SESSION['admin_session_secure']['toastr']);
    }
}

$year = date('Y');
?>