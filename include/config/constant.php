<?php

# WEBSITE CONSTANTS 
if (!defined("SITE_NAME")) {
    define('SITE_NAME', "Unity", true);
}
define('SITE_EMAIL', "support@unitymla.com");
define('NO_DATA', "No Data Found!", true);
define('API_USER', 'rina-facilitator_api1.cwebconsultants.com');
define('API_PASS', 'TF4RWLBLYUD5YU57');
define('API_SIG', 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AaCGfM2.XWV3k8gjzrZGgRpH-veS');
define('APP_ID', 'APP-80W284485P519543T');
define('ADMIN_EMAIL', 'rina-facilitator@cwebconsultants.com');
//define('PP_CONFIG_PATH', 'paypal_sdk_config.ini');
# PHP Validation types
define('VALIDATE_REQUIRED', "req", true);
define('VALIDATE_EMAIL', "email", true);
define("VALIDATE_MAX_LENGTH", "maxlength");
define("VALIDATE_MIN_LENGTH", "minlength");
define("VALIDATE_NUMERIC", "num");
define("VALIDATE_ALPHA", "alpha");
define("VALIDATE_ALPHANUM", "alphanum");

define("TEMPLATE", "default");
define("CURRENCY_SYMBOL", "$");

define("ADMIN_FOLDER", '');

define("ADD_ATTRIBUTE_PRICE_TO_PRODUCT_PRICE", true);
define("ATTRIBUTE_PRICE_OVERLAP", false);

define('VERIFY_EMAIL_ON_REGISTER', true);
define('ERROR_EMAIL', 'rocky.developer004@gmail.com', true);

$conf_shipping_type = array('quantity', 'subtotal');

define('SHIPPING_TYPE', 'Quantity');

$AllowedImageTypes = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$AllowedFileTypes = array('application/vnd.ms-excel');

# new allowed photo mime type array.
$conf_allowed_file_mime_type = array('application/vnd.ms-excel', 'application/msexcel', 'application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'applications/vnd.pdf', 'application/pdf');
$conf_allowed_photo_mime_type = array('image/gif', 'image/jpeg', 'image/pjpeg', 'image/jpg', 'image/png');
$conf_allowed_audio_mime_type = array('audio/mpeg', 'audio/mpeg', 'audio/mpg', 'audio/x-mpeg', 'audio/mp3', 'application/force-download', 'application/octet-stream');
$conf_allowed_video_mime_type = array('video/mp4', 'video/mpeg', 'video/x-msvideo', 'video/quicktime', 'ideo/x-flv', 'video/x-ms-wmv');
$conf_order_status = array('Received', 'Processing', 'Shipped', 'Delivered');

define("DOC_TYPE", '<!DOCTYPE html>', true);
define("DOC_LANGUAGE", "en", true);
define("DOC_CHAR_SET", 'utf-8', true);

/* set soft delete status */
define("SOFT_DELETE", 1, true);


/* Younifund constants */

/* Decide the file logo according to extension */
$file_extension = array(
    'pdf' => 'pdf.jpg',
    'doc' => 'word.png',
    'docx' => 'word.png',
    'txt' => 'file.png',
);



/* Used for admin for naming of project type */

$project_type_name = array(
    'approved' => 'Approved & Running',
    'new' => 'Waiting for Approval',
    'rejected' => 'Rejected',
    'expired' => 'Incomplete Expired',
    'successful' => 'Complete & Successful',
    'suspended' => 'Suspended'
);



$conf_resizedCriteria = array(
    'slider' => array(
        'thumb' => array('width' => '213', 'height' => '151'),
        'medium' => array('width' => '1256', 'height' => '350') /* home page slider */
    ),
    'blog' => array(
        'thumb' => array('width' => '213', 'height' => '151'),
        'medium' => array('width' => '311', 'height' => '233') /* blog */
    ),
    'user_card' => array(
        'thumb' => array('width' => '166', 'height' => '123'),
        'medium' => array('width' => '276', 'height' => '161')
    ),
    'user' => array(
        'small' => array('width' => '64', 'height' => '64'), /* funder */
        'smaller' => array('width' => '42', 'height' => '42'), /* campaign buzz comment */
        'smallest' => array('width' => '40', 'height' => '35'), /* top account menu  */
        'thumb' => array('width' => '110', 'height' => '105'), /* top account menu inner */
        'medium' => array('width' => '210', 'height' => '230'), /* my profile window */
        'big' => array('width' => '220', 'height' => '191') /* my profile page */
    ),
    'project' => array(
        'thumb' => array('width' => '166', 'height' => '123'),
        'small' => array('width' => '70', 'height' => '70'), /* extra stuff small */
        'medium' => array('width' => '293', 'height' => '246'), /* campaign listing box */
        'medium_slider' => array('width' => '370', 'height' => '250'), /* Extra stuff */
        'big' => array('width' => '645', 'height' => '403'), /* campaign detail page */
        'bigger' => array('width' => '665', 'height' => '242'), /* previous project */
    ),
);

$conf_resizedCriteria_resizing = array(
    'project' => array(
        'thumb' => array('width' => '166', 'height' => '123'),
        'small' => array('width' => '70', 'height' => '70'), /* extra stuff small */
        'medium' => array('width' => '293', 'height' => '252'), /* campaign listing box */
        'medium_slider' => array('width' => '370', 'height' => '250'), /* Extra stuff */
        'big' => array('width' => '645', 'height' => '403'), /* campaign detail page */
        'bigger' => array('width' => '665', 'height' => '242'), /* previous project */
    ),
);

$color_code_for_3 = array(
    '123' => array('1' => '2', '2' => '3', '3' => '1'),
    '231' => array('1' => '3', '2' => '1', '3' => '2'),
    '312' => array('1' => '1', '2' => '2', '3' => '3')
);

$color_code_for_2 = array(
    '12' => array('1' => '2', '2' => '3'),
    '23' => array('1' => '3', '2' => '1'),
    '31' => array('1' => '1', '2' => '2'),
);

define('START_PROJECT_EMAIL', true);
define('WAITING_PROJECT_EMAIL', true);