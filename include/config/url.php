<?php

# you will find the urls to main sections of the website here.
# Website URLs.
define("DIR_WS_SITE_ASSETS", DIR_WS_SITE . "assets/", true);
define("DIR_WS_SITE_IMAGE", DIR_WS_SITE_ASSETS . "images/", true);
define("DIR_WS_SITE_FILE", DIR_WS_SITE_ASSETS . "files/", true);
define("DIR_WS_SITE_CSS", DIR_WS_SITE_ASSETS . "css/", true);
define("DIR_WS_SITE_PLUGINS", DIR_WS_SITE_ASSETS . "plugins/", true);
define("DIR_WS_SITE_JS", DIR_WS_SITE_ASSETS . "js/", true);
define("DIR_WS_SITE_GRAPHIC", DIR_WS_SITE . "graphic/", true);
define("DIR_WS_SITE_GRAPHIC_SUBPAGE_GRAPHIC", DIR_WS_SITE . "graphic/subpage_graphics/", true);
define("DIR_WS_SITE_UPLOAD", DIR_WS_SITE . "upload/", true);
define("DIR_WS_SITE_INCLUDE", DIR_WS_SITE . "include/", true);
define("DIR_WS_SITE_CONTROL", DIR_WS_SITE . "", true);


define("DIR_WS_SITE_CONTROL_INCLUDE", DIR_WS_SITE_CONTROL . "tmp/", true);
define("DIR_WS_SITE_CONTROL_PHP", DIR_WS_SITE_CONTROL . "script/", true);
define("DIR_WS_SITE_CONTROL_FORM", DIR_WS_SITE_CONTROL . "form/", true);


# Graphic URLs.
define("DIR_WS_SITE_GRAPHIC_ICON", DIR_WS_SITE_GRAPHIC . "icon/", true);
define("DIR_WS_SITE_GRAPHIC_TITLE", DIR_WS_SITE_GRAPHIC . "title/", true);
define("DIR_WS_SITE_GRAPHIC_BUTTON", DIR_WS_SITE_GRAPHIC . "button/", true);
define("DIR_WS_SITE_GRAPHIC_FLASH", DIR_WS_SITE_GRAPHIC . "flash/", true);
define("DIR_WS_SITE_GRAPHIC_IMAGE", DIR_WS_SITE_GRAPHIC . "image/", true);
define("DIR_WS_SITE_GRAPHIC_EDGE", DIR_WS_SITE_GRAPHIC . "edge/", true);

#Upload URLs  |Use these paths to upload files| 
define("DIR_WS_SITE_UPLOAD_PHOTO", DIR_WS_SITE_UPLOAD . "photo/", true);
define("DIR_WS_SITE_UPLOAD_VIDEO", DIR_WS_SITE_UPLOAD . "video/", true);
define("DIR_WS_SITE_UPLOAD_AUDIO", DIR_WS_SITE_UPLOAD . "audio/", true);


#Include URLs
define("DIR_WS_SITE_INCLUDE_CLASS", DIR_WS_SITE_INCLUDE . "class/", true);
define("DIR_WS_SITE_INCLUDE_CONFIG", DIR_WS_SITE_INCLUDE . "config/", true);
define("DIR_WS_SITE_INCLUDE_EMAIL", DIR_WS_SITE_INCLUDE . "email/", true);
define("DIR_WS_SITE_INCLUDE_EXCEL", DIR_WS_SITE_INCLUDE . "excel/", true);
define("DIR_WS_SITE_INCLUDE_FUNCTION", DIR_WS_SITE_INCLUDE . "function/", true);
define("DIR_WS_SITE_INCLUDE_NEWSLETTER", DIR_WS_SITE_INCLUDE . "newsletter/", true);
define("DIR_WS_SITE_INCLUDE_TEMPLATE", DIR_WS_SITE_INCLUDE . "template/", true);
define("DIR_WS_SITE_INCLUDE_TEST_TOOL", DIR_WS_SITE_INCLUDE . "test_tool/", true);


# File System URLs
define("DIR_FS_SITE_ASSETS", DIR_FS_SITE . "assets/", true);
define("DIR_FS_SITE_GRAPHIC", DIR_FS_SITE . "graphic/", true);
define("DIR_FS_SITE_UPLOAD", DIR_FS_SITE . "upload/", true);
define("DIR_FS_SITE_INCLUDE", DIR_FS_SITE . "include/", true);

define("DIR_FS_SITE_CONTROL", DIR_FS_SITE . "/", true);


define("DIR_FS_SITE_TEMPLATE_INC", DIR_FS_SITE . "include/", true);
define("DIR_FS_SITE_TEMPLATE_INNER", DIR_FS_SITE . "include_inner/", true);

define("DIR_FS_SITE_CONTROL_INCLUDE", DIR_FS_SITE_CONTROL . "include/", true);
define("DIR_FS_SITE_CONTROL_IMAGE", DIR_FS_SITE_CONTROL . "image/", true);
define("DIR_FS_SITE_CONTROL_PHP", DIR_FS_SITE_CONTROL . "script/", true);
define("DIR_FS_SITE_CONTROL_FORM", DIR_FS_SITE_CONTROL . "form/", true);
define("DIR_FS_SITE_CONTROL_CSS", DIR_FS_SITE_CONTROL . "css/", true);

# Graphic URLs.
define("DIR_FS_SITE_GRAPHIC_ICON", DIR_FS_SITE_GRAPHIC . "icon/", true);
define("DIR_FS_SITE_GRAPHIC_TITLE", DIR_FS_SITE_GRAPHIC . "title/", true);
define("DIR_FS_SITE_GRAPHIC_BUTTON", DIR_FS_SITE_GRAPHIC . "button/", true);
define("DIR_FS_SITE_GRAPHIC_FLASH", DIR_FS_SITE_GRAPHIC . "flash/", true);
define("DIR_FS_SITE_GRAPHIC_IMAGE", DIR_FS_SITE_GRAPHIC . "image/", true);
define("DIR_FS_SITE_GRAPHIC_EDGE", DIR_FS_SITE_GRAPHIC . "edge/", true);

#Upload URLs
define("DIR_FS_SITE_UPLOAD_PHOTO", DIR_FS_SITE_UPLOAD . "photo/", true);
define("DIR_FS_SITE_UPLOAD_VIDEO", DIR_FS_SITE_UPLOAD . "video/", true);
define("DIR_FS_SITE_UPLOAD_AUDIO", DIR_FS_SITE_UPLOAD . "audio/", true);

define("DIR_FS_SITE_UPLOAD_PHOTO_CATEGORY", DIR_FS_SITE_UPLOAD_PHOTO . "category/", true);
define("DIR_FS_SITE_UPLOAD_PHOTO_PRODUCT", DIR_FS_SITE_UPLOAD_PHOTO . "product/", true);
define("DIR_FS_SITE_UPLOAD_PHOTO_BANNER", DIR_FS_SITE_UPLOAD_PHOTO . "banner/", true);

#Include URLs
define("DIR_FS_SITE_INCLUDE_CLASS", DIR_FS_SITE_INCLUDE . "class/", true);
define("DIR_FS_SITE_INCLUDE_CONFIG", DIR_FS_SITE_INCLUDE . "config/", true);
define("DIR_FS_SITE_INCLUDE_EMAIL", DIR_FS_SITE_INCLUDE . "email/", true);
define("DIR_FS_SITE_INCLUDE_EXCEL", DIR_FS_SITE_INCLUDE . "excel/", true);
define("DIR_FS_SITE_INCLUDE_FUNCTION", DIR_FS_SITE_INCLUDE . "function/", true);
define("DIR_FS_SITE_INCLUDE_PAYPAL", DIR_FS_SITE_INCLUDE . "paypal/", true);
define("DIR_FS_SITE_INCLUDE_FUNCTION_CLASS", DIR_FS_SITE_INCLUDE . "functionClass/", true);
define("DIR_FS_SITE_INCLUDE_NEFSLETTER", DIR_FS_SITE_INCLUDE . "neFSletter/", true);
define("DIR_FS_SITE_INCLUDE_TEMPLATE", DIR_FS_SITE_INCLUDE . "template/", true);
define("DIR_FS_SITE_INCLUDE_TEST_TOOL", DIR_FS_SITE_INCLUDE . "test_tool/", true);
define("DIR_FS_SITE_INCLUDE_PAYMENT_GATEWAY", DIR_FS_SITE_INCLUDE . "payment_gateway/", true);
?>