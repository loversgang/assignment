<?php

/*
 * Records the admin visits - 
 * 
 */

class adminlog {

    protected $user;
    protected $ip_address;
    protected $datetime;

    function adminlog() {
        $admin = new admin_session();
        $this->user = $admin->get_username();
        $this->ip = $_SERVER['REMOTE_ADDR'];
        $this->dt = date("y-m-d h:i:s");
    }

    function setMsg($msg = '') {
        if ($msg != '') {
            $query = new query('access_log');
            $query->Data['user'] = $this->user;
            $query->Data['ip'] = $this->ip;
            $query->Data['dt'] = $this->dt;
            $query->Data['action'] = mysql_real_escape_string($msg);
            $query->Insert();
        }
    }

    function getAllMsg($user = '') {
        if ($user != '') {
            $query = new query('access_log');
            $query->Where = "where user='" . mysql_real_escape_string($user) . "'";
            $query->DisplayAll();
        } else {
            $query = new query('access_log');
            $query->DisplayAll();
        }
    }

}

;
