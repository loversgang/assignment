<?php

class Paypal {

    var $apiUrl = "https://svcs.sandbox.paypal.com/AdaptivePayments/";
    var $paypalUrl = "https://sandbox.paypal.com/webscr?cmd=_ap-payment&paykey=";

    function __construct() {
        $this->headers = array(
            "X-PAYPAL-SECURITY-USERID: " . API_USER,
            "X-PAYPAL-SECURITY-PASSWORD: " . API_PASS,
            "X-PAYPAL-SECURITY-SIGNATURE: " . API_SIG,
            "X-PAYPAL-REQUEST-DATA-FORMAT: JSON",
            "X-PAYPAL-RESPONSE-DATA-FORMAT: JSON",
            "X-PAYPAL-APPLICATION-ID: " . APP_ID,
        );
        $this->envelope = array(
            "errorLanguage" => "en_US",
            "detailLevel" => "ReturnAll"
        );
    }

    function getPaymentOptions($paykey) {
        $packet = array(
            "requestEnvelope" => $this->envelope,
            "payKey" => $paykey
        );
        return $this->_paypalSend($packet, "GetPaymentOptions");
    }

    function setPaymentDetails() {
        
    }

    function createPayRequest() {
        
    }

    function _paypalSend($data, $call) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $call);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        $data_r = json_decode(curl_exec($ch), TRUE);
        return $data_r;
    }

    function splitPay($admin_email, $user_email, $admin_amount, $user_amount, $assign_id) {
        $createPacket = array(
            "actionType" => "PAY",
            "currencyCode" => "USD",
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => round($admin_amount),
                        "email" => $admin_email
                    ),
                    array(
                        "amount" => round($user_amount),
                        "email" => $user_email
                    ),
                )
            ),
            "returnUrl" => make_admin_url('assignment', 'view', 'view', '&tx=' . md5($assign_id) . '&id=' . $assign_id),
            "cancelUrl" => make_admin_url('assignment'),
            "requestEnvelope" => $this->envelope
        );
        $response = $this->_paypalSend($createPacket, 'Pay');
        $paykey = $response['payKey'];
        return $this->paypalUrl . $paykey;
        //$dets = $this->getPaymentOptions($paykey);
        //header("Location: " . $this->paypalUrl . $paykey);
    }

    function splitPay2($admin_email, $admin_amount, $user_amount, $assign_id) {
        $createPacket = array(
            "actionType" => "PAY",
            "currencyCode" => "USD",
            "receiverList" => array(
                "receiver" => array(
                    array(
                        "amount" => round($admin_amount),
                        "email" => $admin_email
                    )
                )
            ),
            "returnUrl" => make_admin_url('assignment', 'view', 'view', '&id=' . $assign_id . '&type=payment_success'),
            "cancelUrl" => make_admin_url('assignment'),
            "requestEnvelope" => $this->envelope
        );
        $response = $this->_paypalSend($createPacket, 'Pay');
        $paykey = $response['payKey'];
        return $this->paypalUrl . $paykey;
        //$dets = $this->getPaymentOptions($paykey);
        //header("Location: " . $this->paypalUrl . $paykey);
    }

    function detailsPacket() {
        $detailsPacket = array(
            "requestEnvelope" => $this->envelope,
            "payKey" => $paykey,
            "receiverOptions" => array(
                array(
                    "receiver" => array("email" => "balbinder@cwebconsultants.com"),
                    "invoiceData" => array(
                        "item" => array(
                            array(
                                "name" => "product_1",
                                "price" => "8.00",
                                "identifier" => "p1"
                            ),
                            array(
                                "name" => "product_1",
                                "price" => "2.00",
                                "identifier" => "p1"
                            ),
                        )
                    )
                ),
                array(
                    "receiver" => array("email" => "bharat@cwebconsultants.com"),
                    "invoiceData" => array(
                        "item" => array(
                            array(
                                "name" => "product_1",
                                "price" => "8.00",
                                "identifier" => "p1"
                            ),
                            array(
                                "name" => "product_1",
                                "price" => "4.00",
                                "identifier" => "p1"
                            ),
                        )
                    )
                ),
            ),
        );
        $this->_paypalSend($detailsPacket, 'SetPaymentOptions');
    }

}

?>