<?php

class userMessage extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('user_messages');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'from_id', 'to_id', 'subject', 'message', 'is_read', 'from_status', 'to_status', 'add_date');
    }

    function saveMessage($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);

        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['add_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getMessage($id) {
        return $this->_getObject('user_messages', $id);
    }

    function getInboxMessages() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE to_id='$user_id' ORDER BY id desc";
        return $this->ListOfAllRecords('object');
    }

    function getSentMessages() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE from_id='$user_id' ORDER BY id desc";
        return $this->ListOfAllRecords('object');
    }

}
