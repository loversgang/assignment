<?php

class job extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('job');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'assignment_id', 'select_date', 'payment_status', 'payment_amount', 'payment_method', 'work_status', 'plag_status', 'rating');
    }

    function saveJob($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['select_date'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSubject($id) {
        return $this->_getObject('job', $id);
    }

    function listJobs() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id ='$user_id' ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function all_jobs($id = 0) {
        if ($id > 0) {
            $this->Where = "WHERE `user_id` = '$id'";
        }
        return $this->ListOfAllRecords('object');
    }

    function all_submited_job() {
        $this->Field = "job.*";
        $this->Where = "LEFT JOIN `assignment` ON `assignment`.`id` = `job`.`assignment_id` WHERE `assignment`.`status` = 'Submitted' ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function getJobByAssignmentId($assignment_id) {
        $this->Where = "WHERE assignment_id='$assignment_id'";
        return $this->DisplayOne();
    }

    public static function get_object($user_id) {
        $this->Where = "WHERE `user_id` = '$user_id'";
        return $this->DisplayOne();
    }

    function update_rating($id, $rating) {
        $this->Data['rating'] = $rating;
        $this->Where = "WHERE `assignment_id` = '$id'";
        $this->UpdateCustom();
    }

    function field_exist($assignment_id) {
        $this->Where = "WHERE `assignment_id` = '$assignment_id'";
        return $this->DisplayOne();
    }

    function assignment_exist($assignment_id) {
        $this->Where = "WHERE `assignment_id` = '$assignment_id'";
        return $this->DisplayOne();
    }

    function assignment_id_user_id($assignment_id, $user_id) {
        $this->Where = "WHERE `assignment_id` = '$assignment_id' AND `user_id` = '$user_id'";
        return $this->DisplayOne();
    }

}
