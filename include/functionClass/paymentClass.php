<?php

class paymentMethod extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('payment_method');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'is_active');
    }

    function savePaymentMethod($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getPaymentMethod($id) {
        return $this->_getObject('payment_method', $id);
    }

    function ListPaymentMethods() {
        $this->Where = "WHERE is_active='1'";
        return $this->ListOfAllRecords('object');
    }

}

class payment {

    function makePayment($admin_email, $user_email, $admin_amount, $user_amount, $assign_id) {
        define("DEFAULT_SELECT", "- Select -");
        $returnUrl = make_admin_url('assignment', 'view', 'view', '&tx=' . md5($assign_id) . '&id=' . $assign_id);
        $cancelUrl = make_admin_url('assignment');
        $memo = "Adaptive Payment - chained Payment";
        $actionType = "PAY";
        $currencyCode = "USD";

        $receiverEmail = array($admin_email, $user_email);
        $receiverAmount = array($admin_amount, $user_amount);
        $primaryReceiver = array("true", "false");
        if (isset($receiverEmail)) {
            $receiver = array();
            for ($i = 0; $i < count($receiverEmail); $i++) {
                $receiver[$i] = new Receiver();
                $receiver[$i]->email = $receiverEmail[$i];
                $receiver[$i]->amount = $receiverAmount[$i];
                $receiver[$i]->primary = $primaryReceiver[$i];
            }
            $receiverList = new ReceiverList($receiver);
        }
        $payRequest = new PayRequest(new RequestEnvelope("en_US"), $actionType, $cancelUrl, $currencyCode, $receiverList, $returnUrl);
        if ($memo != "") {
            $payRequest->memo = $memo;
        }
        $service = new AdaptivePaymentsService(Configuration::getAcctAndConfig());
        try {
            $response = $service->Pay($payRequest);
            $ack = strtoupper($response->responseEnvelope->ack);
            if ($ack == "SUCCESS") {
                $payKey = $response->payKey;
                $_SESSION['pay_key'] = $payKey;
                $payPalURL = PAYPAL_REDIRECT_URL . '_ap-payment&paykey=' . $payKey;
                return $payPalURL;
            }
        } catch (Exception $ex) {
            require_once '../Common/Error.php';
            exit;
        }
    }

}
