<?php

/*
 * Abstract Class -
 * You are not adviced to make edits into this class.
 * Created By :- cWebConsultants India
 * http://www.cwebconsultants.com
 * Package :- cWebconsultants's web content management system
 *
 */

class cwebc extends query {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    function __construct($table) {
        parent::__construct($table);
        $this->TableName = $table;
        $this->orderby = 'position';
        $this->order = 'asc';
    }

}
