<?php

class institute extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'sequence') {
        parent::__construct('institutes');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'type', 'is_active', 'domain');
    }

    function saveInstitute($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getInstitute($id) {
        return $this->_getObject('institutes', $id);
    }

    function listInstitutes() {
        return $this->ListOfAllRecords('object');
    }

}
