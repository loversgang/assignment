<?php

class assignment extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('assignment');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'title', 'price', 'payment_method', 'subject_id', 'length_in_pages', 'due_date', 'time_due', 'description', 'post_date', 'status', 'ip_address', 'optional_link', 'is_active', 'file');
    }

    function saveAsignment($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['post_date'] = time();
            $this->Data['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getAssignment($id) {
        return $this->_getObject('assignment', $id);
    }

    function listAssignments() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id ='$user_id' AND `payment_status` = '1' ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function listAssignments_not_submitted() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id ='$user_id' AND `payment_status` = '1' ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function allAssignments() {
        $this->Where = "WHERE `payment_status` = 1";
        return $this->ListOfAllRecords('object');
    }

    function listactiveAssignments() {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id !='$user_id' AND status='active' AND is_active=1 AND `payment_status` = 1 ORDER BY `id` DESC";
        return $this->ListOfAllRecords('object');
    }

    function listactiveAssignmentsBySubject($subject_id) {
        $user_id = $_SESSION['admin_session_secure']['user_id'];
        $this->Where = "WHERE user_id !='$user_id' AND status='active' AND subject_id='$subject_id' AND is_active=1 AND `payment_status` = 1";
        return $this->ListOfAllRecords('object');
    }

    function update_payment_status($id, $payment_status) {
        $this->Data['id'] = $id;
        $this->Data['payment_status'] = $payment_status;
        $this->Update();
    }

    function assignment_using_field($field, $value) {
        $this->Where = "WHERE `$field` = '$value'";
        return $this->ListOfAllRecords('object');
    }

    function update_field($field, $value, $id) {
        $this->Data['id'] = $id;
        $this->Data[$field] = $value;
        $this->Update();
    }

    function papers($user_id, $approval) {
        $this->Where = "WHERE `user_id` = '$user_id' AND (`is_task_approved` = '$approval')";
        return $this->ListOfAllRecords('object');
    }

    function field_exist($field, $value) {
        $this->Where = "WHERE `$field` = '$value' AND `payment_status` = 0";
        return $this->DisplayOne();
    }

    function delete_assignment($id) {
        $this->id = $id;
        $this->Delete();
    }

}

class subjects extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('subjects');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'title', 'description', 'is_active');
    }

    function saveSubject($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getSubject($id) {
        return $this->_getObject('subjects', $id);
    }

    function listSubjects() {
        $this->Where = "WHERE is_active='1'";
        return $this->ListOfAllRecords('object');
    }

}

class files extends cwebc {

    protected $orderby;
    protected $parent_id;
    protected $order;
    protected $requiredVars;

    /* */

    function __construct($order = 'asc', $orderby = 'id') {
        parent::__construct('files');
        $this->orderby = $orderby;
        $this->order = $order;
        $this->requiredVars = array('id', 'user_id', 'assignment_id', 'file', 'date_add');
    }

    function saveFile($POST) {
        $this->Data = $this->_makeData($POST, $this->requiredVars);
        if (isset($this->Data['id']) && $this->Data['id'] != '') {
            if ($this->Update()) {
                return $this->Data['id'];
            }
        } else {
            $this->Data['date_add'] = time();
            $this->Insert();
            return $this->GetMaxId();
        }
    }

    function getFile($id) {
        return $this->_getObject('files', $id);
    }

    function listFiles() {
        return $this->ListOfAllRecords('object');
    }

    function getFileByAssignmentId($assignment_id) {
        $this->Where = "WHERE assignment_id='$assignment_id'";
        return $this->DisplayOne();
    }

    function getFileByAssignmentIdAndUserID($assignment_id, $user_id) {
        $this->Where = "WHERE assignment_id='$assignment_id' AND user_id='$user_id'";
        return $this->DisplayOne();
    }

}
