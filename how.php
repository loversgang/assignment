<?php
require_once("include/config/config.php");
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
?>
<div style="margin-top: 10px">
    <div class="col-md-12" style="background: #000">
        <h2 style="color:#fff;text-align: center">How it works</h2>
    </div>
    <div class="col-md-12" style="background-color: #C7E1E6;text-align: center">
        <center>
            <div class="col-md-3">
                <img style="height: 230px" src="<?php echo DIR_WS_SITE_IMAGE ?>pay_someone.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 110px" src="<?php echo DIR_WS_SITE_IMAGE ?>isequalto.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>post.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 65px" src="<?php echo DIR_WS_SITE_IMAGE ?>next.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img style="margin-top: 12px" src="<?php echo DIR_WS_SITE_IMAGE ?>wait.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 65px" src="<?php echo DIR_WS_SITE_IMAGE ?>next.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img style="margin-top: 15px" src="<?php echo DIR_WS_SITE_IMAGE ?>receive.png" class="img img-responsive"/>
            </div>
            <div class="col-md-12">
                <img style="margin: 15px 0px;width: 100%" src="<?php echo DIR_WS_SITE_IMAGE ?>dashes.png" class="img img-responsive"/>
            </div>
            <div class="col-md-3">
                <img style="height: 230px" src="<?php echo DIR_WS_SITE_IMAGE ?>get_paid.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 110px" src="<?php echo DIR_WS_SITE_IMAGE ?>isequalto.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>browse.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 65px" src="<?php echo DIR_WS_SITE_IMAGE ?>next.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>write.png" class="img img-responsive"/>
            </div>
            <div class="col-md-1">
                <img style="margin-top: 65px" src="<?php echo DIR_WS_SITE_IMAGE ?>next.png" class="img img-responsive hidden-xs"/>
            </div>
            <div class="col-md-2">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>dollars.png" class="img img-responsive"/>
            </div>
            <div class="col-md-12">
                <img style="margin: 15px 0px;width: 100%" src="<?php echo DIR_WS_SITE_IMAGE ?>dashes.png" class="img img-responsive"/>
            </div>
            <div class="col-md-6">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>p_methods.png" class="img img-responsive"/>
            </div>
            <div class="col-md-6">
                <img src="<?php echo DIR_WS_SITE_IMAGE ?>p2_methods.png" class="img img-responsive"/>
            </div>
        </center>
    </div>
    <?php
    require 'tmp/footer.php';
    