<?php
require_once("include/config/config.php");
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
?>
<div class="line"></div>
<div class="container">
    <div class="pat">
        <div class="f1_text">
            <p>About Unity</p>
        </div>
        <div class="f1">
            <p>Unity is a service that allows college students to anonymously connect as an academic community. This connection is made through an exchange of services that varies from a wide range of topics and subjects. Students across the nation can finally have a platform to advance as one on an educational level. Unity’s service helps alleviate the stress inflicted upon the lives of college students from educational institutions.
            </p>
        </div>  
        <div class="row">
            <div class="col-md-5">
                <div class="left_b">
                    <h5>Post an Assignment</h5>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">Anonymous.</p>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">Reliable </p>
                    <ul class="who">
                        <li>Users who are rated below 3 stars more than three times are blocked.</li>
                        <li>All completed assignments are processed through a plagiarism checker.</li>
                    </ul>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">FREE. Just make an account!</p>
                </div>
            </div>
            <div class="col-md-2">
                <p class="or_1">OR</p>
            </div>
            <div class="col-md-5">
                <div class="left_b">
                    <h5>Seek an Assignment</h5>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">Get paid within minutes!</p>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">Safe </p>
                    <ul class="who">
                        <li>We hold the funds of the assignment to ensure proper payment.​</li>
                    </ul>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">You keep 85% of all profits.</p>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">Work when you want.</p>
                    <p class="m1"><i class="fa fa-star"></i></p>
                    <p class="m2">FREE. Just make an account!</p>
                </div>
            </div>
            <div class="f1">
                <p>
                    We cut out the frantic web searching and the lonely sensation that students experience by connecting all college students for the sole purpose of education online in a safe and quick manner. Excessive amounts are spent on education each year; it is logical to get the direct learning you desire for the price you want.
                </p>
            </div>
        </div>
        <div class="f2_text">
            <p>How It Works</p>
        </div>
        <br /></br></br></br>
        <div class="row mh" style="margin-top: -40px;">
            <div class="col-md-3">
                <p class="pa">Post an Assignment</p>
            </div> 
            <div class="col-md-1">
                <div class="fl"></div>
                <div class="fl"></div>
            </div>
            <div class="col-md-2">
                <div class="post_dc">
                    <i class="fa fa-comment"></i>
                    <p class="pt">Post</p>
                </div>
                <p class="pda"> Post a description of the assignment through Unity with your price and due date.</p>
            </div>
            <div class="col-md-1">
                <i class="fa fa-long-arrow-right"></i>
            </div>
            <div class="col-md-2">
                <div class="post_dc">
                    <i class="fa fa-clock-o"></i>
                    <p class="pt">Wait</p>
                </div>
                <p class="pda"> Your post will be viewed by college students across the nation and completed once accepted.</p>
            </div>
            <div class="col-md-1">
                <i class="fa fa-long-arrow-right"></i>
            </div>
            <div class="col-md-2">
                <div class="post_dc1">
                    <i class="fa fa-file"></i>
                    <p class="pt">Receive</p>
                </div>
                <p class="pda"> Receive the completed work through the Unity website!</p>
            </div>
        </div>
        <div class="f3_text">
            <p></p>
        </div>
        </br></br></br></br>
        <div class="row mh"  style="margin-top: -30px;">
            <div class="col-md-3">
                <p class="pa">Seek an Assignment</p>
            </div> 
            <div class="col-md-1">
                <div class="fl"></div>
                <div class="fl"></div>
            </div>
            <div class="col-md-2">
                <div class="post_dc">
                    <div class="post_dc2">
                        <span class="lap"><i class="fa fa-laptop"></i></span>
                        <span class="mob">><i class="fa fa-mobile"></i></span>
                    </div>
                    <p class="pt">Browse</p>
                </div>
                <p class="pda"> Browse through a variety of assignments and choose one that you want to complete.</p>
                <br />
            </div>

            <div class="col-md-1">
                <i class="fa fa-long-arrow-right"></i>
            </div>

            <div class="col-md-2">
                <div class="post_dc">
                    <i class="fa fa-file"></i>
                    <p class="pt">Write</p>
                </div>
                <p class="pda"> Complete and upload the assignment by the deadline provided.</p>

            </div>

            <div class="col-md-1">
                <i class="fa fa-long-arrow-right"></i>
            </div>

            <div class="col-md-2">
                <div class="post_dc1">
                    <i class="fa fa-usd" style="padding:21px 60px;font-size: 43px;"></i>
                    <p class="pt">Get Paid</p>
                </div>
                <p class="pda"> Receive payment through Venmo!</p>

            </div>

        </div>
        <div class="f2_text">
            <p>Eligible Payment Methods</p>
        </div>  
        <div class="row eligible-row">
            <div class="col-md-5">
                <div class="row" style="margin-left:90px">
                    <br />
                    <p style="text-align: center; font-size: 37px;" class="font_0"><span style="font-family:palatino linotype,serif;"><span style="font-size:37px;">Pay With:</span></span></p>
                    <br />
                    <br />
                    <div class="col-md-6">
                        <img src="<?php echo DIR_WS_SITE_ASSETS . 'images/payment/1.png' ?>" class="img img-responsive" style="margin-left: -32px;" />
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo DIR_WS_SITE_ASSETS . 'images/payment/2.png' ?>" class="img img-responsive" style="margin-top: -13px;margin-left: 30px;" />
                    </div>
                    <br />
                    <div class="col-md-6">
                        <img src="<?php echo DIR_WS_SITE_ASSETS . 'images/payment/3.png' ?>" class="img img-responsive" style="margin-top:30px" />
                    </div>
                    <div class="col-md-6">
                        <img src="<?php echo DIR_WS_SITE_ASSETS . 'images/payment/4.jpg' ?>" class="img img-responsive" style="margin-top: 40px;margin-left: 46px;" />
                    </div>
                </div>   
            </div>
            <div class="col-md-2">
                <p class="or payment-line"></p>
            </div>
            <div style="margin-left: 200px;">
                <div class="col-md-5">
                    <br />
                    <p style="text-align: center; font-size: 37px;" class="font_0"><span style="font-family:palatino linotype,serif;"><span style="font-size:37px;">Receive Payment<br /> Via:</span></span></p>
                    <img src="<?php echo DIR_WS_SITE_ASSETS . 'images/venmo.png' ?>" class="img img-responsive" style="margin: 60px 50px;" />
                </div>
            </div>
        </div>
        <br />
        <span class="footer-copyright"><div style="margin-top: -10px;; margin-left: 10px;">© 2016 Unity MLA, LLC. All rights reserved.</div></span>
    </div>  
</div>
<!--<div class="bt"></div>-->  
<?php
require 'tmp/footer.php';
?>