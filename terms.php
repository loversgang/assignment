<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Terms And Conditions</h4>
            </div>
            <div class="modal-body">
                <div class="bluebox" style="height: 300px;overflow-y: scroll;">
                    <h2 class="licence"></h2>
                    <p>
                        YOUR USE OF THIS WEBSITE AND SYSTEM CONSTITUTES YOUR
                        AGREEMENT TO THE FOLLOWING TERMS AND CONDITIONS. IF YOU
                        DISAGREE WITH ANY OF THE FOLLOWING TERMS AND CONDITIONS,
                        PLEASE REFRAIN FROM USING OUR WEBSITE AND SERVICES.
                        THIS WEBSITE AND SYSTEM IS ONLY AVAILABLE FOR USE IF YOU ARE A
                        CURRENT STUDENT OF A HIGHER EDUCATIONAL INSTITUTE.
                        OTHERWISE, USE OF ANY PRODUCTS OR SERVICES OFFERED BY THIS
                        WEBSITE IS STRICTLY PROHIBITED.
                        BY SELECTING A SERVICE ON THIS WEBSITE OR MOBILE APPLICATION
                        YOU ACKNOWLEDGE HAVING READ, UNDERSTOOD, AND AGREED TO
                        ALL THESE TERMS AND CONDITIONS TO THEIR FULLEST EXTENT. BY
                        SELECTING A SERVICE AND / OR PAYMENT, YOU ALSO AGREE TO BE
                        LEGALLY BOUND BY THESE TERMS AND CONDITIONS, IN WHICH THE
                        AGREEMENT BETWEEN YOU AND UNITY MLA, LLC, BECOMES EFFECTIVE
                        IMMEDIATELY.
                    </p>
                    <h2 class="licence">INTERPRETATION</h2>
                    <p>
                        In this document:
                        “Website” means UNITYFORCOLLEGE.com.
                        "System" means the Unity Mobile Application
                        “Client”, “You” or “Yours” mean and refer to you and / or any other person
                        selecting the service through the website on your behalf.
                        “Company”, “We” or “Our” mean and refer to Unity MLA, LLC , a company
                        registered under the laws of the United States of America.
                        “Product” – an original assignment, essay, paper, and / or other written product,
                        which is prepared and delivered to the client in accordance with the selection of
                        service to not be distributed in any possible way for academic credit or for the aid
                        in obtaining a degree at a higher educational Institute.
                        “Order” refers to the option of posting an assignment and means a written
                        description on a standard electronic form, filled and submitted online by the client
                        to our website. "Order" specifies the scope of work and other requirements of the
                        customer regarding the product.
                    </p>
                    <h2 class="licence">OUR SERVICES</h2>
                    <p>
                        By posting or seeking an assignment and / or payment, you are purchasing the product or receiving a payment for the order once completed and selecting a service strictly for your personal, non-commercial, use not intended to be submitted for academic credit or advancement in obtaining a degree at a higher educational institute.All our products are drafted by current college students of accredited higher educational institutes within the United States of America, of whom transfer all rights and ownership of the products to the company. All products are nonrefundable and come with no warranties, expressed or implied. By seeking a service and / or payment, you are legally complying to complete an assignment within the given time frame of the assignment to be feasible to pass a plagiarism check software maintaining complete originality in addition to transferring all rights and ownership of the product to the company.-
                        Additional detailed information about our services can be found on our FAQ
                        page. Please note, it is your obligation to read these Terms and Conditions and
                        FAQ page, prior to selecting your service and / or payment to our website.
                    </p>
                    <h2 class="licence">COPYRIGHT & PERSONAL USE</h2>
                    <p>The products this company delivers are completely original. The full copyright
                        pertaining to products and other materials delivered is retained by the company
                        and / or its affiliates and partners.
                        The purpose of delivered products, and other materials available from this
                        website, is strictly for personal, non-commercial use and is not intended for any
                        form of achievement for academic credit or the assistance in obtaining a degree
                        from a higher educational institute. By selecting a service with our company, you
                        agree not to not display, distribute, modify, publish, reproduce, and/or transmit
                        any of the products and services offered of our company, Neither will you,
                        without prior written consent, exploit the products, services, assignments, and/or
                        create derivative works from the contents provided by this website or the
                        system.
                        By selecting a service, you also agree to defend, indemnify, and hold harmless
                        the company for any and all unauthorized use made of any material, product,
                        content, and/or service available from this website and the company’s mobile
                        application. Any unauthorized use of delivered products and / or content of this
                        website can subject our customers to civil or criminal penalties.</p>
                    <h2 class="licence">NO REFUND</h2>
                    <p>All products this company offers are non-refundable, and come with no
                        warranties, expressed or implied.
                        No refund can be granted to the client under any circumstances once the order is
                        completed, unless specifically stated herein.
                        In certain cases, the company may provide a refund at its own discretion.
                        We absolutely do not guarantee that our system or website will aid you in
                        achieving a desirable grade or academic credit in any circumstances as stated in
                        these agreed upon terms and conditions.</p>
                    <h2 class="licence">PENALTY SYSTEM</h2>
                    <p>You	agree	to	complete	the	assignment	upon	accepting	to	complete	the	assignment.	In	
                        the	situation	in	which	you	don't	complete	the	assignment,	you	will	receive	a	message	
                        outlining	the	situation	on	the	first	offense.	Cases	in	which	the	same	situation	arises	
                        more	than	once	the	client	will	be	given	a	final	offense	notice	stating	the	client	is	
                        prohibited	to	use	the	service	and	will	be	discarded	as	a	client	unless	there	is	written	
                        communication	and	written	authorization	by	Unity.	A	notice	of	appeal	to	this	penalty	
                        system	may	be	submitted	to	Unity	in	which	membership	will	be	under	Unity's	
                        discretion.	Unity	reserves	the	right	to	choose	not	to	give	penalties	on	a	situational	basis	
                        and	reserves	the	right	to	change	this	policy.</p>
                    <h2 class="licence">NO PLAGIARISM</h2>
                    <p>By agreeing to these terms and conditions, you acknowledge that the company
                        reserves the right to cancel any service request, assignment, agreement,
                        arrangement or contract with any person who condones or attempts to pass
                        plagiarized products as original.
                        You also agree that any product delivered by the company may not be disclosed
                        to any third parties, nor distributed in any way for payment or for any other
                        purpose. You also acknowledge that if the company suspects that the delivered
                        product has been distributed or used by the customer in any form of plagiarism,
                        the company reserves the right to refuse to carry out any further work and
                        services with this client.
                        It is not acceptable for customers to put their own name on the product we
                        deliver. The custom written samples are provided by Unity MLA, LLC, for
                        research purposes only and can not be used as a substitute of your own writing.
                        It can only be used as a model or work cited, from which you can learn how to
                        draft your own research properly or take inspiration for your own thinking. Entire
                        parts of the research provided by our company may be used in customer's
                        original piece of writing only if properly cited or paraphrased. Please refer to your
                        University definition of plagiarism for acceptable use of source material.
                        Our company does not condone, encourage, or knowingly take part in plagiarism
                        or any other acts of academic dishonesty or fraud at ANY TIME. We strongly
                        adhere to and abide by all copyright laws, and will not knowingly allow any client
                        to carry out plagiarism or violate copyright laws.
                        Neither the company nor any of its affiliates and / or partners shall be liable for
                        any inappropriate, illegal, unethical, or otherwise wrongful use of the products
                        and / or other written material received from our website. This includes and is not
                        limited to plagiarism, expulsion, academic probation, loss of scholarships / titles /
                        awards / prizes / grants / positions, lawsuits, poor grading, failure, suspension, or
                        any other disciplinary or legal actions outside the intended use of this system and
                        /or website expressed in these terms.
                        The clients who post, request, seek, and / or complete assignments from our
                        website are solely responsible for any and all disciplinary actions arising from the
                        improper, unethical, inappropriate and / or illegal use of material outside of the
                        intended use for this system and/or website.</p>
                    <h2 class="licence">LINKS DISCLAIMER</h2>
                    <p>Even though this website can be linked to others, we do not in any way
                        approve, endorse, certify or sponsor linked sites unless specifically stated
                        herein. The company is not the owner of, is not responsible for, and does
                        not control any content of any website linked to our website. By agreeing
                        to these terms and conditions, you acknowledge that linking to other
                        websites is at your users’ own risk.</p>
                    <h2 class="licence">PRIVACY & SECURITY</h2>
                    <p>Please refer to our Privacy Statement posted on this website for detailed
                        explanation of our company's policies and practices regarding collection, storage
                        and use of online guests’ information. For more information about the security of any personal information you submit when you register,select a service with this
                        website, and proceed with the payment, please refer to the Privacy Policy webpage</p>
                    <h2 class="licence">WARRANTIES</h2>
                    <p>By selecting a service and proceeding with payment, you acknowledge and that
                        you are in complete understanding and agreement with the statements above, as
                        well as each of the following:
                        • All products are provided entirely as assistance examples for research,
                        reference, and / or for the purposes of learning how to write a paper in a
                        proper manner and in a particular citation style (MLA, APA, Chicago,
                        Turabian, Harvard, and so forth).
                        • The proper citation is required for any information and / or ideas used from the
                        product. The act of using our written materials to attain higher marks
                        within education institutions is never allowed and the company does not
                        offer you guarantee for results.
                        • You comply and agree to the terms wherein this website acquires payment
                        which is for the work and time involved to gather, organize, correct, edit,
                        post, and deliver reference materials, in addition to the maintenance,
                        administration and advertising involved in regards to the company’s
                        system and/or website .
                        • Aside from a rational number of copies for non-commercial and/or personal
                        use, you may not display, distribute, modify, publish, reproduce, and/or
                        transmit any of our products or services. Neither will you, without prior
                        written consent, exploit the products, services, assignments, and/or create
                        derivative works from the contents provided by this website or the
                        system.
                        • All services, assignments, written work, contents, and products are acquired
                        from current college students who enter in a "Work-For-Hire" agreement
                        subjected to federal and State laws. Once the product is acquired there is
                        a transfer of all rights and ownership to the company and / or its affiliates
                        and partners.
                        • Instantly after conducted research by yourself and/ or reference use of the
                        material is complete, you comply to the agreement of destroying all
                        delivered products, assignments, written work, and contents. No copies
                        shall be made for distribution, and no parts of any product shall be used
                        without proper citation.
                        • You agree to receive emailed information for your password for your first time
                        registering as client wherein the password is required to be changed upon
                        first use of this website or the system as a registered client.
                        • You agree to receive emailed promotional information about deals and online
                        events organized by the company. You have the right to unsubscribe from
                        receiving this kind of information directly, through the e-mail marketing
                        distributor.
                        • You agree to completing the assignment if chosen to complete the assigment
                        and agree to pay the fees associated with failing to complete the
                        assignment.</p>
                    <h2 class="licence">WARRANTY DISCLAIMER</h2>
                    <p>THE COMPANY MAKES NO REPRESENTATIONS OR WARRANTIES WITH
                        REGARD TO THE WEBSITE, SYSTEM, OR ANY MATERIALS HEREIN,
                        WHETHER EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE,
                        INCLUDING, WITHOUT LIMITATION, ANY IMPLIED WARRANTY OF
                        MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT
                        OR ANY IMPLIED WARRANTY ARISING OUT OF COURSE
                        OF PERFORMANCE, COURSE OF DEALING OR USAGE OF TRADE.
                        IN ADDITION, THE COMPANY MAKES NO REPRESENTATION THAT THE
                        OPERATION OF THE WEBSITE, SYSTEM, AND ITS SERVICE WILL BE
                        UNINTERRUPTED OR ERROR-FREE. THE COMPANY SHALL NOT BE HELD
                        LIABLE FOR THE CONSEQUENCES OF ANY INTERRUPTIONS OR ERRORS
                        ON THE WEBSITE OR SYSTEM. IT IS YOUR RESPONSIBILITY TO ANALYZE
                        THE ACCURACY, COMPLETENESS, OR USEFULNESS OF ANY OPINION,
                        ADVICE, INFORMATION, OR OTHER CONTENT PROVIDED IN
                        CONNECTION WITH THE SERVICE OR OTHERWISE MADE AVAILABLE
                        THROUGH THE WEBSITE OR SYSTEM. PLEASE SEEK THE ADVICE OF
                        PROFESSIONALS, AS APPROPRIATE, REGARDING THE EVALUATION OF
                        ANY SUCH OPINION, ADVICE, INFORMATION OR OTHER CONTENT.</p>
                    <h2 class="licence">LIMITATION OF LIABILITY</h2>
                    <p>By acknowledging the above terms and conditions, you agree to release and
                        hold the company and its employees, shareholders, officers, agents,
                        representatives, directors, affiliates, promotion, subsidiaries, advertising and fulfillment agencies, any third-party providers or sources of information or data and legal advisers (the “Company’s Affiliates”) harmless from any and all losses,rights, damages, claims, and actions of any kind, arising from or related to the products, including but not limited to: (a) telephone, electronic, hardware or software, Internet, network, email, or computer malfunctions, failures or difficulties of any kind; (b) failed, garbled, incomplete or delayed computer transmissions; (c) any condition caused by events beyond the control of the company, that might cause the product to be corrupted, delayed or disrupted; (d) any injuries, damages or losses of any sort arising in connection with, or as a result of, utilizing our services in the scope of business or; (e) any printing or typographical errors in any materials associated with our services. In addition, you agree to defend, indemnify, and hold the company and company’s Affiliates harmless from any claim, suit or demand, including attorney’s fees, made by a third party due to or arising out of your utilizing of our services, your violation or breach of these Terms and Conditions, your violation of any rights of a third party, or any other act or omission by you. IN NO EVENT SHALL THE COMPANY BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, SPECIAL OR CONSEQUENTIAL DAMAGES ARISING OUT OF OR IN ANY WAY CONNECTED WITH THE USE OF THIS WEBSITE OR SYSTEM, OR ANY INFORMATION PROVIDED ON THIS WEBSITE OR SYSTEM. DUE TO SOME STATES OR JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FORCONSEQUENTIAL OR INCIDENTAL DAMAGES, THE ABOVE LIMITATION MIGHT NOT BE APPLIED TO YOU.</p>
                    <h2 class="licence">AMENDMENTS</h2>
                    <p>You agree and acknowledge the fact that the company may unilaterally change these Terms and Conditions. It is highly recommended that our customers keep track of any changes made to the above Terms and Conditions by reviewing this webpage from time to time, since we guarantee immediate reflection of any such changes on the present webpage of this website.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>