<?php
if (isset($_POST['submit'])) {
    extract($_POST);
    $to_email = SITE_EMAIL;
    $content = $name . '<br /><br />' . $message;
    send_email_by_cron($to_email, $content, $subject, $name, $email);
    $_SESSION['admin_session_secure']['toastr'] = 'Thank you! One of our representatives will get back to you within 48 hours.';
    Redirect(DIR_WS_SITE);
}
?>
<div class="modal fade" id="contact_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo SITE_EMAIL ?></h4>
            </div>
            <form method="POST">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Name</label> <span style="color:red">*</span>
                        <input type="text" class="form-control" name="name" id="name" required />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label> <span style="color:red">*</span>
                        <input type="text" class="form-control" name="email" id="email" required required />
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject</label> <span style="color:red">*</span>
                        <input type="text" class="form-control" name="subject" id="subject" />
                    </div>
                    <div class="form-group">
                        <label for="pwd">Message</label> <span style="color:red">*</span>
                        <textarea name="message" id="message" class="form-control" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>