<?php

isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
if ($user_type != 'admin') {
    Redirect(make_admin_url('home'));
}
$modName = 'user';
switch ($action):
    case'list':
        $query = new user;
        $listUsers = $query->listUsers_admin();
        break;

    case'block':
        $query = new user;
        $query->block_user($id);
        $admin_user->set_pass_msg('Action Perform Successfully');
        Redirect(make_admin_url('user'));
        break;

    case'delete':
        $query = new user;
        $query->delete_user($id);
        $admin_user->set_pass_msg('Deleted user successfully');
        Redirect(make_admin_url('user'));
        break;

    default:break;
endswitch;
