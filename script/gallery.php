<?php

include_once(DIR_FS_SITE . 'include/functionClass/userMediaClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['pid']) ? $pid = $_GET['pid'] : $pid = '0';
isset($_GET['vid']) ? $vid = $_GET['vid'] : $vid = '0';
isset($_GET['type']) ? $type = $_GET['type'] : $type = 'photo';
$modName = 'gallery';
$user_id = $_SESSION['admin_session_secure']['user_id'];
#handle actions here.
switch ($action):
    case'list':
        $obj = new userGallery;
        $photos = $obj->listGallery('photo');

        $obj = new userGallery;
        $videos = $obj->listGallery('video');

        if (isset($_POST['create_gallery'])) {
            $_POST['user_id'] = $user_id;
            $_POST['is_primary'] = isset($_POST['is_primary']) ? $_POST['is_primary'] : 0;
            $obj = new userGallery;
            $obj->saveGallery($_POST);
            $admin_user->set_pass_msg('Gallery Created Successfully!');
            Redirect(make_admin_url('gallery'));
        }
        break;
    case'insert':
        break;
    case'update':
        if ($pid > 0) {
            $obj = new userPhoto;
            $photos = $obj->getGalleryPhoto($user_id, $pid);
            $obj = new userGallery;
            $gallery = $obj->getGallery($pid);
        }
        if ($vid) {
            $obj = new userVideo;
            $videos = $obj->getGalleryVideo($user_id, $vid);
            $obj = new userGallery;
            $gallery = $obj->getGallery($vid);
        }
        if (isset($_POST['update_gallery'])) {
            $_POST['user_id'] = $user_id;
            $_POST['is_primary'] = isset($_POST['is_primary']) ? $_POST['is_primary'] : 0;
            $obj = new userGallery;
            $obj->saveGallery($_POST);
            $admin_user->set_pass_msg('Gallery Updated Successfully!');
            Redirect(make_admin_url('gallery', 'update', 'update', 'id=' . $id));
        }
        break;
    case'delete':
        break;
    default:break;
endswitch;
