<?php

include_once(DIR_FS_SITE . 'include/functionClass/assignmentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paymentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/jobClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paypalClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['tx']) ? $tx = $_GET['tx'] : $tx = '0';
$modName = 'assignment';

$_SESSION['uid'] = '1';
$_SESSION['username'] = 'bharat';
$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
$paypal_id = 'sriniv_1293527277_biz@inbox.com';

if (isset($_GET['st']) && $_GET['st'] == 'Completed') {
    $query = new assignment;
    $query->update_payment_status($id, 1);
}

if ($tx == md5($id)) {
    $arr['id'] = $id;
    $arr['status'] = 'Paid';
    $obj = new assignment;
    $obj->saveAsignment($arr);
    $admin_user->set_pass_msg('Payment Successfull!');
    Redirect(make_admin_url('assignment', 'view', 'view', 'id=' . $id));
}
switch ($action):
    case'list':
        if ($user_type == 'admin') {
            $query = new user;
            $get_detail = $query->getUser($id);
            if (!$id) {
                $query = new assignment();
                $assignments = $query->allAssignments();
            } else {
                $query = new assignment();
                $assignments = $query->assignment_using_field('user_id', $id);
            }
        } else {
            $query = new user;
            $get_detail = $query->getUser();
            $obj = new assignment;
            $assignments = $obj->listAssignments_not_submitted();
        }
        break;
    case'insert':
        $obj = new subjects;
        $subjects = $obj->listSubjects();
        $obj = new paymentMethod;
        $paymentMethods = $obj->ListPaymentMethods();
        if (isset($_POST['post_assignment'])) {
            if ($_POST['price'] >= 20) {
                $file_info = pathinfo($_FILES['file']['name']);
                $file_extension = $file_info['extension'];
                $file_name = time() . $_FILES['file']['name'];
                $tmp_name = $_FILES['file']['tmp_name'];
                $path = DIR_FS_SITE . 'assets/assignments/' . $file_name;
                $move = move_uploaded_file($tmp_name, $path);
                $_POST['user_id'] = $user_id;
                $_POST['file'] = $file_name;
                $_POST['due_date'] = $_POST['month'] . '-' . $_POST['date'] . '-' . $_POST['year'];
                $obj = new assignment;
                $assignment_id = $obj->saveAsignment($_POST);
                Redirect(make_admin_url('pay', 'list', 'list&id=' . $assignment_id));
            } else {
                $admin_user->set_pass_msg('There is a $20 minimum.');
                Redirect(make_admin_url('assignment', 'insert', 'insert'));
            }
        }
        break;
    case'delete':
        $query = new assignment;
        $query->delete_assignment($id);
        Redirect(make_admin_url('assignment'));
        break;
    case'view':
        $obj = new assignment;
        $assignment = $obj->getAssignment($id);

        $query = new files;
        $file = $query->getFileByAssignmentId($id);

        $query = new job;
        $job = $query->getJobByAssignmentId($id);

        if (isset($_POST['cancel_assignment'])) {
            $arr['id'] = $_POST['assignment_id'];
            $arr['is_active'] = 0;
            $obj = new assignment;
            $obj->saveAsignment($arr);
            $admin_user->set_pass_msg('Assignment Cancelled Successfully!');
            Redirect(make_admin_url('assignment'));
        }
        if (isset($_POST['undo_cancel_assignment'])) {
            $arr['id'] = $_POST['assignment_id'];
            $arr['is_active'] = 1;
            $obj = new assignment;
            $obj->saveAsignment($arr);
            $admin_user->set_pass_msg('Assignment Undo Successfully!');
            Redirect(make_admin_url('assignment'));
        }
        if ($assignment->status == 'Submitted') {
            $user = get_object('user', $job->user_id);
            $submitter_name = $user->username;
        }

        break;
    default:break;
endswitch;
?>