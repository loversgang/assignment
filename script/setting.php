<?php
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

$modName = 'setting';
#handle actions here.
switch ($action):
    case'list':
        if (isset($_POST['change_password'])) {
            if ($logged_user->password == md5($_POST['password'])) {
                if ($_POST['new_password'] == $_POST['confirm_new_password']) {
                    $arr['id'] = $user_id;
                    $arr['hash'] = $_POST['new_password'];
                    $arr['password'] = md5($_POST['new_password']);
                    $obj = new user;
                    $obj->saveUser($arr);
                    $obj = new user;
                    $user = $obj->getUser();
                    $subject = 'Password Changed';
                    $to_email = $user->email;
                    $from_email = SITE_EMAIL;
                    $FromName = SITE_NAME;
                    ob_start();
                    ?>
                    <div>
                        You changed your password.<br/>
                        <b>Account Details:</b><br/>
                        <b>Username:</b> <?php echo $user->username; ?><br/>
                        <b>Password:</b> <?php echo $user->hash; ?><br/>
                        <br/>
                        <br/>
                        Regards,<br/>
                        <b>Unity Inc.</b>
                    </div>
                    <?php
                    $message = ob_get_clean();
                    SendEmail($subject, $to_email, $from_email, $FromName, $message);
                    $admin_user->set_pass_msg('Password Updated Successfully and email has been sent to your email address with account details!');
                    Redirect(make_admin_url('setting'));
                } else {
                    $admin_user->set_error();
                    $admin_user->set_pass_msg('Password Not Matched!');
                    Redirect(make_admin_url('setting'));
                }
            } else {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Wrong Current Password!');
                Redirect(make_admin_url('setting'));
            }
        }
        break;
    case'insert':
        break;
    case'update':
        break;
    case'delete':
        break;
    default:break;
endswitch;
