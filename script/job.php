
<?php

include_once(DIR_FS_SITE . 'include/functionClass/assignmentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paymentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/jobClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
isset($_GET['assignment_id']) ? $assignment_id = $_GET['assignment_id'] : $assignment_id = '0';
isset($_GET['ty']) ? $ty = $_GET['ty'] : $ty = 'approved';

$modName = 'job';

switch ($action):
    case'list':
        if ($user_type == 'admin') {
            $query = new user;
            $get_detail = $query->getUser($id);
            $query = new job;
            $jobs = $query->all_submited_job();
        } else {
            $query = new user;
            $get_detail = $query->getUser();
            $obj = new job;
            $jobs = $obj->listJobs();
        }

        break;
    case'insert':
        $obj = new subjects;
        $subjects = $obj->listSubjects();
        $obj = new paymentMethod;
        $paymentMethods = $obj->ListPaymentMethods();
        if (isset($_POST['post_assignment'])) {
            $_POST['user_id'] = $user_id;
            $obj = new assignment;
            $obj->saveAsignment($_POST);
            $admin_user->set_pass_msg('Assignment Posted Successfully!');
            Redirect(make_admin_url('assignment'));
        }
        break;
    case'update':
        break;
    case'view':
        $query = new job();
        $assignment_exist = $query->assignment_exist($id);
        if ($assignment_exist) {
            $query = new job;
            $assignment_id_user_id = $query->assignment_id_user_id($id, $user_id);
            if (!$assignment_id_user_id) {
                Redirect(make_admin_url('job'));
            }
        }

        $obj = new assignment;
        $assignment = $obj->getAssignment($id);

        $query = new job;
        $job = $query->getJobByAssignmentId($id);

        if (isset($_POST['cancel_assignment'])) {
            // Update Assignment
            $arr['id'] = $_POST['assignment_id'];
            $arr['status'] = 'active';
            $obj = new assignment;
            $obj->saveAsignment($arr);

            // Delete Job
            $obj = new job;
            $job = $obj->getJobByAssignmentId($_POST['assignment_id']);
            $object = new job;
            $object->id = $job->id;
            $object->Delete();
            $admin_user->set_pass_msg('Assignment Cancelled Successfully!');
            Redirect(make_admin_url('job'));
        }
        if (isset($_POST['accept_assignment'])) {
            $query = new job;
            $assignment_id_exist = $query->field_exist();
            // save job
            if ($assignment_id_exist) {
                $admin_user->set_error();
                $admin_user->set_pass_msg('Assignment Already Accepted!');
                Redirect(make_admin_url('job'));
            } else {
                $array['user_id'] = $user_id;
                $array['assignment_id'] = $_POST['assignment_id'];
                $array['payment_amount'] = $_POST['payment_amount'];
                $array['payment_method'] = $_POST['payment_method'];
                $obj = new job;
                $obj->saveJob($array);

                // update assignment
                $arr['id'] = $_POST['assignment_id'];
                $arr['status'] = 'Being Done';
                $obj = new assignment;
                $obj->saveAsignment($arr);
                $admin_user->set_pass_msg('Assignment Accepted Successfully!');
                Redirect(make_admin_url('job'));
            }
        }
        if (isset($_POST['upload_assignment'])) {
            if (is_array($_FILES['file']) && $_FILES['file']['error'] == 0) {
                if (is_uploaded_file($_FILES['file']['tmp_name'])) {
                    $sourcePath = $_FILES['file']['tmp_name'];
                    $filename = time() . $_FILES['file']['name'];
                    $targetPath = DIR_FS_SITE_ASSETS . "files/" . $filename;
                    if (!is_dir(DIR_FS_SITE_ASSETS . 'files/')) {
                        mkdir(DIR_FS_SITE_ASSETS . 'files/', 0777, true);
                    }
                    if (move_uploaded_file($sourcePath, $targetPath)) {
                        $arr['user_id'] = $user_id;
                        $arr['assignment_id'] = $_POST['assignment_id'];
                        $arr['file'] = $filename;
                        $obj = new files;
                        $obj->saveFile($arr);

                        // Update Assignment
                        $arrr['id'] = $_POST['assignment_id'];
                        $arrr['status'] = 'Submitted';

                        $obj = new assignment;
                        $obj->saveAsignment($arrr);
                        $admin_user->set_pass_msg('Assignment Submitted Successfully!');
                        Redirect(make_admin_url('job', 'venmo', 'venmo'));
                    }
                }
            }
        }
        break;
    case 'approve':
        $query = new assignment;
        $assignment = $query->getAssignment($id);

        $query = new job;
        $job = $query->getJobByAssignmentId($id);
        $user = get_object('user', $assignment->user_id);
        $user2 = get_object('user', $job->user_id);
        $job_poster_email = $user->email;
        $job_doer = $user2->email;
        if ($ty == 'approved') {
            $doer_message = "Dear " . $user2->username . ":<br /><br />
 
The assignment uploaded to " . $user->username . " has been reviewed and confirmed by a Unity representative! Please allow up to 2 business days for your payment to be applied to your Venmo account.<br /><br />
 
 
Best Regards,<br /><br />
 
Unity MLA, LLC";
            $poster_message = "Dear " . $user->username . ",<br /><br />
 
Your assignment has been completed by " . $user2->username . " and is now available under the “Completed Papers” section of your account.<br /><br />
 
 
Enjoy!<br /><br />
 
Unity MLA, LLC";
            $query = new assignment;
            $query->update_field('is_task_approved', 1, $id);
            $admin_user->set_pass_msg('Assignment Confirm Successfully');
            $subject = 'Assignment Confirmed';
            send_email_by_cron($job_poster_email, $poster_message, $subject);
            send_email_by_cron($job_doer, $doer_message, $subject);
        } else {
            $doer_message = "Dear " . $user2->username . ",<br /><br />
 
The assignment that you have uploaded to " . $user->username . " was denied for at least one of the following reasons:<br /><br />
 
-the assignment did not have at least 70% originality;<br />
-the assignment had too many grammatical errors or;<br />
-the assignment was not about the topic requested.<br /><br />
 
The work you have submitted has been deleted and you will not receive payment due to the reasons listed above.<br /><br />
 
 
Best Regards,<br /><br />
 
Unity MLA, LLC";
            $poster_message = "Dear " . $user->username . ",<br /><br />
 
The assignment that was uploaded by " . $user2->username . " was denied for one of the following reasons:<br /><br />
 
-the assignment did not have at least 70% originality;<br />
-the assignment had too many grammatical errors or;<br />
-the assignment was not about the topic requested.<br /><br />
 
A refund will be issued to you within 48 hours.<br /><br />
 
 
Best regards,<br /><br />
 
Unity MLA, LLC";
            $query = new assignment;
            $query->update_field('is_task_approved', 2, $id);
            $admin_user->set_pass_msg('Assignment Decline Successfully');
            $subject = 'Assignment Declined';
            send_email_by_cron($job_poster_email, $poster_message, $subject);
            send_email_by_cron($job_doer, $doer_message, $subject);
        }
        Redirect(make_admin_url('job'));
        break;

    case'admin':
        $obj = new assignment;
        $assignment = $obj->getAssignment($assignment_id);

        $query = new files;
        $file = $query->getFileByAssignmentId($assignment_id);

        $query = new job;
        $job = $query->getJobByAssignmentId($assignment_id);

        break;

    case'venmo':

        $query = new user;
        $user = $query->getUser();
        if (isset($_POST['venmo_submit'])) {
            $query = new user;
            $query->update_field($user_id, 'venmo', $_POST['venmo']);
            $admin_user->set_pass_msg('Venmo Username added successfully');
            Redirect(make_admin_url('job'));
        }
        break;
    default:break;
endswitch;
