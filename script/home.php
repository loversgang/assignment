<?php

include_once(DIR_FS_SITE . 'include/functionClass/assignmentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/paymentClass.php');
include_once(DIR_FS_SITE . 'include/functionClass/jobClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';

if ($admin_user->is_logged_in()) {
    Redirect(make_admin_url('job'));
}

$modName = 'home';
#handle actions here.
switch ($action):
    case'list':
        if ($logged_user->option_type == 'post') {
            if ($user_type == 'admin') {
                $obj = new assignment;
                $assignments = $obj->allAssignments();
            } else {
                $obj = new assignment;
                $assignments = $obj->listAssignments();
            }
        } else {
            $obj = new job;
            $jobs = $obj->listJobs();
        }
        break;
    case'insert':
        break;
    case'update':
        break;
    case'delete':
        break;
    default:break;
endswitch;
