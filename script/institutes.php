<?php

include_once(DIR_FS_SITE . 'include/functionClass/instituteClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
if ($user_type != 'admin') {
    Redirect(make_admin_url('home'));
}
$modName = 'institutes';
switch ($action):
    case'list':
        $obj = new institute;
        $institutes = $obj->listInstitutes();
        if (isset($_POST['save_institute'])) {
            $obj = new institute;
            $obj->saveInstitute($_POST);
            $admin_user->set_pass_msg('Institute Added Successfully!');
            Redirect(make_admin_url('institutes'));
        }
        break;

    case'insert':
        break;
    case'update':
        $obj = new institute;
        $institutes = $obj->listInstitutes();
        $obj = new institute;
        $inst = $obj->getInstitute($id);
        if (isset($_POST['update_institute'])) {
            $obj = new institute;
            $obj->saveInstitute($_POST);
            $admin_user->set_pass_msg('Institute Updated Successfully!');
            Redirect(make_admin_url('institutes', 'update', 'update', 'id=' . $id));
        }
        break;
    case'delete':
        break;
    default:break;
endswitch;
