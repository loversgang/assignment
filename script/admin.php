<?php

include_once(DIR_FS_SITE . 'include/functionClass/metaDataClass.php');
isset($_GET['action']) ? $action = $_GET['action'] : $action = 'list';
isset($_GET['section']) ? $section = $_GET['section'] : $section = 'list';
isset($_GET['id']) ? $id = $_GET['id'] : $id = '0';
$user_id = $_SESSION['admin_session_secure']['user_id'];
$modName = 'admin';
#handle actions here.
switch ($action):
    case'list':
        break;
    case'insert':
        $obj = new metaGroup;
        $groups = $obj->ListMetaGroups();
        if (isset($_POST['create_meta'])) {
            extract($_POST);
            $_POST['is_matching'] = isset($_POST['is_matching']) ? $_POST['is_matching'] : 0;
            $_POST['is_searchable'] = isset($_POST['is_searchable']) ? $_POST['is_searchable'] : 0;
            $obj = new meta;
            $meta_id = $obj->saveMeta($_POST);
            if ($type == 'radio' || $type == 'checkbox' || $type == 'select') {
                foreach ($options as $option) {
                    $arr['meta_id'] = $meta_id;
                    $arr['value'] = $option;
                    $obj = new metaOptions;
                    $obj->saveMetaOption($arr);
                }
            }
            $admin_user->set_pass_msg('Meta Created Successfully!');
            Redirect(make_admin_url('admin', 'insert', 'insert'));
        }
        break;
    case'update':
        break;
    case'delete':
        break;
    default:break;
endswitch;
