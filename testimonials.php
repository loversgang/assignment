<?php
require_once("include/config/config.php");
$Page = isset($_GET['Page']) ? $_GET['Page'] : "home";
require 'tmp/header.php';
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-header video-style">Watch our new TV ad</div>
                <div class="panel-body" style="background-color:#A8A8A8">
                    <span class="footer-copyright">© 2016 Unity MLA, LLC. All rights reserved.</span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require 'tmp/footer.php';
