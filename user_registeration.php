<?php
if (isset($_POST['login_user'])) {
    if ($user = validate_user('user', (array) $_POST, true)) {
        if ($user->is_email_verified == 1) {
            if ($user->is_block == 1) {
                if ($user->first_time_login == 0) {
                    $_SESSION['admin_session_secure']['welcome'] = 'welcome';
                }
                $admin_user->set_admin_user_from_object($user);
                update_last_access($user->id, 1, $user->first_time_login);
                if ($user->user_type == 'user') {

                    Redirect(make_admin_url('center'));
                } else {
                    Redirect(make_admin_url('job'));
                }
            } else {
                $_SESSION['admin_session_secure']['toastr_error'] = 'error';
                $_SESSION['admin_session_secure']['toastr'] = 'Your account blocked by Admin';
                Redirect(DIR_WS_SITE);
            }
        } else {
            $_SESSION['admin_session_secure']['toastr_error'] = 'error';
            $_SESSION['admin_session_secure']['toastr'] = 'Please verify your email address';
            Redirect(DIR_WS_SITE);
        }
    } else {
        $_SESSION['admin_session_secure']['toastr_error'] = 'error';
        $_SESSION['admin_session_secure']['toastr'] = 'Username or Password is incorrect';
        Redirect(DIR_WS_SITE);
    }
}
if (isset($_COOKIE['assignment'])) {
    $user_detail = user::getUserByUsername($_COOKIE['assignment']);
    if ($user_detail) {
        if ($user = validate_user('user', (array) $user_detail, true)) {
            $user->user_type = 'user';
            $admin_user->set_admin_user_from_object($user);
            update_last_access($user->id, 1);
            Redirect(DIR_WS_SITE_CONTROL . "control.php");
        }
    }
}
if (isset($_POST['signup_user'])) {
    $obj = new user;
    $usernameExists = $obj->checkField('username', $_POST['username']);
    $obj = new user;
    $emailExists = $obj->checkField('email', $_POST['email']);
    if (!$usernameExists) {
        if (!$emailExists) {
            $_POST['user_type'] = 'user';
            $obj = new user;
            $_POST['dob'] = $_POST['month'] . '-' . $_POST['date'] . '-' . $_POST['year'];
            $user_id = $obj->saveUser($_POST);
            $obj = new user;
            $user = $obj->getUser($user_id);
            if ($user) {
                $subject = 'Email Verification';
                $from_email = SITE_EMAIL;
                $FromName = 'Unity';
                ob_start();
                ?>
                <div>
                    Welcome to Unity! Your verification code is <?php echo $user->verif_code; ?>. Click <a href="<?php echo DIR_WS_SITE ?>verify.php?id=<?php echo $user->id ?>&code=<?php echo $user->verif_code ?>">here</a> to verify your email and activate your account.<br /><br />
                    Username : <?php echo $user->username ?>
                </div>
                <?php
                $message = ob_get_clean();
                SendEmail($subject, $user->email, $from_email, $FromName, $message);
                $_SESSION['admin_session_secure']['toastr'] = 'Confirmation email sent to your email account';
                Redirect(DIR_WS_SITE);
            }
        } else {
            $_SESSION['admin_session_secure']['toastr_error'] = 'error';
            $_SESSION['admin_session_secure']['toastr'] = 'Email already exist';
            Redirect(DIR_WS_SITE);
        }
    } else {
        $_SESSION['admin_session_secure']['toastr_error'] = 'error';
        $_SESSION['admin_session_secure']['toastr'] = 'Username already exist';
        Redirect(DIR_WS_SITE);
    }
}
?>