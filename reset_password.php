<?php
require_once("include/config/config.php");
$Page = 'home';
$code = isset($_GET['code']) ? $_GET['code'] : '';

if ($admin_user->is_logged_in()) {
    Redirect(DIR_WS_SITE_CONTROL . "control.php");
}

$query = new user;
$is_reset_password = $query->get_using_field('reset_password', $code);

if (!$is_reset_password) {
    Redirect(DIR_WS_SITE_CONTROL . "control.php");
}

if (isset($_POST['reset_password'])) {
    extract($_POST);
    if ($password === $confirm_password) {
        $query = new user;
        $query->update_field($is_reset_password->id, 'password', md5($password));
        $_SESSION['admin_session_secure']['toastr'] = 'Password changed successfully!';
        Redirect(DIR_WS_SITE);
    } else {
        $admin_user->set_error();
        $admin_user->set_pass_msg('Passwords do not match');
        Redirect(DIR_WS_SITE_CONTROL . "reset_password.php");
    }
}
require 'tmp/header.php';
?>
<div class="row-fluid">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                Create new password
            </div>
            <div class="panel-body">
                <?php display_message(1); ?>
                <form action="" method="POST">
                    <div class="form-group">
                        <label class="control-label col-md-4">New Password</label>
                        <div class="col-md-8">
                            <input type="password" name="password" class="form-control" required/><br />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Confirm Password</label>
                        <div class="col-md-8">
                            <input type="password" name="confirm_password" class="form-control" required/><br />
                            <button type="submit" name="reset_password" class="btn btn-primary pull-right">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>
</div>
<?php require 'tmp/footer.php'; ?>